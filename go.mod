module gitee.com/hckdigi/silinode-common

go 1.16

//agent
require (
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/eclipse/paho.mqtt.golang v1.4.1
	github.com/fxamacker/cbor/v2 v2.2.0
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-redis/redis/v7 v7.2.0
	github.com/google/uuid v1.1.2
	github.com/pebbe/zmq4 v1.0.0
	github.com/pkg/errors v0.9.1
	github.com/satori/go.uuid v1.2.0
	golang.org/x/net v0.0.0-20220520000938-2e3eb7b945c2 // indirect
	gopkg.in/yaml.v2 v2.4.0
)

// device
require (
	bitbucket.org/bertimus9/systemstat v0.0.0-20180207000608-0eeff89b0690
	github.com/OneOfOne/xxhash v1.2.8
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.5.1
)

// mod
require (
	github.com/BurntSushi/toml v0.4.1
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/go-kit/kit v0.8.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/hashicorp/consul/api v1.1.0
	github.com/mitchellh/consulstructure v0.0.0-20190329231841-56fdc4d2da54
	github.com/mitchellh/copystructure v1.0.0 // indirect
	github.com/pelletier/go-toml v1.2.0
)
