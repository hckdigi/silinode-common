package link

import (
	"testing"
	"time"
)

func TestPrintMsgTypes(t *testing.T) {
	t.Log("start")
	ch := msgTypeIterator()
	time.Sleep(time.Second)
	for mt := range ch {
		t.Log(mt)
	}
	t.Log("end")
}
