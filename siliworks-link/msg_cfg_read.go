package link

import "encoding/json"

// MsgCfgRead 是云端读取设备配置消息（下行）。
type MsgCfgRead struct {
	*MsgHeader       `json:"header"`
	BodyOfMsgCfgRead `json:"body"`
}

type BodyOfMsgCfgRead struct {
	Type string `json:"type"`
}

var _ Msg = (*MsgCfgRead)(nil)

// NewMsgCfgRead 构造云端读取设备配置消息（下行）。
func NewMsgCfgRead(dst DstDevice, cfgType string) Msg {
	return &MsgCfgRead{
		MsgHeader: newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeCfgRead),
		BodyOfMsgCfgRead: BodyOfMsgCfgRead{
			Type: cfgType,
		},
	}
}

func (m MsgCfgRead) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgCfgRead) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgCfgRead) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.Type == "" {
		return newInvalidMsgFieldError("type", "missing")
	}

	return nil
}
