// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"bytes"
	"encoding/json"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// 注意，后续的开发者，每新增一条消息，改动的地方有：
//   1. 增加消息类型常量
//   2. getTopicTpl
//   3. unmarshalToMsg
// 之后，再创建消息对应的文件，写法模仿其他消息类，来实现 Msg 接口。

type MsgType uint8

const (
	/**
	 * 物模型相关
	 */

	// 读取设备属性（下行）
	MsgTypeDvcReadProp MsgType = iota + 1
	// 读取设备属性（回复）
	MsgTypeDvcReadPropReply

	// 修改设备属性（下行）
	MsgTypeDvcWriteProp
	// 修改设备属性（回复）
	MsgTypeDvcWritePropReply

	// 调用设备功能（下行）
	MsgTypeDvcFuncInvk
	// 调用设备功能（回复）
	MsgTypeDvcFuncInvkReply

	// 设备事件上报
	MsgTypeDvcEvent

	/**
	 * 网关功能
	 */

	// 绑定子设备（上行）
	MsgTypeGwBindSubdvc
	// 绑定子设备（回复）
	MsgTypeGwBindSubdvcReply

	// 解绑子设备（上行）
	MsgTypeGwUnbindSubdvc
	// 解绑子设备（回复）
	MsgTypeGwUnbindSubdvcReply

	// 子设备上下线（上行）
	MsgTypeGwOnofflineSubdvc
	// 子设备上下线（回复）
	MsgTypeGwOnofflineSubdvcReply

	// 查询拓扑关系（上行）
	MsgTypeGwTopoQuery
	// 查询拓扑关系（回复）
	MsgTypeGwTopoQueryReply

	// 拓扑关系变化（下行）
	MsgTypeGwTopoChange
	// 拓扑关系变化（回复）
	MsgTypeGwTopoChangeReply

	/**
	 * 配置同步相关
	 */

	// 云端配置推送（下行）
	MsgTypeCfgPush
	// 云端配置推送（回复）
	MsgTypeCfgPushReply

	// 云端读取设备配置（下行）
	MsgTypeCfgRead
	// 云端读取设备配置（回复）
	MsgTypeCfgReadReply

	// 设备端配置查询（上行）
	MsgTypeCfgQuery
	// 设备端配置查询（回复）
	MsgTypeCfgQueryReply

	// 设备配置上报（上行）
	MsgTypeCfgReport

	/**
	 * 其他消息
	 */

	// 透传消息（下行）
	MsgTypeTransparent
	// 透传消息（回复）
	MsgTypeTransparentReply

	// 心跳消息（上行）
	MsgTypeHeartbeat

	// 下线通知（下行）
	MsgTypeOfflineNotification

	// 非法消息
	MsgTypeInvalid // 开发者注意：该消息类型保持放在最后，否则会影响消息类型迭代器！
)

// getTopicTpl 获取消息类型的 topic 模板。
func (mt MsgType) getTopicTpl() string {
	var tpl string

	switch mt {
	// 物模型相关
	case MsgTypeDvcReadProp:
		tpl = fmt.Sprintf("/%s/%s/downlink/props/read", placeHolderProduct, placeHolderDevice)
	case MsgTypeDvcReadPropReply:
		tpl = fmt.Sprintf("/%s/%s/uplink/props/read-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeDvcWriteProp:
		tpl = fmt.Sprintf("/%s/%s/downlink/props/write", placeHolderProduct, placeHolderDevice)
	case MsgTypeDvcWritePropReply:
		tpl = fmt.Sprintf("/%s/%s/uplink/props/write-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeDvcFuncInvk:
		tpl = fmt.Sprintf("/%s/%s/downlink/func/invoke", placeHolderProduct, placeHolderDevice)
	case MsgTypeDvcFuncInvkReply:
		tpl = fmt.Sprintf("/%s/%s/uplink/func/invoke-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeDvcEvent:
		tpl = fmt.Sprintf("/%s/%s/uplink/event/report", placeHolderProduct, placeHolderDevice)

	// 网关功能
	case MsgTypeGwBindSubdvc:
		tpl = fmt.Sprintf("/%s/%s/uplink/gateway/bind", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwBindSubdvcReply:
		tpl = fmt.Sprintf("/%s/%s/downlink/gateway/bind-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwUnbindSubdvc:
		tpl = fmt.Sprintf("/%s/%s/uplink/gateway/unbind", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwUnbindSubdvcReply:
		tpl = fmt.Sprintf("/%s/%s/downlink/gateway/unbind-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwOnofflineSubdvc:
		tpl = fmt.Sprintf("/%s/%s/uplink/gateway/subdvc-onoffline", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwOnofflineSubdvcReply:
		tpl = fmt.Sprintf("/%s/%s/downlink/gateway/subdvc-onoffline-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwTopoQuery:
		tpl = fmt.Sprintf("/%s/%s/uplink/gateway/topo", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwTopoQueryReply:
		tpl = fmt.Sprintf("/%s/%s/downlink/gateway/topo-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwTopoChange:
		tpl = fmt.Sprintf("/%s/%s/downlink/gateway/topo-change", placeHolderProduct, placeHolderDevice)
	case MsgTypeGwTopoChangeReply:
		tpl = fmt.Sprintf("/%s/%s/uplink/gateway/topo-change-reply", placeHolderProduct, placeHolderDevice)

	// 配置同步相关
	case MsgTypeCfgPush:
		tpl = fmt.Sprintf("/%s/%s/downlink/config/push", placeHolderProduct, placeHolderDevice)
	case MsgTypeCfgPushReply:
		tpl = fmt.Sprintf("/%s/%s/uplink/config/push-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeCfgRead:
		tpl = fmt.Sprintf("/%s/%s/downlink/config/read", placeHolderProduct, placeHolderDevice)
	case MsgTypeCfgReadReply:
		tpl = fmt.Sprintf("/%s/%s/uplink/config/read-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeCfgQuery:
		tpl = fmt.Sprintf("/%s/%s/uplink/config/query", placeHolderProduct, placeHolderDevice)
	case MsgTypeCfgQueryReply:
		tpl = fmt.Sprintf("/%s/%s/downlink/config/query-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeCfgReport:
		tpl = fmt.Sprintf("/%s/%s/uplink/config/report", placeHolderProduct, placeHolderDevice)

	// 其他消息
	case MsgTypeTransparent:
		tpl = fmt.Sprintf("/%s/%s/downlink/transparent", placeHolderProduct, placeHolderDevice)
	case MsgTypeTransparentReply:
		tpl = fmt.Sprintf("/%s/%s/uplink/transparent-reply", placeHolderProduct, placeHolderDevice)
	case MsgTypeHeartbeat:
		tpl = fmt.Sprintf("/%s/%s/uplink/heartbeat", placeHolderProduct, placeHolderDevice)
	case MsgTypeOfflineNotification:
		tpl = fmt.Sprintf("/%s/%s/downlink/offline-notification", placeHolderProduct, placeHolderDevice)
	}

	return tpl
}

// unmarshalToMsg 将消息内容反序列化为 mt 类型的消息实例。
func (mt MsgType) unmarshalToMsg(payload []byte) (Msg, error) {
	var msg Msg

	switch mt {

	// 物模型相关消息
	case MsgTypeDvcReadProp:
		msg = new(MsgDvcReadProp)
	case MsgTypeDvcReadPropReply:
		msg = new(MsgDvcReadPropReply)
	case MsgTypeDvcWriteProp:
		msg = new(MsgDvcWriteProp)
	case MsgTypeDvcWritePropReply:
		msg = new(MsgDvcWritePropReply)
	case MsgTypeDvcFuncInvk:
		msg = new(MsgDvcFuncInvk)
	case MsgTypeDvcFuncInvkReply:
		msg = new(MsgDvcFuncInvkReply)
	case MsgTypeDvcEvent:
		msg = new(MsgDvcEvent)

	// 网关功能消息
	case MsgTypeGwBindSubdvc:
		msg = new(MsgGwBindSubdvc)
	case MsgTypeGwBindSubdvcReply:
		msg = new(MsgGwBindSubdvcReply)
	case MsgTypeGwUnbindSubdvc:
		msg = new(MsgGwUnbindSubdvc)
	case MsgTypeGwUnbindSubdvcReply:
		msg = new(MsgGwUnbindSubdvcReply)
	case MsgTypeGwOnofflineSubdvc:
		msg = new(MsgGwOnofflineSubdvc)
	case MsgTypeGwOnofflineSubdvcReply:
		msg = new(MsgGwOnofflineSubdvcReply)
	case MsgTypeGwTopoQuery:
		msg = new(MsgGwTopoQuery)
	case MsgTypeGwTopoQueryReply:
		msg = new(MsgGwTopoQueryReply)
	case MsgTypeGwTopoChange:
		msg = new(MsgGwTopoChange)
	case MsgTypeGwTopoChangeReply:
		msg = new(MsgGwTopoChangeReply)

	// 配置同步相关
	case MsgTypeCfgPush:
		msg = new(MsgCfgPush)
	case MsgTypeCfgPushReply:
		msg = new(MsgCfgPushReply)
	case MsgTypeCfgRead:
		msg = new(MsgCfgRead)
	case MsgTypeCfgReadReply:
		msg = new(MsgCfgReadReply)
	case MsgTypeCfgQuery:
		msg = new(MsgCfgQuery)
	case MsgTypeCfgQueryReply:
		msg = new(MsgCfgQueryReply)
	case MsgTypeCfgReport:
		msg = new(MsgCfgReport)

	// 其他消息
	case MsgTypeTransparent:
		msg = new(MsgTransparent)
	case MsgTypeTransparentReply:
		msg = new(MsgTransparentReply)
	case MsgTypeHeartbeat:
		msg = new(MsgHeartbeat)
	case MsgTypeOfflineNotification:
		msg = new(MsgOfflineNotification)
	}

	if err := json.Unmarshal(payload, msg); err != nil {
		return msg, err
	}

	msg.setType(mt)
	return msg, msg.Validate()
}

// fit 判断 topic 是否符合该消息类型。
func (mt MsgType) fit(topic string) (bool, error) {
	tplReg, err := mt.genTopicTplReg()
	if err != nil {
		return false, err
	}

	return tplReg.MatchString(topic), nil
}

// genTopicTplReg 生成消息类型的 topic 模板正则。
func (mt MsgType) genTopicTplReg() (*regexp.Regexp, error) {
	tpl := mt.getTopicTpl()
	idxs := mt.getBraceIndicesOfTopicTpl()

	pattern := new(bytes.Buffer)
	pattern.WriteByte('^')

	var end int
	for i := 0; i < len(idxs); i += 2 {
		raw := tpl[end:idxs[i]]
		end = idxs[i+1]

		parts := strings.SplitN(tpl[idxs[i]+1:end-1], ":", 2)
		name := parts[0]
		patt := "[^/]+"
		if len(parts) == 2 {
			patt = parts[1]
		}

		if name == "" || patt == "" {
			return nil, fmt.Errorf("missing name or pattern in %q", tpl[idxs[i]:end])
		}

		fmt.Fprintf(pattern, "%s(?P<%s>%s)", regexp.QuoteMeta(raw), "v"+strconv.Itoa(i), patt)
	}

	pattern.WriteString(regexp.QuoteMeta(tpl[end:]) + "[/]?$")
	return regexp.Compile(pattern.String())
}

// getBraceIndicesOfTopicTpl 获取消息类型 topic 模板中前后花括号索引。
func (mt MsgType) getBraceIndicesOfTopicTpl() (idxs []int) {
	s := mt.getTopicTpl()

	for i := 0; i < len(s); i++ {
		switch s[i] {
		case '{':
			idxs = append(idxs, i)
		case '}':
			idxs = append(idxs, i+1)
		}
	}

	return
}

// msgTypeIterator 返回一个所有消息类型的迭代器。
func msgTypeIterator() (msgTypes <-chan MsgType) {
	ch := make(chan MsgType, int(MsgTypeInvalid)-1)

	go func() {
		for i := 1; i < int(MsgTypeInvalid); i++ {
			ch <- MsgType(i)
		}
		close(ch)
	}()

	return ch
}
