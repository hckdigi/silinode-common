// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/14

package link

import (
	"encoding/json"
)

// MsgCfgQuery 是设备端配置查询消息（上行）。
type MsgCfgQuery struct {
	*MsgHeader        `json:"header"`
	BodyOfMsgCfgQuery `json:"body"`
}

type BodyOfMsgCfgQuery struct {
	Type string `json:"type"`
}

var _ Msg = (*MsgCfgQuery)(nil)

func NewMsgCfgQuery(src SrcDevice, cfgType string) Msg {
	return &MsgCfgQuery{
		MsgHeader: newMsgHeader(src.ProductID, src.DeviceName, MsgTypeCfgQuery),
		BodyOfMsgCfgQuery: BodyOfMsgCfgQuery{
			Type: cfgType,
		},
	}
}

func (m MsgCfgQuery) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgCfgQuery) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgCfgQuery) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Type) == 0 {
		return newInvalidMsgFieldError("type", "missing")
	}

	return nil
}
