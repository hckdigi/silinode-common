package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwTopoChangeReply 是拓扑关系变化消息的回复（上行）。
type MsgGwTopoChangeReply struct {
	ReplyStatus                `json:"reply"`
	*MsgHeader                 `json:"header"`
	BodyOfMsgGwTopoChangeReply `json:"body"`
}

type BodyOfMsgGwTopoChangeReply struct {
	Results []SubdvcOpResult `json:"devices"`
}

var _ Msg = (*MsgGwTopoChangeReply)(nil)

// NewMsgGwTopoChangeReply 构造一个拓扑关系变化消息的回复（上行）。
// src 为网关设备，msgID 与下行拓扑变化消息一致，
// replyStatus 可通过 NewSuccessReply 或 NewFailReply 构造，表示处理的成功与否，
// result 表示各设备的操作结果。
func NewMsgGwTopoChangeReply(src SrcDevice, msgID string, replyStatus ReplyStatus, results []SubdvcOpResult) Msg {
	return &MsgGwTopoChangeReply{
		ReplyStatus:                replyStatus,
		MsgHeader:                  newMsgHeader(src.ProductID, src.DeviceName, MsgTypeGwTopoChangeReply, msgID),
		BodyOfMsgGwTopoChangeReply: BodyOfMsgGwTopoChangeReply{Results: results},
	}
}

func (m MsgGwTopoChangeReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwTopoChangeReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwTopoChangeReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Results) == 0 {
		return newInvalidMsgFieldError("devices", "empty")
	}

	for i, res := range m.Results {
		if res.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if res.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}
		if res.Result == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].result", i), "missing")
		}
		if res.Result > 1 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].result", i), "cannot be greater than 1")
		}
		if res.Result < 0 && res.FailMsg == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].failMsg", i), "missing")
		}
	}

	return nil
}
