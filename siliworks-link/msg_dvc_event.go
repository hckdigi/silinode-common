// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"encoding/json"
	"time"
)

// MsgDvcEvent 是设备事件上报消息（上行）。
type MsgDvcEvent struct {
	*MsgHeader        `json:"header"`
	BodyOfMsgDvcEvent `json:"body"`
}

type BodyOfMsgDvcEvent struct {
	// EventID 为设备事件 ID。
	EventID string `json:"eventId"`
	// EventTime 为设备事件的时间，此时间与消息头部的时间不同，此时间才真正代表事
	// 件产生的时间。对于离线属性上报的场景来讲，该字段代表当时事件的时间。
	EventTime time.Time `json:"eventTime"`
	// Props 为事件内的各属性。
	Props KVs `json:"props"`
}

var _ Msg = (*MsgDvcEvent)(nil)

func NewMsgDvcEvent(src SrcDevice, eventID string, eventTime time.Time, props KVs) Msg {
	return &MsgDvcEvent{
		MsgHeader: newMsgHeader(src.ProductID, src.DeviceName, MsgTypeDvcEvent),
		BodyOfMsgDvcEvent: BodyOfMsgDvcEvent{
			EventID:   eventID,
			EventTime: eventTime,
			Props:     props,
		},
	}
}

func (m MsgDvcEvent) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgDvcEvent) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgDvcEvent) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.EventID == "" {
		return newInvalidMsgFieldError("eventID", "missing")
	}

	if m.EventTime.IsZero() {
		return newInvalidMsgFieldError("eventTime", "zero value")
	}

	if len(m.Props) == 0 {
		return newInvalidMsgFieldError("props", "empty")
	}

	return nil
}
