// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"errors"
)

type MsgHandler func(msg Msg)

type Parser struct {
	handlers map[MsgType]MsgHandler
}

// NewParser 构建消息解析器。
func NewParser() *Parser {
	return &Parser{handlers: make(map[MsgType]MsgHandler)}
}

// SetMsgHandler 注册消息处理单元。
func (p *Parser) SetMsgHandler(msgType MsgType, msgHandler MsgHandler) *Parser {
	if p.handlers == nil {
		p.handlers = make(map[MsgType]MsgHandler, 1)
	}

	p.handlers[msgType] = msgHandler
	return p
}

// Parser 执行解析，该方法应该在 SetMsgHandler 之后调用。若未发现可用的
// 消息处理单元，则会忽略处理。
func (p *Parser) Parse(topic string, payload []byte) error {
	if len(p.handlers) == 0 {
		return errors.New("link: no msg handlers registered")
	}
	if len(topic) == 0 {
		return errors.New("the topic passed in is empty")
	}

	mt, err := p.parseMsgType(topic)
	if err != nil {
		return err
	}

	handler, ok := p.handlers[mt]
	if !ok {
		return nil // no matched handlers, ignore directly.
	}

	msg, err := mt.unmarshalToMsg(payload)
	if err != nil {
		return err
	}

	handler(msg)
	return nil
}

func (p *Parser) parseMsgType(topic string) (MsgType, error) {
	for msgType := range msgTypeIterator() {
		fit, err := msgType.fit(topic)
		if err != nil {
			return MsgTypeInvalid, err
		}
		if fit {
			return msgType, nil
		}
	}

	return MsgTypeInvalid, ErrNonKlinkMsg
}
