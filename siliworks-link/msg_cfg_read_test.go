package link

import (
	"fmt"
	"testing"
)

func TestNewMsgCfgRead(t *testing.T) {
	msg := NewMsgCfgRead(DstDevice{
		ProductID:  "product_id_xxx",
		DeviceName: "device_name_xxx",
	}, "resource.all")

	fmt.Println(msg.GetMsgTopic())
	fmt.Printf("%s\n", msg.GetPayload())
}
