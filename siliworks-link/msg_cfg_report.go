package link

import "encoding/json"

// MsgCfgReport 是云端配置上报消息（上行）。
type MsgCfgReport struct {
	*MsgHeader         `json:"header"`
	BodyOfMsgCfgReport `json:"body"`
}

type BodyOfMsgCfgReport struct {
	Type   string `json:"type"`
	Config KVs    `json:"config"`
}

var _ Msg = (*MsgCfgReport)(nil)

func NewMsgCfgReport(src DstDevice, cfgType string, config KVs) Msg {
	return &MsgCfgReport{
		MsgHeader: newMsgHeader(src.ProductID, src.DeviceName, MsgTypeCfgReport),
		BodyOfMsgCfgReport: BodyOfMsgCfgReport{
			Type:   cfgType,
			Config: config,
		},
	}
}

func (m MsgCfgReport) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgCfgReport) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgCfgReport) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.Type == "" {
		return newInvalidMsgFieldError("type", "missing")
	}
	if len(m.Config) == 0 {
		return newInvalidMsgFieldError("config", "missing")
	}

	return nil
}
