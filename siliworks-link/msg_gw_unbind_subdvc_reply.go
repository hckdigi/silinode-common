// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/14

package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwUnbindSubdvcReply 是网关解绑子设备消息（回复）。
type MsgGwUnbindSubdvcReply struct {
	ReplyStatus                  `json:"reply"`
	*MsgHeader                   `json:"header"`
	BodyOfMsgGwUnbindSubdvcReply `json:"body"`
}

type BodyOfMsgGwUnbindSubdvcReply struct {
	Results []SubdvcOpResult `json:"devices"`
}

var _ Msg = (*MsgGwUnbindSubdvcReply)(nil)

func NewMsgGwUnbindSubdvcReply(dst DstDevice, msgID string, replyStatus ReplyStatus, results []SubdvcOpResult) Msg {
	return &MsgGwUnbindSubdvcReply{
		ReplyStatus: replyStatus,
		MsgHeader:   newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeGwUnbindSubdvcReply, msgID),
		BodyOfMsgGwUnbindSubdvcReply: BodyOfMsgGwUnbindSubdvcReply{
			Results: results,
		},
	}
}

func (m MsgGwUnbindSubdvcReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwUnbindSubdvcReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwUnbindSubdvcReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Results) == 0 {
		return newInvalidMsgFieldError("devices", "empty")
	}

	for i, res := range m.Results {
		if res.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if res.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}
		if res.Result == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].result", i), "missing")
		}
		if res.Result != 1 && res.Result != -1 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].result", i), "neither 1 nor -1")
		}
		if res.Result == 1 && res.FailMsg == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].failMsg", i), "missing")
		}
	}

	return nil
}
