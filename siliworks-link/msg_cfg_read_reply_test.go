package link

import (
	"fmt"
	"testing"
)

func TestNewMsgCfgReadReply(t *testing.T) {
	msg := NewMsgCfgReadReply(SrcDevice{
		ProductID:  "product_id_xxx",
		DeviceName: "device_name_xxx",
	}, "", NewSuccessReply(), "resource.all", map[string]interface{}{
		"foo": "bar",
		"baz": "quz",
	})

	fmt.Println(msg.GetMsgTopic())
	fmt.Printf("%s\n", msg.GetPayload())
}
