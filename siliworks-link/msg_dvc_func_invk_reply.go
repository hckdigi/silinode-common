package link

import (
	"encoding/json"
)

// MsgDvcFuncInvkReply 是调用设备功能消息（回复）。
type MsgDvcFuncInvkReply struct {
	ReplyStatus               `json:"reply"`
	*MsgHeader                `json:"header"`
	BodyOfMsgDvcFuncInvkReply `json:"body"`
}

type BodyOfMsgDvcFuncInvkReply struct {
	Output KVs `json:"output"`
}

var _ Msg = (*MsgDvcFuncInvkReply)(nil)

// NewMsgDvcFuncInvkReply 构建调用设备功能的回复消息。
func NewMsgDvcFuncInvkReply(src SrcDevice, msgID string, replyStatus ReplyStatus, output KVs) Msg {
	return &MsgDvcFuncInvkReply{
		ReplyStatus:               replyStatus,
		MsgHeader:                 newMsgHeader(src.ProductID, src.DeviceName, MsgTypeDvcFuncInvkReply, msgID),
		BodyOfMsgDvcFuncInvkReply: BodyOfMsgDvcFuncInvkReply{Output: output},
	}
}

func (m MsgDvcFuncInvkReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgDvcFuncInvkReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgDvcFuncInvkReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	return nil
}
