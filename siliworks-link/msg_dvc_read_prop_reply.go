package link

import (
	"encoding/json"
)

// MsgDvcReadPropReply 是读取设备属性（回复）消息。
type MsgDvcReadPropReply struct {
	ReplyStatus               `json:"reply"`
	*MsgHeader                `json:"header"`
	BodyOfMsgDvcReadPropReply `json:"body"`
}

type BodyOfMsgDvcReadPropReply struct {
	Props KVs `json:"props"`
}

var _ Msg = (*MsgDvcReadPropReply)(nil)

func NewMsgDvcReadPropReply(src SrcDevice, msgID string, replyStatus ReplyStatus, props KVs) Msg {
	return &MsgDvcReadPropReply{
		ReplyStatus:               replyStatus,
		MsgHeader:                 newMsgHeader(src.ProductID, src.DeviceName, MsgTypeDvcReadPropReply, msgID),
		BodyOfMsgDvcReadPropReply: BodyOfMsgDvcReadPropReply{Props: props},
	}
}

func (m MsgDvcReadPropReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgDvcReadPropReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgDvcReadPropReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.IsSuccess() && len(m.Props) == 0 {
		return newInvalidMsgFieldError("props", "empty")
	}

	return nil
}
