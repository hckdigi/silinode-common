package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwOnofflineSubdvcReply 是网关代理子设备上下线消息的回复。
type MsgGwOnofflineSubdvcReply struct {
	ReplyStatus                     `json:"reply"`
	*MsgHeader                      `json:"header"`
	BodyOfMsgGwOnofflineSubdvcReply `json:"body"`
}

type BodyOfMsgGwOnofflineSubdvcReply struct {
	Results []SubdvcOnofflineResult `json:"devices"`
}

// SubdvcOnofflineResult 表示子设备上下线处理结果。
type SubdvcOnofflineResult struct {
	// ProductID 表示子设备产品 ID。
	ProductID string `json:"productId"`
	// DeviceName 表示子设备名称。
	DeviceName string `json:"deviceName"`
	// Action 表示子设备上下线动作，1 上线 -1 下线，与上行请求中的 Status 字段对应。
	Action int `json:"action"`
	// Result 表示云端执行结果，1 成功 -1 失败。
	Result int `json:"result"`
	// FailMsg 表示失败原因。
	FailMsg string `json:"failMsg,omitempty"`
}

var _ Msg = (*MsgGwOnofflineSubdvcReply)(nil)

// NewMsgGwOnofflineSubdvcReply 构造网关代理子设备上下线消息的回复。
func NewMsgGwOnofflineSubdvcReply(dst DstDevice, msgID string, replyStatus ReplyStatus, results []SubdvcOnofflineResult) Msg {
	return &MsgGwOnofflineSubdvcReply{
		ReplyStatus: replyStatus,
		MsgHeader:   newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeGwOnofflineSubdvcReply, msgID),
		BodyOfMsgGwOnofflineSubdvcReply: BodyOfMsgGwOnofflineSubdvcReply{
			Results: results,
		},
	}
}

func (m MsgGwOnofflineSubdvcReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwOnofflineSubdvcReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwOnofflineSubdvcReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Results) == 0 {
		return newInvalidMsgFieldError("devices", "empty")
	}

	for i, res := range m.Results {
		if res.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if res.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}

		if res.Action == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].action", i), "missing")
		}
		if res.Action != 1 && res.Action != -1 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].action", i), "neither 1 nor -1")
		}

		if res.Result == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].result", i), "missing")
		}
		if res.Result != 1 && res.Result != -1 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].result", i), "neither 1 nor -1")
		}
		if res.Result == -1 && res.FailMsg == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].failMsg", i), "missing")
		}
	}

	return nil
}
