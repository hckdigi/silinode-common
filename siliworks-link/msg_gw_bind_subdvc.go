// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/14

package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwBindSubdvc 是网关绑定子设备消息（上行）。
type MsgGwBindSubdvc struct {
	*MsgHeader            `json:"header"`
	BodyOfMsgGwBindSubdvc `json:"body"`
}

type BodyOfMsgGwBindSubdvc struct {
	Devices []SubdvcAuth `json:"devices"`
}

// SubdvcAuth 是子设备认证信息。
type SubdvcAuth struct {
	ProductID      string `json:"productId"`
	DeviceName     string `json:"deviceName"`
	Random         string `json:"random"`
	ExpirationTime int    `json:"expirationTime"`
	AuthType       string `json:"authType"`
	SignMethod     string `json:"signMethod"`
	Signature      string `json:"signature"`
}

var _ Msg = (*MsgGwBindSubdvc)(nil)

func NewMsgGwBindSubdvc(src SrcDevice, subdvcs []SubdvcAuth) Msg {
	return &MsgGwBindSubdvc{
		MsgHeader: newMsgHeader(src.ProductID, src.DeviceName, MsgTypeGwBindSubdvc),
		BodyOfMsgGwBindSubdvc: BodyOfMsgGwBindSubdvc{
			Devices: subdvcs,
		},
	}
}

func (m MsgGwBindSubdvc) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwBindSubdvc) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwBindSubdvc) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Devices) == 0 {
		return newInvalidMsgFieldError("devices", "empty")
	}

	for i, dvc := range m.Devices {
		if dvc.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if dvc.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}
		if len(dvc.Random) == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].random", i), "missing")
		}
		if dvc.ExpirationTime == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].expirationTime", i), "missing")
		}
		if dvc.AuthType == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].authType", i), "missing")
		}
		if dvc.SignMethod == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].signMethod", i), "missing")
		}
		if dvc.Signature == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].signature", i), "missing")
		}
	}

	return nil
}
