package link

import "encoding/json"

// MsgOfflineNotification 是云端下发的下线通知消息（下行）。
type MsgOfflineNotification struct {
	*MsgHeader                   `json:"header"`
	BodyOfMsgOfflineNotification `json:"body"`
}

type BodyOfMsgOfflineNotification struct {
	Reason string `json:"reason"`
}

var _ Msg = (*MsgOfflineNotification)(nil)

func NewMsgOfflineNotification(dst DstDevice, reason string) Msg {
	return &MsgOfflineNotification{
		MsgHeader: newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeOfflineNotification),
		BodyOfMsgOfflineNotification: BodyOfMsgOfflineNotification{
			Reason: reason,
		},
	}
}

func (m MsgOfflineNotification) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgOfflineNotification) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgOfflineNotification) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.Reason == "" {
		return newInvalidMsgFieldError("reason", "missing")
	}

	return nil
}
