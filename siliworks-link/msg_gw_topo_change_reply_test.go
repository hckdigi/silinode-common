package link

import (
	"encoding/json"
	"testing"
)

func TestNewMsgGwTopoChangeReply(t *testing.T) {
	msg := NewMsgGwTopoChangeReply(SrcDevice{
		ProductID:  "foo",
		DeviceName: "bar",
	}, "uuid", NewSuccessReply(), []SubdvcOpResult{
		{
			ProductID:  "p1",
			DeviceName: "d1",
			Result:     1,
			FailMsg:    "",
		}, {
			ProductID:  "p2",
			DeviceName: "d2",
			Result:     -1,
			FailMsg:    "some reasons",
		},
	})

	b, _ := json.MarshalIndent(msg, "", "    ")
	t.Log(string(b))
}
