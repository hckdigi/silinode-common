// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/14

package link

import (
	"encoding/json"
)

// MsgGwTopoQuery 是网关查询拓扑关系（上行）。
type MsgGwTopoQuery struct {
	*MsgHeader `json:"header"`
}

var _ Msg = (*MsgGwTopoQuery)(nil)

func NewMsgGwTopoQuery(src SrcDevice) Msg {
	return &MsgGwTopoQuery{
		MsgHeader: newMsgHeader(src.ProductID, src.DeviceName, MsgTypeGwTopoQuery),
	}
}

func (m MsgGwTopoQuery) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwTopoQuery) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwTopoQuery) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	return nil
}
