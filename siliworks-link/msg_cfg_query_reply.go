// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"encoding/json"
)

// MsgCfgQueryReply 是设备端配置查询消息（下行回复）。
type MsgCfgQueryReply struct {
	ReplyStatus            `json:"reply"`
	*MsgHeader             `json:"header"`
	BodyOfMsgCfgQueryReply `json:"body"`
}

type BodyOfMsgCfgQueryReply struct {
	Config KVs `json:"config"`
}

var _ Msg = (*MsgCfgQueryReply)(nil)

func NewMsgCfgQueryReply(dst DstDevice, msgID string, replyStatus ReplyStatus, config KVs) Msg {
	return &MsgCfgQueryReply{
		ReplyStatus:            replyStatus,
		MsgHeader:              newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeCfgQueryReply),
		BodyOfMsgCfgQueryReply: BodyOfMsgCfgQueryReply{Config: config},
	}
}

func (m MsgCfgQueryReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgCfgQueryReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgCfgQueryReply) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Config) == 0 {
		return newInvalidMsgFieldError("config", "missing")
	}

	return nil
}
