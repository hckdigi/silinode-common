package link

import (
	"errors"
	"fmt"
)

var (
	// ErrNonKlinkMsg 非 Klink 协议消息。
	ErrNonKlinkMsg = errors.New("non-link message")
	// ErrInvalidMsgField 消息字段值不合法。
	// 所有的消息反序列化后的 validation 操作返回的错误类型都为该类型，
	// 若 parser.Parse 方法返回的错误为该类型，说明是发送方发送的消息
	// 字段不合法。可使用 errors.Is(err, link.ErrInvalidMsgField) 方法判断。
	ErrInvalidMsgField = errors.New("invalid message field")
	// ErrIncompatibleKlinkVersion 表示 Klink 版本不兼容。
	ErrIncompatibleKlinkVersion = errors.New("incompatible link version")
)

func newInvalidMsgFieldError(fieldName, errMsg string) error {
	return fmt.Errorf("%w: %q (%s)", ErrInvalidMsgField, fieldName, errMsg)
}
