// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/14

package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwUnbindSubdvc 是网关解绑子设备消息（上行）。
type MsgGwUnbindSubdvc struct {
	*MsgHeader              `json:"header"`
	BodyOfMsgGwUnbindSubdvc `json:"body"`
}

type BodyOfMsgGwUnbindSubdvc struct {
	Devices []DeviceIdentity `json:"devices"`
}

var _ Msg = (*MsgGwUnbindSubdvc)(nil)

func NewMsgGwUnbindSubdvc(src SrcDevice, subdvcs []DeviceIdentity) Msg {
	return &MsgGwUnbindSubdvc{
		MsgHeader: newMsgHeader(src.ProductID, src.DeviceName, MsgTypeGwUnbindSubdvc),
		BodyOfMsgGwUnbindSubdvc: BodyOfMsgGwUnbindSubdvc{
			Devices: subdvcs,
		},
	}
}

func (m MsgGwUnbindSubdvc) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwUnbindSubdvc) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwUnbindSubdvc) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Devices) == 0 {
		return newInvalidMsgFieldError("devices", "empty")
	}

	for i, dvc := range m.Devices {
		if dvc.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if dvc.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}
	}

	return nil
}
