package link

import "encoding/json"

// MsgCfgReadReply 是云端读取设备配置的回复消息（上行）。
type MsgCfgReadReply struct {
	ReplyStatus           `json:"reply"`
	*MsgHeader            `json:"header"`
	BodyOfMsgCfgReadReply `json:"body"`
}

type BodyOfMsgCfgReadReply struct {
	Type   string `json:"type"`
	Config KVs    `json:"config"`
}

var _ Msg = (*MsgCfgReadReply)(nil)

// NewMsgCfgReadReply 构造云端读取设备配置的回复消息（上行）。
func NewMsgCfgReadReply(src SrcDevice, msgID string, replyStatus ReplyStatus, cfgType string, cfgBody KVs) Msg {
	return &MsgCfgReadReply{
		ReplyStatus: replyStatus,
		MsgHeader:   newMsgHeader(src.ProductID, src.DeviceName, MsgTypeCfgReadReply, msgID),
		BodyOfMsgCfgReadReply: BodyOfMsgCfgReadReply{
			Type:   cfgType,
			Config: cfgBody,
		},
	}
}

func (m MsgCfgReadReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgCfgReadReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgCfgReadReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.Type == "" {
		return newInvalidMsgFieldError("type", "missing")
	}
	if len(m.Config) == 0 {
		return newInvalidMsgFieldError("config", "empty")
	}

	return nil
}
