// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/14

package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwBindSubdvcReply 是网关绑定子设备消息（回复）。
type MsgGwBindSubdvcReply struct {
	ReplyStatus                `json:"reply"`
	*MsgHeader                 `json:"header"`
	BodyOfMsgGwBindSubdvcReply `json:"body"`
}

type BodyOfMsgGwBindSubdvcReply struct {
	Results []SubdvcOpResult `json:"devices"`
}

// SubdvcOpResult 是子设备操作结果。
type SubdvcOpResult struct {
	ProductID  string `json:"productId"`
	DeviceName string `json:"deviceName"`
	Result     int    `json:"result"`
	FailMsg    string `json:"failMsg,omitempty"`
}

var _ Msg = (*MsgGwBindSubdvcReply)(nil)

func NewMsgGwBindSubdvcReply(dst DstDevice, msgID string, replyStatus ReplyStatus, results []SubdvcOpResult) Msg {
	return &MsgGwBindSubdvcReply{
		ReplyStatus: replyStatus,
		MsgHeader:   newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeGwBindSubdvcReply, msgID),
		BodyOfMsgGwBindSubdvcReply: BodyOfMsgGwBindSubdvcReply{
			Results: results,
		},
	}
}

func (m MsgGwBindSubdvcReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwBindSubdvcReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwBindSubdvcReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Results) == 0 {
		return newInvalidMsgFieldError("devices", "empty")
	}

	for i, res := range m.Results {
		if res.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if res.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}
		if res.Result == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].result", i), "missing")
		}
		if res.Result != 1 && res.Result != -1 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].result", i), "neither 1 nor -1")
		}
		if res.Result == 1 && res.FailMsg == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].failMsg", i), "missing")
		}
	}

	return nil
}
