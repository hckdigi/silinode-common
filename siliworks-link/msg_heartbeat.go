// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2022/05/18

package link

import (
	"encoding/json"
)

// MsgHeartbeat 是直连设备/网关心跳上报消息（上行）。
type MsgHeartbeat struct {
	*MsgHeader `json:"header"`
}

var _ Msg = (*MsgHeartbeat)(nil)

func NewMsgHeartbeat(src SrcDevice) Msg {
	return &MsgHeartbeat{
		MsgHeader: newMsgHeader(src.ProductID, src.DeviceName, MsgTypeHeartbeat),
	}
}

func (m MsgHeartbeat) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgHeartbeat) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgHeartbeat) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	return nil
}
