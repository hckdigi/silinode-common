// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"reflect"
	"testing"
)

func TestFuncInputs(t *testing.T) {
	type args struct {
		pairs []interface{}
	}
	tests := []struct {
		name string
		args args
		want []FuncInput
	}{
		// TODO: Add test cases.
		{
			args: args{pairs: []interface{}{"foo", 123}},
			want: []FuncInput{{Name: "foo", Value: 123}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FuncInputs(tt.args.pairs...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FuncInputs() = %v, want %v", got, tt.want)
			}
		})
	}
}
