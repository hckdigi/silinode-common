// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/14

package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwTopoQueryReply 是网关查询拓扑关系（回复）。
type MsgGwTopoQueryReply struct {
	ReplyStatus               `json:"reply"`
	*MsgHeader                `json:"header"`
	BodyOfMsgGwTopoQueryReply `json:"body"`
}

type BodyOfMsgGwTopoQueryReply struct {
	// Subdvcs 是网关下绑定的子设备列表。
	Subdvcs []SubdvcWithConfig `json:"devices"`
}

type SubdvcWithConfig struct {
	DeviceIdentity
	Config KVs `json:"config"`
}

var _ Msg = (*MsgGwTopoQueryReply)(nil)

func NewMsgGwTopoQueryReply(dst DstDevice, msgID string, replyStatus ReplyStatus, subdvcs []SubdvcWithConfig) Msg {
	return &MsgGwTopoQueryReply{
		ReplyStatus: replyStatus,
		MsgHeader:   newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeGwTopoQueryReply, msgID),
		BodyOfMsgGwTopoQueryReply: BodyOfMsgGwTopoQueryReply{
			Subdvcs: subdvcs,
		},
	}
}

func (m MsgGwTopoQueryReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwTopoQueryReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwTopoQueryReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	for i, res := range m.Subdvcs {
		if res.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if res.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}
		if res.Config == nil {
			return newInvalidMsgFieldError(fmt.Sprintf("device[%d].Config", i), "missing")
		}
	}

	return nil
}
