// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"encoding/json"
)

// MsgDvcReadProp 是读取设备属性消息。
type MsgDvcReadProp struct {
	*MsgHeader           `json:"header"`
	BodyOfMsgDvcReadProp `json:"body"`
}

type BodyOfMsgDvcReadProp struct {
	Props []string `json:"props"`
}

var _ Msg = (*MsgDvcReadProp)(nil)

func NewMsgDvcReadProp(dst DstDevice, props []string) Msg {
	return &MsgDvcReadProp{
		MsgHeader: newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeDvcReadProp),
		BodyOfMsgDvcReadProp: BodyOfMsgDvcReadProp{
			Props: props,
		},
	}
}

func (m MsgDvcReadProp) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgDvcReadProp) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgDvcReadProp) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Props) == 0 {
		return newInvalidMsgFieldError("props", "empty")
	}

	return nil
}
