// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"strconv"
	"strings"
	"time"

	uuid "github.com/satori/go.uuid"
)

/**********************************************************************************************/

// Msg 是 Klink 协议消息。
type Msg interface {
	// GetPayload 获取消息数据。
	GetPayload() []byte
	// GetMsgTopic 获取消息所对应的 MQ Topic。
	GetMsgTopic() string
	// GetMsgID 获取消息 ID。
	GetMsgID() string
	// GetProductID 获取消息发送/接收者的产品 ID。
	GetProductID() string
	// GetDeviceName 获取消息发送/接受者的设备名称。
	GetDeviceName() string
	// GetMsgTime 获取消息创建时间。
	GetMsgTime() time.Time
	// GetKlinkVersion 获取消息的 Klink 版本。
	GetKlinkVersion() Version
	// Validate 进行消息的自身验证。
	Validate() error
	// setType 为消息设定类型（内部方法）。
	setType(MsgType)
}

/**********************************************************************************************/

// MsgHeader 是 Klink 协议消息头，被消息内嵌，方法被继承。
type MsgHeader struct {
	// Timestamp 代表消息产生的时间。
	Timestamp Time `json:"timestamp"`
	// MsgID 是消息的唯一标识。
	MsgID string `json:"messageId"`
	// ProductID 是消息发送/接收者设备的产品 ID。
	ProductID string `json:"productId"`
	// DeviceName 是消息发送/接收者设备的名称。
	DeviceName string `json:"deviceName"`
	// MsgType 是消息类型。
	MsgType `json:"-"`
	// KlinkVersion 是 Klink 协议版本。
	KlinkVersion Version `json:"linkVersion"`
}

func newMsgHeader(productID, deviceName string, msgType MsgType, msgID ...string) *MsgHeader {
	var messageID string
	if len(msgID) == 0 {
		messageID = uuid.NewV4().String()
	} else {
		messageID = msgID[0]
	}

	return &MsgHeader{
		Timestamp:    newTime(time.Now()),
		MsgID:        messageID,
		ProductID:    productID,
		DeviceName:   deviceName,
		MsgType:      msgType,
		KlinkVersion: KlinkVersion,
	}
}

func (h MsgHeader) GetMsgID() string {
	return h.MsgID
}

func (h MsgHeader) GetMsgTime() time.Time {
	return h.Timestamp.Time
}

func (h MsgHeader) GetProductID() string {
	return h.ProductID
}

func (h MsgHeader) GetDeviceName() string {
	return h.DeviceName
}

func (h MsgHeader) GetKlinkVersion() Version {
	return h.KlinkVersion
}

func (h MsgHeader) Validate() error {
	if h.Timestamp.IsZero() {
		return newInvalidMsgFieldError("timestamp", "missing")
	}
	if h.MsgID == "" {
		return newInvalidMsgFieldError("messageId", "missing")
	}
	if h.ProductID == "" {
		return newInvalidMsgFieldError("productId", "missing")
	}
	if h.DeviceName == "" {
		return newInvalidMsgFieldError("deviceName", "missing")
	}

	// 解析消息时，验证发送方的 major 版本是否与本地 major 版本一致，
	// 若 major 版本不一致则不兼容。
	if h.GetKlinkVersion().Major != KlinkVersion.Major {
		return ErrIncompatibleKlinkVersion
	}

	return nil
}

func (h *MsgHeader) setType(t MsgType) {
	h.MsgType = t
}

/**********************************************************************************************/

// ReplyStatus 是消息响应状态，被 reply 消息内嵌，方法被继承。
type ReplyStatus struct {
	Success bool   `json:"success"`
	ErrCode string `json:"errCode,omitempty"`
	ErrMsg  string `json:"errMsg,omitempty"`
}

// NewSuccessReply 构造成功的 ReplyStatus。
func NewSuccessReply() ReplyStatus {
	return ReplyStatus{Success: true}
}

// NewFailReply 构造失败的 ReplyFail。
func NewFailReply(errCode, errMsg string) ReplyStatus {
	return ReplyStatus{
		Success: false,
		ErrCode: errCode,
		ErrMsg:  errMsg,
	}
}

func (rs ReplyStatus) IsSuccess() bool {
	return rs.Success
}

func (rs ReplyStatus) GetErrCode() string {
	return rs.ErrCode
}

func (rs ReplyStatus) GetErrMsg() string {
	return rs.ErrMsg
}

func (rs ReplyStatus) Validate() error {
	if rs.Success && (rs.ErrCode != "" || rs.ErrMsg != "") {
		return newInvalidMsgFieldError("success", "reply status is success but has error code or error message")
	}

	if !rs.Success && (rs.ErrCode == "" || rs.ErrMsg == "") {
		return newInvalidMsgFieldError("success", "reply status failed but no error code or error message")
	}

	return nil
}

/**********************************************************************************************/

// DeviceIdentity 是设备的身份标识。
type DeviceIdentity struct {
	ProductID  string `json:"productId"`
	DeviceName string `json:"deviceName"`
}

// DstDevice 是消息发送的目标设备（消息下行时）。
type DstDevice = DeviceIdentity

// SrcDevice 用于在消息发送时，标明自身信息（消息上行时）。
type SrcDevice = DeviceIdentity

/**********************************************************************************************/

// KVs 用于 Klink 中的扩展型字段。
type KVs = map[string]interface{}

/**********************************************************************************************/

// FuncInput 是设备功能调用时的入参。
type FuncInput struct {
	Name  string      `json:"name"`
	Value interface{} `json:"value"`
}

// NewFuncInput 构建设备功能调用的参数。
// 调用方式：
//   FuncInputs(name1, value1, name2, value2, ...) // name 必须为 string 类型
// 若传递的参数不符合要求，则返回 nil。
func FuncInputs(pairs ...interface{}) []FuncInput {
	if len(pairs) == 0 {
		return nil
	}

	if len(pairs)%2 != 0 {
		return nil
	}

	s := make([]FuncInput, 0)

	for i := 0; i < len(pairs); i += 2 {
		name, ok := pairs[i].(string)
		if !ok {
			return nil
		}
		s = append(s, FuncInput{Name: name, Value: pairs[i+1]})
	}

	return s
}

/**********************************************************************************************/

// Time 为 Klink 内置时间类型，重写 JSON 序列化规则。
type Time struct {
	time.Time
}

func (t Time) MarshalJSON() (data []byte, err error) {
	data = []byte(strconv.FormatInt(t.UnixNano()/1e6, 10))
	return
}

func (t *Time) UnmarshalJSON(data []byte) error {
	numStr := string(data)

	var (
		num  int64
		numf float64
		err  error
	)

	num, err = strconv.ParseInt(numStr, 10, 64)
	if err != nil {
		// 兼顾科学计数法形式的时间戳数字，注意，是数字，timestamp 字段不可为字符串。
		numf, err = strconv.ParseFloat(numStr, 64)
		if err != nil {
			return newInvalidMsgFieldError("header.timestamp", "value type invalid")
		}
		num = int64(numf)
	}

	t.Time = time.Unix(0, int64(time.Millisecond)*num)
	return nil
}

func newTime(t time.Time) Time {
	return Time{t}
}

/**********************************************************************************************/

const (
	placeHolderProduct = "{productId}"
	placeHolderDevice  = "{deviceName}"
)

func replacePlaceholderProductDevice(tpl, productID, deviceID string) string {
	tpl = strings.ReplaceAll(tpl, placeHolderProduct, productID)
	tpl = strings.ReplaceAll(tpl, placeHolderDevice, deviceID)

	return tpl
}
