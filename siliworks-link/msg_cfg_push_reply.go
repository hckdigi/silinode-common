package link

import "encoding/json"

// MsgCfgPushReply 是云端配置推送消息的上行回复消息。
type MsgCfgPushReply struct {
	ReplyStatus           `json:"reply"`
	*MsgHeader            `json:"header"`
	BodyOfMsgCfgPushReply `json:"body"`
}

type BodyOfMsgCfgPushReply struct {
	Type     string `json:"type"`
	Response KVs    `json:"response"`
}

var _ Msg = (*MsgCfgPushReply)(nil)

func NewMsgCfgPushReply(src SrcDevice, msgID string, replyStatus ReplyStatus, cfgType string, response KVs) Msg {
	return &MsgCfgPushReply{
		ReplyStatus: replyStatus,
		MsgHeader:   newMsgHeader(src.ProductID, src.DeviceName, MsgTypeCfgPushReply, msgID),
		BodyOfMsgCfgPushReply: BodyOfMsgCfgPushReply{
			Type:     cfgType,
			Response: response,
		},
	}
}

func (m MsgCfgPushReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgCfgPushReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgCfgPushReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.Type == "" {
		return newInvalidMsgFieldError("type", "missing")
	}

	return nil
}
