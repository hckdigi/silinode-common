// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"testing"
)

func TestParser_parseMsgType(t *testing.T) {
	parser := NewParser()
	mt, err := parser.parseMsgType("/001/002/child/1/online")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log("mt", mt)
}

// func TestNewParser(t *testing.T) {
// 	parser := NewParser()
// 	parser.SetMsgHandler(MsgTypeDvcPong, func(msg Msg) {
// 		fmt.Println("in pong handler")
// 		fmt.Println(msg.GetMsgTopic())
// 		pong, ok := msg.(*MsgDvcPong)
// 		if !ok {
// 			log.Println("not ok")
// 			return
// 		}
// 		fmt.Println("pong time:", pong.MsgHeader.Timestamp.Time)
// 	})
// 	parser.SetMsgHandler(MsgTypeDvcTSLPubReply, func(msg Msg) {
// 		fmt.Println("in tsl handler")
// 	})

// 	topic := "/xxx/xxx/ping/reply"
// 	payload := []byte(`{
// 		"reply": {
// 			"success": true
// 		},
// 		"header": {
// 			"timestamp": 1631935001010,
// 			"messageId": "same_as_downstream_msg_id",
// 			"productId": "product_id_xxx",
// 			"deviceId": "device_id_xxx",
// 			"linkVersion": "v2.2.1"
// 		}
// 	}`)

// 	if err := parser.Parse(topic, payload); err != nil {
// 		log.Println("error: ", err)
// 	}
// }

// func TestParseMsgChildEventWithENotationTimestamp(t *testing.T) {
// 	parser := NewParser()
// 	parser.SetMsgHandler(MsgTypeChildEvent, func(msg Msg) {
// 		encoder := json.NewEncoder(os.Stdout)
// 		encoder.SetIndent("", "\t")
// 		encoder.Encode(msg)
// 	})

// 	topic := "/xxx/xxx/child/008dda35-3e26-4019-8a31-396d46daa073/event/TemperatureAlarm"
// 	payload := []byte(`{
//         "header": {
//                 "timestamp": 1.634018571767e+12,
//                 "messageId": "60ffd0ff-435e-4839-af0c-7dafc49c395f",
//                 "productId": "fdddd2e7-b862-4140-9ea0-680b255d8f99",
//                 "deviceId": "d5a0a5fc-1775-4c8c-ab06-6f0c27255306",
//                 "linkVersion": "v2.5.0"
//         },
//         "body": {
//                 "childDeviceId": "008dda35-3e26-4019-8a31-396d46daa073",
//                 "eventId": "TemperatureAlarm",
//                 "propName": "Temperature",
//                 "propValue": 27.1
//         }
// 	}`)

// 	if err := parser.Parse(topic, payload); err != nil {
// 		t.Error(err)
// 	}
// }
