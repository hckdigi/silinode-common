// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/16

package link

import (
	"encoding/json"
)

// MsgTransparentReply 是云端透传消息的回复消息（上行）。
type MsgTransparentReply struct {
	ReplyStatus               `json:"reply"`
	*MsgHeader                `json:"header"`
	BodyOfMsgTransparentReply `json:"body"`
}

type BodyOfMsgTransparentReply struct {
	Payload json.RawMessage `json:"payload"`
}

var _ Msg = (*MsgTransparentReply)(nil)

func NewMsgTransparentReply(src SrcDevice, msgID string, replyStatus ReplyStatus, payload json.RawMessage) Msg {
	return &MsgTransparentReply{
		ReplyStatus: replyStatus,
		MsgHeader:   newMsgHeader(src.ProductID, src.DeviceName, MsgTypeTransparentReply, msgID),
		BodyOfMsgTransparentReply: BodyOfMsgTransparentReply{
			Payload: payload,
		},
	}
}

func (m MsgTransparentReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgTransparentReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgTransparentReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Payload) == 0 {
		return newInvalidMsgFieldError("payload", "missing")
	}

	return nil
}
