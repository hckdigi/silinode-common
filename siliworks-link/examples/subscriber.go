// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package examples

import (
	"errors"
	"log"

	"gitee.com/hckdigi/silinode-common/siliworks-link"
)

func SubscriberDemo() {
	// 构建消息解析器，并注册不同消息的处理单元。
	// 所有的消息类型都以 `MsgType` 作为前缀，方便 editor/IDE 查找到你想要的类型。
	parser := link.NewParser().
		SetMsgHandler(link.MsgTypeDvcReadProp, DvcReadPropHandler).
		SetMsgHandler(link.MsgTypeDvcWriteProp, DvcWritePropHandler).
		SetMsgHandler(link.MsgTypeDvcFuncInvk, DvcFuncInvkHandler)

	// 模拟订阅的场景，将 topic 与 payload 传给 parser，parser 内部会做消息
	// 的类型识别、消息解析、协议格式验证，之后根据消息类型调用用户注册的处理单元。
	new(mqttSubscriber).Subscribe("/myProductId/myDeviceId/#", func(topic string, payload []byte) {
		err := parser.Parse(topic, payload)

		if errors.Is(err, link.ErrNonKlinkMsg) {
			// 处理非 link 协议的消息...
			return
		}

		if errors.Is(err, link.ErrInvalidMsgField) {
			// 发来的消息字段不符合规范，在此处理...
			return
		}

		if errors.Is(err, link.ErrIncompatibleKlinkVersion) {
			// 发送方的 Klink 协议版本与本地版本不兼容，在此处理...
			return
		}

		if err != nil {
			// 处理解析过程中出现的其他错误...
			return
		}
	})
}

// 以下为不同消息的处理单元，类似于 gin 的 handler，
// 在处理逻辑内部需做消息类型的断言转换，这样才可以
// 提取到消息内部参数。

func DvcReadPropHandler(msg link.Msg) {
	// 类型转换。所有类型的消息都以 `Msg` 作为前缀，方便 editor/IDE 查找到你想要的消息。
	// 需要注意，msg 底层类型为消息的指针。
	msgDvcReadProp, _ := msg.(*link.MsgDvcReadProp)
	log.Printf("corex want to read these props: %v", msgDvcReadProp.Props)

	// ...
}

func DvcWritePropHandler(msg link.Msg) {
	// ...
}

func DvcFuncInvkHandler(msg link.Msg) {
	// ...
}

// mqttSubscriber 模拟 MQTT 消息订阅。
type mqttSubscriber struct{}

func (s mqttSubscriber) Subscribe(topic string, handler func(topic string, payload []byte)) {
	// ...
}
