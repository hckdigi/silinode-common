// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package examples

import "gitee.com/hckdigi/silinode-common/siliworks-link"

func PublisherDemo() {
	// 目标设备
	dst := link.DstDevice{ProductID: "edgex", DeviceName: "edgex001"}

	// 构建消息，所有的消息构造函数都以 `NewMsg` 为前缀，方便 editor/IDE 查找到你想要的构建函数。
	msg := link.NewMsgDvcReadProp(dst, []string{"cpu", "mem"})

	// 获取消息所对应的 MQ Topic 和消息内容数据
	topic, payload := msg.GetMsgTopic(), msg.GetPayload()

	// 发布消息
	new(mqttPublisher).Publish(topic, payload)
}

// mqttPublisher 模拟 MQTT 发布者。
type mqttPublisher struct{}

func (p mqttPublisher) Publish(topic string, payload []byte) error {
	// ...
	return nil
}
