// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"encoding/json"
)

// MsgDvcWriteProp 是修改设备属性消息。
type MsgDvcWriteProp struct {
	*MsgHeader            `json:"header"`
	BodyOfMsgDvcWriteProp `json:"body"`
}

type BodyOfMsgDvcWriteProp struct {
	Props KVs `json:"props"`
}

var _ Msg = (*MsgDvcWriteProp)(nil)

func NewMsgDvcWriteProp(dst DstDevice, props KVs) Msg {
	return &MsgDvcWriteProp{
		MsgHeader:             newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeDvcWriteProp),
		BodyOfMsgDvcWriteProp: BodyOfMsgDvcWriteProp{Props: props},
	}
}

func (m MsgDvcWriteProp) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgDvcWriteProp) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgDvcWriteProp) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Props) == 0 {
		return newInvalidMsgFieldError("properties", "empty")
	}

	return nil
}
