package link

import (
	"encoding/json"
	"testing"
)

func Test_newVersion(t *testing.T) {
	v := newVersion(1, 2, 3)
	b, err := json.Marshal(v)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(string(b))

	var ver Version
	if err := json.Unmarshal(b, &ver); err != nil {
		t.Error(err)
		return
	}

	t.Log(ver)
}
