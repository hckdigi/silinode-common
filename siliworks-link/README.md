<div align=center><img width="30%" height="30%" src="./logo.png" /></div>

<div align=center>

# Klink 协议 Go 语言实现

KLink 是基于 MQTT 的业务层协议，用于 IoT 中直连设备或网关设备与云平台之间的通讯。此仓库为 KLink 的 Go 实现。

</div>

## 安装

因为本仓库 URL 层级太多，Go 目前仅支持三段式的 Module 名称，后面的字符会全被当作版本字符处理，所以需要做一些特殊操作来安装本包。

##### 1. 修改 `go.mod` 文件，加入如下几行

```
replace (
    gitee.com/hckdigi/silinode-common/siliworks-link => gitlab.kaiwuren.com/iotcloud/iotcloudmod/link-go.git/v2 v2.0.2 // 实际使用的是该版本号，此处非固定 v2.0.2，请使用最新的版本
)

require gitee.com/hckdigi/silinode-common/siliworks-link v2.0.0 // 此版本号随意写，只要 major 版本与 Module Name 一致即可
```

##### 2. 执行命令

```
$ go mod download
$ go mod tidy
```

## 使用

### 发布端

```go
package examples

import "gitlab.kaiwuren.com/iotcloud/iotcloudmod/contracts/v2/link"

func main() {
	// 目标设备
	dst := link.DstDevice{ProductID: "edgex", DeviceID: "edgex001"}

	// 构建消息，所有的消息构造函数都以 `NewMsg` 为前缀，方便 editor/IDE 查找到你想要的构建函数。
	msg := link.NewMsgDvcReadProp(dst, []string{"cpu", "mem"})

	// 获取消息所对应的 MQ Topic 和消息内容数据
	topic, payload := msg.GetMsgTopic(), msg.GetPayload()

	// 发布消息
	new(mqttPublisher).Publish(topic, payload)
}

// mqttPublisher 模拟 MQTT 发布者。
type mqttPublisher struct{}

func (p mqttPublisher) Publish(topic string, payload []byte) error {
	// ...
	return nil
}

```

### 订阅端

```go
package examples

import (
	"errors"
	"log"

	"gitlab.kaiwuren.com/iotcloud/iotcloudmod/contracts/v2/link"
)

func main() {
	// 构建消息解析器，并注册不同消息的处理单元。
	// 所有的消息类型都以 `MsgType` 作为前缀，方便 editor/IDE 查找到你想要的类型。
	parser := link.NewParser().
		SetMsgHandler(link.MsgTypeDvcReadProp, DvcReadPropHandler).
		SetMsgHandler(link.MsgTypeDvcWriteProp, DvcWritePropHandler).
		SetMsgHandler(link.MsgTypeDvcFuncInvk, DvcFuncInvkHandler)

	// 模拟订阅的场景，将 topic 与 payload 传给 parser，parser 内部会做消息
	// 的类型识别、消息解析、协议格式验证，之后根据消息类型调用用户注册的处理单元。
	new(mqttSubscriber).Subscribe("/myProductId/myDeviceId/#", func(topic string, payload []byte) {
		err := parser.Parse(topic, payload)

		if errors.Is(err, link.ErrNonKlinkMsg) {
			// 处理非 link 协议的消息...
			return
		}

		if errors.Is(err, link.ErrInvalidMsgField) {
			// 发来的消息字段不符合规范，在此处理...
			return
		}

		if errors.Is(err, link.ErrIncompatibleKlinkVersion) {
			// 发送方的 Klink 协议版本与本地版本不兼容，在此处理...
			return
		}

		if err != nil {
			// 处理解析过程中出现的其他错误...
		}
	})
}

// 以下为不同消息的处理单元，类似于 gin 的 handler，
// 在处理逻辑内部需做消息类型的断言转换。

func DvcReadPropHandler(msg link.Msg) {
	// 类型转换。所有类型的消息都以 `Msg` 作为前缀，方便 editor/IDE 查找到你想要的消息。
	// 需要注意，msg 底层类型为消息的指针。
	msgDvcReadProp, _ := msg.(*link.MsgDvcReadProp)
	log.Printf("corex want to read these props: %v", msgDvcReadProp.Properties)

	// ...
}

func DvcWritePropHandler(msg link.Msg) {
	// ...
}

func DvcFuncInvkHandler(msg link.Msg) {
	// ...
}

// mqttSubscriber 模拟 MQTT 消息订阅。
type mqttSubscriber struct{}

func (s mqttSubscriber) Subscribe(topic string, handler func(topic string, payload []byte)) {
	// ...
}

```

## 接口文档

```
$ go doc # 具体使用方法见 `go help doc`。
```
