// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"encoding/json"
)

// MsgDvcFuncInvk 是调用设备功能消息（下行）。
type MsgDvcFuncInvk struct {
	*MsgHeader           `json:"header"`
	BodyOfMsgDvcFuncInvk `json:"body"`
}

type BodyOfMsgDvcFuncInvk struct {
	Func   string      `json:"func"`
	Inputs []FuncInput `json:"inputs"`
}

var _ Msg = (*MsgDvcFuncInvk)(nil)

// NewMsgDvcFuncInvk 构造调用设备功能消息，inputs 参数可使用 FuncInputs 工具函数构造。
func NewMsgDvcFuncInvk(dst DstDevice, funcName string, inputs []FuncInput) Msg {
	return &MsgDvcFuncInvk{
		MsgHeader: newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeDvcFuncInvk),
		BodyOfMsgDvcFuncInvk: BodyOfMsgDvcFuncInvk{
			Func:   funcName,
			Inputs: inputs,
		},
	}
}

func (m MsgDvcFuncInvk) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgDvcFuncInvk) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgDvcFuncInvk) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.Func == "" {
		return newInvalidMsgFieldError("func", "missing")
	}

	return nil
}
