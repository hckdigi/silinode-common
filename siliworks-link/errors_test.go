package link

import (
	"errors"
	"testing"
)

func TestNewInvalidMsgFieldError(t *testing.T) {
	err := newInvalidMsgFieldError("foo", "too long")
	if !errors.Is(err, ErrInvalidMsgField) {
		t.Fail()
		return
	}

	t.Log(errors.Unwrap(err))
	t.Log(err)
}
