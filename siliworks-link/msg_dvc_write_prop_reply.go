// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/08/31

package link

import (
	"encoding/json"
)

// MsgDvcWritePropReply 是修改设备属性（回复）消息。
type MsgDvcWritePropReply struct {
	ReplyStatus                `json:"reply"`
	*MsgHeader                 `json:"header"`
	BodyOfMsgDvcWritePropReply `json:"body"`
}

type BodyOfMsgDvcWritePropReply struct {
	Props KVs `json:"props"`
}

var _ Msg = (*MsgDvcWritePropReply)(nil)

func NewMsgDvcWritePropReply(src SrcDevice, msgID string, replyStatus ReplyStatus, props KVs) Msg {
	return &MsgDvcWritePropReply{
		ReplyStatus: replyStatus,
		MsgHeader:   newMsgHeader(src.ProductID, src.DeviceName, MsgTypeDvcWritePropReply, msgID),
		BodyOfMsgDvcWritePropReply: BodyOfMsgDvcWritePropReply{
			Props: props,
		},
	}
}

func (m MsgDvcWritePropReply) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgDvcWritePropReply) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgDvcWritePropReply) Validate() error {
	if err := m.ReplyStatus.Validate(); err != nil {
		return err
	}

	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.IsSuccess() && len(m.Props) == 0 {
		return newInvalidMsgFieldError("props", "empty")
	}

	return nil
}
