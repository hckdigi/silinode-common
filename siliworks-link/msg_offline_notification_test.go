package link

import (
	"testing"
)

func TestNewMsgOfflineNotification(t *testing.T) {
	msg := NewMsgOfflineNotification(DstDevice{
		ProductID:  "foo",
		DeviceName: "bar",
	}, "offline reason tip")
	t.Log(msg.GetMsgTopic(), string(msg.GetPayload()))
}
