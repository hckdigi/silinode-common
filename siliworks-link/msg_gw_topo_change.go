package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwTopoChange 是网关设备与网关子设备之间拓扑关系变更的消息（下行）。
type MsgGwTopoChange struct {
	*MsgHeader            `json:"header"`
	BodyOfMsgGwTopoChange `json:"body"`
}

type BodyOfMsgGwTopoChange struct {
	Subdvcs []SubdvcChange `json:"devices"`
}

// SubdvcChange 代表发生变更的子设备，以及变更的类型和内容。
type SubdvcChange struct {
	DeviceIdentity
	Status int `json:"status"`
	Config KVs `json:"config"`
}

var _ Msg = (*MsgGwTopoChange)(nil)

// NewMsgGwTopoChange 构建拓扑关系变化消息，传入消息目的地 dst，和变更项 subdvcs。
func NewMsgGwTopoChange(dst DstDevice, subdvcs []SubdvcChange) Msg {
	return &MsgGwTopoChange{
		MsgHeader: newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeGwTopoChange),
		BodyOfMsgGwTopoChange: BodyOfMsgGwTopoChange{
			Subdvcs: subdvcs,
		},
	}
}

func (m MsgGwTopoChange) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwTopoChange) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwTopoChange) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Subdvcs) == 0 {
		return newInvalidMsgFieldError("devices", "empty")
	}

	for i, dvc := range m.Subdvcs {
		if dvc.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if dvc.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}
		if dvc.Status == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].status", i), "missing")
		}
		if dvc.Status < 1 || dvc.Status > 3 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].status", i), "must be 1, 2 or 3")
		}
		if dvc.Status == 3 && len(dvc.Config) == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].config", i), "missing")
		}
	}

	return nil
}
