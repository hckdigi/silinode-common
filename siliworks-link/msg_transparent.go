package link

import "encoding/json"

// MsgTransparent 是云端透传消息（下行）。
type MsgTransparent struct {
	*MsgHeader           `json:"header"`
	BodyOfMsgTransparent `json:"body"`
}

type BodyOfMsgTransparent struct {
	Payload json.RawMessage `json:"payload"`
}

var _ Msg = (*MsgTransparent)(nil)

func NewMsgTransparent(dst DstDevice, payload json.RawMessage) Msg {
	return &MsgTransparent{
		MsgHeader: newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeTransparent),
		BodyOfMsgTransparent: BodyOfMsgTransparent{
			Payload: payload,
		},
	}
}

func (m MsgTransparent) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgTransparent) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgTransparent) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Payload) == 0 {
		return newInvalidMsgFieldError("payload", "missing")
	}

	return nil
}
