package link

import "encoding/json"

// MsgCfgPush 是云端配置推送消息（下行）。
type MsgCfgPush struct {
	*MsgHeader       `json:"header"`
	BodyOfMsgCfgPush `json:"body"`
}

type BodyOfMsgCfgPush struct {
	Type   string `json:"type"`
	Config KVs    `json:"config"`
}

var _ Msg = (*MsgCfgPush)(nil)

func NewMsgCfgPush(dst DstDevice, cfgType string, config KVs) Msg {
	return &MsgCfgPush{
		MsgHeader: newMsgHeader(dst.ProductID, dst.DeviceName, MsgTypeCfgPush),
		BodyOfMsgCfgPush: BodyOfMsgCfgPush{
			Type:   cfgType,
			Config: config,
		},
	}
}

func (m MsgCfgPush) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgCfgPush) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgCfgPush) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if m.Type == "" {
		return newInvalidMsgFieldError("type", "missing")
	}
	if len(m.Config) == 0 {
		return newInvalidMsgFieldError("config", "missing")
	}

	return nil
}
