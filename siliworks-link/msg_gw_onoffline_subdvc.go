// @author Xavier Zhao <xvrzhao@gmail.com>
// @date 2021/12/14

package link

import (
	"encoding/json"
	"fmt"
)

// MsgGwOnofflineSubdvc 是网关代理子设备上下线消息（上行）。
type MsgGwOnofflineSubdvc struct {
	*MsgHeader                 `json:"header"`
	BodyOfMsgGwOnofflineSubdvc `json:"body"`
}

type BodyOfMsgGwOnofflineSubdvc struct {
	Devices []SubdvcOnoffline `json:"devices"`
}

// SubdvcOnoffline 代表网关子设备的上下线请求。
type SubdvcOnoffline struct {
	DeviceIdentity
	Status int `json:"status"`
}

var _ Msg = (*MsgGwOnofflineSubdvc)(nil)

func NewMsgGwOnofflineSubdvc(src SrcDevice, subdvcs []SubdvcOnoffline) Msg {
	return &MsgGwOnofflineSubdvc{
		MsgHeader: newMsgHeader(src.ProductID, src.DeviceName, MsgTypeGwOnofflineSubdvc),
		BodyOfMsgGwOnofflineSubdvc: BodyOfMsgGwOnofflineSubdvc{
			Devices: subdvcs,
		},
	}
}

func (m MsgGwOnofflineSubdvc) GetPayload() []byte {
	b, _ := json.Marshal(m)
	return b
}

func (m MsgGwOnofflineSubdvc) GetMsgTopic() string {
	return replacePlaceholderProductDevice(m.getTopicTpl(), m.ProductID, m.DeviceName)
}

func (m MsgGwOnofflineSubdvc) Validate() error {
	if err := m.MsgHeader.Validate(); err != nil {
		return err
	}

	if len(m.Devices) == 0 {
		return newInvalidMsgFieldError("devices", "empty")
	}

	for i, dvc := range m.Devices {
		if dvc.ProductID == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].productId", i), "missing")
		}
		if dvc.DeviceName == "" {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].deviceName", i), "missing")
		}
		if dvc.Status == 0 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].status", i), "missing")
		}
		if dvc.Status != 1 && dvc.Status != -1 {
			return newInvalidMsgFieldError(fmt.Sprintf("devices[%d].status", i), "neither 1 nor -1")
		}
	}

	return nil
}
