package link

import (
	"fmt"
	"strconv"
	"strings"
)

// KlinkVersion 声明 Klink 版本号。
//
// 注意，对于增加协议消息的版本，不需要升级 major 版本，只需升级 minor 版本。
// 对于 删除协议消息 / 增加消息字段 / 移除消息字段 的情况，属于不兼容性更新，
// 会对上下游造成影响，故应当升级 major 版本。解析消息时，已集成对 major 版
// 本的检验。
var KlinkVersion Version = newVersion(2, 3, 15)

type Version struct {
	Major uint8
	Minor uint8
	Patch uint8
}

func newVersion(major, minor, patch uint8) Version {
	return Version{
		Major: major,
		Minor: minor,
		Patch: patch,
	}
}

func newVersionFromString(s string) Version {
	parts := strings.Split(s, ".")
	if len(parts) != 3 {
		return Version{Major: 255} // major 255 表示非法版本
	}

	parts[0] = strings.TrimPrefix(parts[0], "v")

	major, errMajor := strconv.Atoi(parts[0])
	minor, errMinor := strconv.Atoi(parts[1])
	patch, errPatch := strconv.Atoi(parts[2])

	if errMajor != nil || errMinor != nil || errPatch != nil {
		return Version{Major: 255}
	}

	return Version{
		Major: uint8(major),
		Minor: uint8(minor),
		Patch: uint8(patch),
	}
}

func (v Version) String() string {
	return fmt.Sprintf("v%d.%d.%d", v.Major, v.Minor, v.Patch)
}

func (v Version) MarshalJSON() (data []byte, err error) {
	s := fmt.Sprintf(`"%s"`, v.String())
	data = []byte(s)
	return
}

func (v *Version) UnmarshalJSON(data []byte) error {
	s := strings.Trim(string(data), `"`)
	ver := newVersionFromString(s)

	v.Major = ver.Major
	v.Minor = ver.Minor
	v.Patch = ver.Patch

	return nil
}
