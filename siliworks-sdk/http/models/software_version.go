package models

// SoftwareType 软件类型。
//   FirmwareType: 固件
//   ImageType: 微服务镜像
//   EMAType: EMA 可执行程序
//   MCUType: MCU
type SoftwareType uint8

const (
	FirmwareType SoftwareType = iota + 1
	ImageType
	EMAType
	MCUType
)

type SoftwareVersion struct {
	ID          string `json:"id"`
	Name        string `json:"name"`        // 名称
	Version     string `json:"version"`     // 版本
	Path        string `json:"path"`        // 云端文件虚拟路径
	FileSize    string `json:"fileSize"`    // 文件大小
	Arch        string `json:"framework"`   // 平台架构
	Description string `json:"description"` // 描述
	Status      int    `json:"status"`      // 状态 1 发布 2 下架
	CreatedAt   string `json:"createdAt"`   // 创建时间
	FileMold    int    `json:"fileMold"`    // 文件上传类型 1 系统提交 2 用户提交
	TaskMold    int    `json:"taskMold"`    // 软件类型 1 固件 2 镜像 3 EMA 可执行文件 4 MCU
}
