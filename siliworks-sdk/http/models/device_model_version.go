package models

type DeviceModelVersion struct {
	ID             string `json:"id"`
	Name           string `json:"name"`
	Manufacturer   string `json:"manufacturer"`
	EquipmentType  string `json:"equipmentType"`
	EquipmentModel string `json:"equipmentModel"`
	Version        string `json:"version"`
	Path           string `json:"path"`
	CreatedAt      string `json:"createdAt"`
	UpdatedAt      string `json:"updatedAt"`
	Describe       string `json:"describe"`
	Used           bool   `json:"used"`
}
