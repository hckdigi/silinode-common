package models

type DynamicRegisterResult struct {
	EncryptionType int    `json:"encryptionType"`
	DevicePSK      string `json:"devicePSK"`
	DeviceCrtPem   string `json:"deviceCrtPem"`
	DeviceKeyPem   string `json:"deviceKeyPem"`
}
