package models

type DeviceModelWithVersions struct {
	Manufacturer   string `json:"manufacturer"`
	EquipmentType  string `json:"equipmentType"`
	EquipmentModel string `json:"equipmentModel"`
	Versions       []struct {
		ID          string `json:"id"`
		Name        string `json:"name"`
		Version     string `json:"version"`
		CreatedAt   string `json:"createdAt"`
		Model       int    `json:"model"`
		Description string `json:"description"`
		Path        string `json:"path"`
	} `json:"rows"`
}
