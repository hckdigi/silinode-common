package models

import "encoding/json"

type DeviceProfileMeta struct {
	Manufacturer   string          `json:"manufacturer"`
	EquipmentType  string          `json:"equipmentType"`
	EquipmentModel string          `json:"equipmentModel"`
	Classification int             `json:"classification"`
	FileName       string          `json:"fileName"`
	Model          int             `json:"model"`
	Name           string          `json:"name"`
	Version        string          `json:"version"`
	Describe       string          `json:"describe"`
	Category       string          `json:"category"`
	FileSize       string          `json:"fileSize"`
	Configuration  json.RawMessage `json:"configuration"`
}
