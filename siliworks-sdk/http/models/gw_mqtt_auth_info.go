package models

type GwMQTTAuthInfo struct {
	ProductID      string `json:"productId"`
	DeviceName     string `json:"deviceName"`
	DeviceNickname string `json:"deviceNickname"`
	EncryptionType int    `json:"encryptionType"`
	DevicePSK      string `json:"devicePSK"`
	DeviceCrtPem   string `json:"deviceCrtPem"`
	DeviceKeyPem   string `json:"deviceKeyPem"`
	ProductSecret  string `json:"productSecret"`
}
