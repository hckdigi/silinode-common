package models

// CoreXUploadTask 是一个 CoreX 平台的上传任务。
type CoreXUploadTask struct {
	URL   string `json:"url"`
	Token string `json:"token"`
}
