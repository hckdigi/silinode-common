package models

type DeviceModel struct {
	Model          int           `json:"model"`
	Label          string        `json:"label"`
	Value          string        `json:"value"`
	Classification int           `json:"classification"`
	Children       []DeviceModel `json:"children"`
}
