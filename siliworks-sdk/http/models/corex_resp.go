package models

import "encoding/json"

// CorexResp 是 CoreX HTTP 返回的外层结构。
type CorexResp struct {
	Code int             `json:"code"`
	Msg  string          `json:"msg"`
	Data json.RawMessage `json:"data"`
}
