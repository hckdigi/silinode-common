package models

type SubdvcAuthInfo struct {
	ProductID      string `json:"productId"`
	DeviceName     string `json:"deviceName"`
	DeviceNickname string `json:"deviceNickname"`
	DevicePSK      string `json:"devicePSK"`
}
