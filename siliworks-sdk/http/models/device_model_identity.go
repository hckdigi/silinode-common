package models

type DeviceModelIdentity struct {
	Manufacturer   string `json:"manufacturer"`
	EquipmentType  string `json:"equipmentType"`
	EquipmentModel string `json:"equipmentModel"`
}
