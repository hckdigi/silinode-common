package models

import "encoding/json"

type AlgorithmResource struct {
	ID            string          `json:"id"`
	Name          string          `json:"name"`
	Version       string          `json:"version"`
	Path          string          `json:"path"`
	Description   string          `json:"description"`
	Category      int             `json:"category"`
	FileSize      string          `json:"fileSize"`
	FileMd5       string          `json:"fileMd5"`
	Configuration json.RawMessage `json:"configuration"`
	CreatedAt     string          `json:"createdAt"`
	Level         int             `json:"level"`
}
