package client

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"crypto/tls"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"runtime"
	"strconv"
	"strings"
	"time"

	cerrors "gitee.com/hckdigi/silinode-common/siliworks-sdk/http/errors"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/http/models"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/private/utils"
)

type client struct {
	host string
	port int
	opts clientOptions
}

// NewClient 构造 HTTP 客户端。
func NewClient(host string, port int, opts ...ClientOption) (Client, error) {
	cli := &client{
		host: host,
		port: port,
	}

	for _, opt := range opts {
		opt.apply(&cli.opts)
	}

	if err := cli.check(); err != nil {
		return nil, err
	}

	return cli, nil
}

func (cli client) check() error {
	if cli.host == "" {
		return errNoHost
	}

	if cli.port == 0 {
		return errNoPort
	}

	if (cli.opts.authType == headerAuth && (cli.opts.productID == "" || cli.opts.deviceName == "" || cli.opts.devicePSK == "")) ||
		(cli.opts.authType == tlsAuth && (cli.opts.httpsPort == 0 || cli.opts.cliCrtFile == "" || cli.opts.cliKeyFile == "")) {
		return errInvalidAuthArgs
	}

	return nil
}

func (cli client) hasSetTimeout() bool {
	return cli.opts.timeout != 0
}

func (cli client) getBaseURL() string {
	if cli.opts.authType == tlsAuth {
		return fmt.Sprintf("https://%s:%d", cli.host, cli.opts.httpsPort)
	}

	return fmt.Sprintf("http://%s:%d", cli.host, cli.port)
}

func (cli client) getAuthHeader(productID, deviceName, devicePSK string) (http.Header, error) {
	rawPSK, err := base64.StdEncoding.DecodeString(devicePSK)
	if err != nil {
		return nil, fmt.Errorf("failed to decode(base64) devicePSK: %w", err)
	}

	clientId := productID + deviceName
	userName := fmt.Sprintf("%s;%s;%s;%d", clientId, "ignore", "ignore", time.Now().Add(time.Minute*10).Unix())

	h := hmac.New(sha256.New, rawPSK)
	h.Write([]byte(userName))
	password := hex.EncodeToString(h.Sum(nil))

	header := make(http.Header)
	header.Set("clientId", clientId)
	header.Set("userName", userName)
	header.Set("password", password)

	return header, nil
}

func (cli client) getHTTPClient() (*http.Client, error) {
	if cli.opts.authType == tlsAuth {

		cliCrt, err := tls.LoadX509KeyPair(cli.opts.cliCrtFile, cli.opts.cliKeyFile)
		if err != nil {
			return nil, fmt.Errorf("failed to load key pair: %w", err)
		}

		return &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					Certificates:       []tls.Certificate{cliCrt},
					InsecureSkipVerify: true,
				},
			},
		}, nil
	}

	return http.DefaultClient, nil
}

func (cli client) request(method, url string, reqBody, respData interface{}) error {
	c, err := cli.getHTTPClient()
	if err != nil {
		return fmt.Errorf("failed to get http client: %w", err)
	}

	buf := new(bytes.Buffer)
	if reqBody != nil {
		if err := json.NewEncoder(buf).Encode(reqBody); err != nil {
			return fmt.Errorf("failed to encode req body: %w", err)
		}
	}

	var req *http.Request
	if cli.hasSetTimeout() {
		ctx, cancel := context.WithTimeout(context.Background(), cli.opts.timeout)
		defer cancel()

		req, err = http.NewRequestWithContext(ctx, method, url, buf)
		if err != nil {
			return fmt.Errorf("failed to new request: %w", err)
		}
	} else {
		req, err = http.NewRequest(method, url, buf)
		if err != nil {
			return fmt.Errorf("failed to new request: %w", err)
		}
	}

	if cli.opts.authType == headerAuth {
		header, err := cli.getAuthHeader(cli.opts.productID, cli.opts.deviceName, cli.opts.devicePSK)
		if err != nil {
			return fmt.Errorf("failed to get auth header: %w", err)
		}
		req.Header = header
	}

	resp, err := c.Do(req)
	if err != nil {
		return fmt.Errorf("failed to send request: %w", err)
	}
	defer resp.Body.Close()

	bs, _ := ioutil.ReadAll(resp.Body)
	// corex 200、401 和 417 均有业务 body
	if resp.StatusCode != http.StatusOK &&
		resp.StatusCode != http.StatusUnauthorized &&
		resp.StatusCode != http.StatusExpectationFailed {
		return fmt.Errorf("failed to request corex: status code %d, %s", resp.StatusCode, bs)
	}

	var cr models.CorexResp
	if err := json.Unmarshal(bs, &cr); err != nil {
		return fmt.Errorf("failed to decode response: %w", err)
	}

	if cr.Code != 0 {
		return fmt.Errorf("failed to request corex, response: %w", cerrors.NewCoreXBusinessError(cr.Code, cr.Msg))
	}

	if respData == nil {
		return nil
	}

	if err := json.Unmarshal(cr.Data, respData); err != nil {
		return fmt.Errorf("failed to decode response data: %w", err)
	}

	return nil
}

func (cli client) GetGwMQTTAuthInfoByDSN(DSN string) (*models.GwMQTTAuthInfo, error) {
	url := strings.ReplaceAll(urlGetGwMQTTAuthInfoByDSN, "{dsn}", url.QueryEscape(DSN))
	url = cli.getBaseURL() + url

	var respStr string
	if err := cli.request(http.MethodGet, url, nil, &respStr); err != nil {
		return nil, fmt.Errorf("failed to request: %w", err)
	}

	// hex decode
	encrypted, err := hex.DecodeString(respStr)
	if err != nil {
		return nil, fmt.Errorf("failed to decode(hex) authinfo from corex: %w", err)
	}

	// aes decrypt
	decrypted, err := utils.AESDecryptCBC(encrypted, []byte(corexAesKey))
	if err != nil {
		return nil, fmt.Errorf("failed to decrypt authinfo from corex: %w", err)
	}

	// json decode
	var info models.GwMQTTAuthInfo
	if err := json.Unmarshal(decrypted, &info); err != nil {
		return nil, fmt.Errorf("failed to unmarshal response from corex: %w", err)
	}

	return &info, nil
}

func (cli client) GetSubdvcAuthInfoByDN(deviceName string) (*models.SubdvcAuthInfo, error) {
	url := strings.ReplaceAll(urlGetSubdvcAuthInfoByDN, "{dn}", url.QueryEscape(deviceName))
	url = cli.getBaseURL() + url

	var respStr string
	if err := cli.request(http.MethodGet, url, nil, &respStr); err != nil {
		return nil, fmt.Errorf("failed to request: %w", err)
	}

	// hex decode
	encrypted, err := hex.DecodeString(respStr)
	if err != nil {
		return nil, fmt.Errorf("failed to decode(hex) authinfo from corex: %w", err)
	}

	// aes decrypt
	decrypted, err := utils.AESDecryptCBC(encrypted, []byte(corexAesKey))
	if err != nil {
		return nil, fmt.Errorf("failed to decrypt authinfo from corex: %w", err)
	}

	// json decode
	var info models.SubdvcAuthInfo
	if err := json.Unmarshal(decrypted, &info); err != nil {
		return nil, fmt.Errorf("failed to unmarshal response from corex: %w", err)
	}

	return &info, nil
}

func (cli client) GetMQTTConnFailDescription(productID, deviceName string) (string, error) {
	var (
		description string
		errBusiness cerrors.CoreXBusinessError
	)

	url := cli.getBaseURL() + strings.ReplaceAll(urlGetMQTTConnFailDescription, "{client_id}", url.QueryEscape(productID+deviceName))
	err := cli.request(http.MethodGet, url, nil, &description)

	if errors.As(err, &errBusiness) {
		return "unknown error (not found from corex)", nil
	}

	if err != nil {
		return "", fmt.Errorf("cli.request failed: %w", err)
	}

	return description, nil
}

func (cli client) DynamicRegister(deviceName, productID, productSecret string) (*models.DynamicRegisterResult, error) {
	nonce := utils.RandNum(4)
	timestamp := strconv.FormatInt(time.Now().Unix(), 10)

	values := make(url.Values)
	values.Set("productId", productID)
	values.Set("deviceName", deviceName)
	values.Set("nonce", nonce)
	values.Set("timestamp", timestamp)

	rawSign := utils.HmacSha256(values.Encode(), productSecret)
	signature := base64.StdEncoding.EncodeToString([]byte(rawSign)) // 最终签名

	bodyMap := map[string]string{
		"productId":  productID,
		"deviceName": deviceName,
		"nonce":      nonce,
		"timestamp":  timestamp,
		"signature":  signature,
	}

	var respStr string
	if err := cli.request(http.MethodPost, cli.getBaseURL()+urlDynamicRegister, bodyMap, &respStr); err != nil {
		return nil, fmt.Errorf("failed to request: %w", err)
	}

	// base64 decode
	bs, err := base64.StdEncoding.DecodeString(respStr)
	if err != nil {
		return nil, fmt.Errorf("failed to base64 decode data in response body: %w", err)
	}

	// AES decrypt
	bs, err = utils.AESDecryptCBC(bs, []byte(productSecret[:16]))
	if err != nil {
		return nil, fmt.Errorf("failed to aes decrypt data in response body: %w", err)
	}

	var res models.DynamicRegisterResult
	if err = json.NewDecoder(bytes.NewBuffer(bs)).Decode(&res); err != nil {
		return nil, fmt.Errorf("failed to json decode data in response body: %w", err)
	}

	return &res, nil
}

func (cli client) GetDeviceModels() ([]models.DeviceModel, error) {
	url := cli.getBaseURL() + urlGetDeviceModels

	var dms []models.DeviceModel
	cli.request(http.MethodGet, url, nil, &dms)
	return dms, nil
}

func (cli client) GetVersionsOfDeviceModel(deviceModelID string) ([]models.DeviceModelVersion, error) {
	url := cli.getBaseURL() + strings.ReplaceAll(urlGetVersionsOfDeviceModel, "{device_model_id}", url.PathEscape(deviceModelID))

	var res struct {
		Versions []models.DeviceModelVersion `json:"rows"`
		Total    int                         `json:"count"`
	}
	err := cli.request(http.MethodGet, url, nil, &res)
	return res.Versions, err
}

func (cli client) GetVersionsOfMultipleDeviceModels(deviceModels []models.DeviceModelIdentity) ([]models.DeviceModelWithVersions, error) {
	url := cli.getBaseURL() + urlGetVersionsOfMultipleDeviceModels

	var res struct {
		DeviceModels []models.DeviceModelWithVersions `json:"rows"`
	}
	err := cli.request(http.MethodPost, url, map[string]interface{}{"list": deviceModels}, &res)
	return res.DeviceModels, err
}

func (cli client) GetDownloadURLByPath(path string) (string, error) {
	url := cli.getBaseURL() + strings.ReplaceAll(urlGetDownloadURLByPath, "{path}", url.QueryEscape(path))

	var downloadURL string
	err := cli.request(http.MethodGet, url, nil, &downloadURL)
	return downloadURL, err
}

func (cli client) GetFileContentByPath(path string) (string, error) {
	url := cli.getBaseURL() + strings.ReplaceAll(urlGetDownloadURLByPath, "{path}", url.QueryEscape(path))

	var downloadURL string
	if err := cli.request(http.MethodGet, url, nil, &downloadURL); err != nil {
		return "", err
	}

	resp, err := http.Get(downloadURL)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	content, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(content), nil
}

func (cli client) CreateUploadTaskOfDeviceProfile(deviceProfileMeta models.DeviceProfileMeta) (*models.CoreXUploadTask, error) {
	url := cli.getBaseURL() + urlCreateUploadTaskOfDeviceProfile

	var task models.CoreXUploadTask
	err := cli.request(http.MethodPost, url, deviceProfileMeta, &task)
	return &task, err
}

func (cli client) CompleteUploadTaskOfResource(token string) error {
	url := cli.getBaseURL() + urlCompleteResourceUploadTask
	return cli.request(http.MethodPost, url, map[string]string{"token": token}, nil)
}

func (cli client) CreateUploadTaskOfDeviceFile(fileBaseName string) (*models.CoreXUploadTask, error) {
	url := cli.getBaseURL() + strings.ReplaceAll(urlCreateUploadTaskOfDeviceFile, "{filename}", url.QueryEscape(fileBaseName))

	var task models.CoreXUploadTask
	err := cli.request(http.MethodGet, url, nil, &task)
	return &task, err
}

func (cli client) CompleteUploadTaskOfDeviceFile(tokens []string) ([]string, error) {
	url := cli.getBaseURL() + urlCompleteDeviceFileUploadTask

	var paths []string
	err := cli.request(http.MethodPost, url, map[string][]string{"tokens": tokens}, &paths)
	return paths, err
}

func (cli client) GetAlgorithmResources() ([]models.AlgorithmResource, error) {
	url := cli.getBaseURL() + urlGetAlgorithmResources

	var res struct {
		AlgorithmResources []models.AlgorithmResource `json:"rows"`
		Total              int                        `json:"count"`
	}

	err := cli.request(http.MethodGet, url, nil, &res)
	return res.AlgorithmResources, err
}

func (cli client) GetSoftwares(softwareType models.SoftwareType) ([]models.SoftwareVersion, error) {
	filter := func(key, value interface{}) string {
		return fmt.Sprintf(`{"name":"%s","value":"%v"}`, key, value)
	}

	query := make(url.Values)
	query.Add("page", "1")
	query.Add("limit", "999")
	query.Add("filters[]", filter("taskMold", softwareType))
	query.Add("filters[]", filter("status", 1))
	query.Add("filters[]", filter("fileMold", 1))
	query.Add("filters[]", filter("arch", runtime.GOARCH))

	url := cli.getBaseURL() + strings.ReplaceAll(urlGetSoftwares, "{query}", query.Encode())

	var res struct {
		Rows  []models.SoftwareVersion `json:"rows"`
		Count int                      `json:"count"`
	}
	err := cli.request(http.MethodGet, url, nil, &res)
	return res.Rows, err
}
