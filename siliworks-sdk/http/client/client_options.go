package client

import "time"

type authType uint8

const (
	headerAuth authType = iota + 1
	tlsAuth
)

type clientOptions struct {
	authType authType

	productID  string
	deviceName string
	devicePSK  string

	httpsPort  int
	cliCrtFile string
	cliKeyFile string

	timeout time.Duration
}

// ClientOption 是构造 HTTP 客户端的可选项。
type ClientOption interface {
	apply(*clientOptions)
}

type funcClientOption struct {
	f func(*clientOptions)
}

func newFuncClientOption(f func(*clientOptions)) *funcClientOption {
	return &funcClientOption{
		f: f,
	}
}

func (fco *funcClientOption) apply(co *clientOptions) {
	fco.f(co)
}

// WithHeaderAuth 指定使用请求头的认证方式。不指定认证方式则走 HTTP 无请求头的请求，
// 如在最初通过网关设备序列码（DSN）请求设备认证信息时，则不需要指定。
func WithHeaderAuth(productID, deviceName, devicePSK string) ClientOption {
	return newFuncClientOption(func(co *clientOptions) {
		co.authType = headerAuth
		co.productID = productID
		co.deviceName = deviceName
		co.devicePSK = devicePSK
	})
}

// WithTLSAuth 指定使用证书的认证方式。不指定认证方式则走 HTTP 无请求头的请求，
// 如在最初通过网关设备序列码（DSN）请求设备认证信息时，则不需要指定。
func WithTLSAuth(httpsPort int, cliCrtFile, cliKeyFile string) ClientOption {
	return newFuncClientOption(func(co *clientOptions) {
		co.authType = tlsAuth
		co.httpsPort = httpsPort
		co.cliCrtFile = cliCrtFile
		co.cliKeyFile = cliKeyFile
	})
}

// WithTimeout 指定请求超时时间，不指定则不进行超时控制。
func WithTimeout(d time.Duration) ClientOption {
	return newFuncClientOption(func(co *clientOptions) {
		co.timeout = d
	})
}
