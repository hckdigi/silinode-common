package client

const (
	urlDynamicRegister                   = "/cloud/api/corex/v2/bootstrap/register/dev"
	urlGetGwMQTTAuthInfoByDSN            = "/cloud/api/corex/v2/device/pre/create?deviceNumber={dsn}"
	urlGetSubdvcAuthInfoByDN             = "/cloud/api/corex/v2/device/pre/bind?deviceNumber={dn}"
	urlGetMQTTConnFailDescription        = "/cloud/api/corex/v2/device/error?src=mqtt&key={client_id}"
	urlGetDeviceModels                   = "/cloud/api/corex/v2/system/product-domain/list?tree=true"
	urlGetVersionsOfDeviceModel          = "/cloud/api/corex/v2/system/resource/{device_model_id}/published/list?model=2&merge=false"
	urlGetVersionsOfMultipleDeviceModels = "/cloud/api/corex/v2/system/product-domain/resource/dp/list"
	urlGetDownloadURLByPath              = "/cloud/api/corex/v2/system/resource/download?path={path}"
	urlCreateUploadTaskOfDeviceProfile   = "/cloud/api/corex/v2/system/resource/with-product-info/url"
	urlCreateUploadTaskOfDeviceFile      = "/cloud/api/corex/v2/system/upload-address?fileName={filename}"
	urlCompleteResourceUploadTask        = "/cloud/api/corex/v2/system/resource/complete"
	urlCompleteDeviceFileUploadTask      = "/cloud/api/corex/v2/system/device/files/upload/complete"
	urlGetAlgorithmResources             = "/cloud/api/corex/v2/system/resource/algorithm/list"
	urlGetSoftwares                      = "/cloud/api/corex/v2/system/task/firmware/list?{query}"
)

const (
	corexAesKey = "IN51ePBoEezPzfEw"
)
