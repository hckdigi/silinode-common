package client

import "errors"

var (
	errNoHost          = errors.New("no host set")
	errNoPort          = errors.New("no port set")
	errInvalidAuthArgs = errors.New("with invalid auth arguments")
)
