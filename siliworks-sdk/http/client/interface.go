package client

import "gitee.com/hckdigi/silinode-common/siliworks-sdk/http/models"

// Client 是与 CoreX 进行 HTTP 交互的客户端。
// 建议每次交互时都进行实例化，不建议共用，因为 Client 尚未提供变更认证信息的接口。
type Client interface {
	/**
	 * 无需认证的接口
	 */

	// DynamicRegister 根据设备名称、产品 ID、产品密钥获取设备认证信息。
	DynamicRegister(deviceName, productID, productSecret string) (*models.DynamicRegisterResult, error)
	// GetGwMQTTAuthInfoByDSN 根据网关 DSN 获取网关认证信息。
	GetGwMQTTAuthInfoByDSN(DSN string) (*models.GwMQTTAuthInfo, error)
	// GetMQTTConnFailDescription 获取网关 MQTT 建连失败的具体原因描述。
	GetMQTTConnFailDescription(productID, deviceName string) (string, error)

	/**
	 * 需要认证的接口
	 */

	// GetSubdvcAuthInfoByDN 根据子设备名称获取子设备认证信息。
	GetSubdvcAuthInfoByDN(deviceName string) (*models.SubdvcAuthInfo, error)
	// GetDeviceModels 获取云端设备型号列表。
	GetDeviceModels() ([]models.DeviceModel, error)
	// GetVersionsOfDeviceModel 查询设备型号的版本列表。
	GetVersionsOfDeviceModel(deviceModelID string) ([]models.DeviceModelVersion, error)
	// GetVersionsOfMultipleDeviceModels 批量查询设备型号的版本列表。
	GetVersionsOfMultipleDeviceModels(deviceModels []models.DeviceModelIdentity) ([]models.DeviceModelWithVersions, error)
	// GetAlgorithmResources 获取算法资源列表。
	GetAlgorithmResources() ([]models.AlgorithmResource, error)
	// GetDownloadURLByPath 根据云端文件路径获取下载地址。
	GetDownloadURLByPath(path string) (string, error)
	// GetFileContentByPath 根据云端文件路径获取文件内容。
	GetFileContentByPath(path string) (string, error)
	// CreateUploadTaskOfDeviceProfile 创建设备模板上传任务。
	CreateUploadTaskOfDeviceProfile(deviceProfileMeta models.DeviceProfileMeta) (*models.CoreXUploadTask, error)
	// CompleteUploadTaskOfResource 通知资源类上传任务完成。
	CompleteUploadTaskOfResource(token string) error
	// CreateUploadTaskOfDeviceFile 创建设备文件类上传任务。
	CreateUploadTaskOfDeviceFile(fileBaseName string) (*models.CoreXUploadTask, error)
	// CompleteUploadTaskOfDeviceFile 通知设备文件类上传任务完成，返回各任务对应的云端文件路径。
	CompleteUploadTaskOfDeviceFile(tokens []string) ([]string, error)
	// GetSoftwares 根据类型获取软件列表。
	GetSoftwares(softwareType models.SoftwareType) ([]models.SoftwareVersion, error)
}
