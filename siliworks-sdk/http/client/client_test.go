package client

import (
	"encoding/json"
	"fmt"
	"net/url"
	"testing"
	"time"

	"gitee.com/hckdigi/silinode-common/siliworks-sdk/http/models"
)

func Test_client_GetGwMQTTAuthInfoByDSN(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	info, err := cli.GetGwMQTTAuthInfoByDSN("HCK_N101P_220428_0555")
	if err != nil {
		t.Error(err)
		return
	}

	t.Logf("%+v", info)
}

func Test_client_GetSubdvcAuthInfoByDN(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("VXWRR9UCK6", "HCK-N101P-2220-0002", "NDA3MDc3ODU3OTMyNjA3NDk2"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	info, err := cli.GetSubdvcAuthInfoByDN("400846166822027269")
	if err != nil {
		t.Error(err)
		return
	}

	t.Logf("%+v", info)
}

func Test_client_GetMQTTConnFailDescription(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	description, err := cli.GetMQTTConnFailDescription("xvrzhao", "20220818")
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(description)
}

func Test_client_GetDeviceModels(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	dms, err := cli.GetDeviceModels()
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(dms)
}

func Test_client_GetVersionsOfDeviceModel(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	dmvs, err := cli.GetVersionsOfDeviceModel("b1574e7b-a48e-463e-b883-2bfa3db9c6aa")
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(dmvs)
}

func Test_client_GetVersionsOfMultipleDeviceModels(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	ms, err := cli.GetVersionsOfMultipleDeviceModels([]models.DeviceModelIdentity{
		{
			Manufacturer:   "hckdigi",
			EquipmentType:  "camera-ipc",
			EquipmentModel: "HIK DS-2CD3T47EWDV3-L",
		}, {
			Manufacturer:   "hckdigi",
			EquipmentType:  "camera-nvr",
			EquipmentModel: "HIK DS-2DE2D40IW-D3",
		},
	})
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(ms)
}

func Test_client_GetFileContentByPath(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	content, err := cli.GetFileContentByPath("system/device_profile/417370281338732910/camera-ipc-device.yaml")
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(content)
}

func Test_client_CreateUploadTaskOfDeviceProfile(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	task, err := cli.CreateUploadTaskOfDeviceProfile(models.DeviceProfileMeta{
		Manufacturer:   "test",
		EquipmentType:  "test",
		EquipmentModel: "test",
		Classification: 1,
		FileName:       "test",
		Model:          3,
		Name:           "test",
		Version:        "v1.2.3",
		Describe:       "test",
		Category:       "test",
		FileSize:       "test",
		Configuration:  nil,
	})

	if err != nil {
		t.Error(err)
		return
	}

	t.Log(task)
}

func Test_client_CompleteUploadTaskOfResource(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	if err := cli.CompleteUploadTaskOfResource("422297779725926766"); err != nil {
		t.Error(err)
		return
	}
}

func Test_client_CreateUploadTaskOfDeviceFile(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	task, err := cli.CreateUploadTaskOfDeviceFile("xvrzhao.jpg")
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println(task)
}

func Test_client_CompleteUploadTaskOfDeviceFile(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	paths, err := cli.CompleteUploadTaskOfDeviceFile([]string{"422587918977073518"})
	if err != nil {
		t.Error(err)
		return
	}

	fmt.Println(paths)
}

func Test_client_GetAlgorithmResources(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	algos, err := cli.GetAlgorithmResources()
	if err != nil {
		t.Error(err)
		return
	}

	b, err := json.MarshalIndent(algos, "", "  ")
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(string(b))
}

func Test_client_GetDownloadURLByPath(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	url, err := cli.GetDownloadURLByPath(url.QueryEscape("system/device_profile/422299120292594030/system/device_profile/422299120292594030/mqtt.test.device.profile (2).yaml"))
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(url)
}

func Test_client_GetSoftwares(t *testing.T) {
	cli, err := NewClient("172.16.13.6", 80, WithHeaderAuth("D4G1TDJZPF", "HCK-N101D-2201-0099", "NDIxNTM3MzA4MDM0OTkwMzUw"), WithTimeout(time.Second))
	if err != nil {
		t.Error(err)
		return
	}

	list, err := cli.GetSoftwares(models.ImageType)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log("count:", len(list))
	jb, _ := json.MarshalIndent(list, "", "  ")
	t.Log(string(jb))
}
