package errors

import "fmt"

type CoreXBusinessError struct {
	Code int
	Msg  string
}

var _ error = (*CoreXBusinessError)(nil)

func NewCoreXBusinessError(code int, msg string) CoreXBusinessError {
	return CoreXBusinessError{
		Code: code,
		Msg:  msg,
	}
}

func (e CoreXBusinessError) Error() string {
	return fmt.Sprintf("%s (code %d)", e.Msg, e.Code)
}

func (e CoreXBusinessError) GetCode() int {
	return e.Code
}

func (e CoreXBusinessError) GetMsg() string {
	return e.Msg
}
