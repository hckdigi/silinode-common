package errors

import (
	"errors"
	"fmt"
	"testing"
)

func TestNewCoreXBusinessError(t *testing.T) {
	err := fmt.Errorf("my error: %w", NewCoreXBusinessError(3, "something wrong"))

	var myerror CoreXBusinessError
	if errors.As(err, &myerror) {
		fmt.Println(myerror.GetCode(), myerror.GetMsg())
	} else {
		fmt.Println("false")
	}
}
