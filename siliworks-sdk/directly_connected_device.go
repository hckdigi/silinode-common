package corexdevicesdk

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/http/client"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/private"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/private/utils"
	"gitee.com/hckdigi/silinode-common/siliworks-link"
)

// DirectlyConnectedDevice 代表一个抽象的直连设备，直连设备的开发者应当根据构造函数获取到该实例，通过
// 实例提供的接口，实现与云端的通信。该接口所有方法均并发安全。
type DirectlyConnectedDevice interface {
	// SetCloudInfo 设置云端平台信息。
	SetCloudInfo(mqttBrokerHost, mqttTCPPort, mqttTLSPort, httpHost, httpPort string)
	// SetAuthInfo 设置设备认证信息。
	SetAuthInfo(authType uint8, productID, productSecret, deviceName, devicePSK, deviceCrtFile, deviceKeyFile string)
	// GetDeviceInfo 获取设备信息。
	GetDeviceInfo() (productID, deviceName string)
	// SetHeartbeatInterval 设置设备心跳上报周期，不设置则不上报心跳。
	SetHeartbeatInterval(time.Duration)
	// GetHeartbeatInterval 获取心跳上报周期，若未设置则返回零值。
	GetHeartbeatInterval() time.Duration
	// Online 设备上线。
	Online() error
	// UpdateConnection 更新设备连接，在连接状态再次调用 SetBrokerURL 或 SetAuthInfo 方法，应当调用此方法来更新连接。
	// wasOnline 代表更新前是否在线。
	// err 为 nil 表示更新上线成功；非 nil 表示更新上线失败，此时为离线状态。
	UpdateConnection() (wasOnline bool, err error)
	// Offline 断开与云端的连接，设备下线。返回之前是否在线。
	Offline() (wasOnline bool)
	// IsOnline 判断设备是否在线。
	IsOnline() bool
	// ReportEvent 上报设备事件。
	ReportEvent(eventID string, eventTime time.Time, eventProps map[string]interface{}) error
	// ReportConfig 上报设备端的配置信息
	ReportConfig(cfgType string, cfgBody map[string]interface{}) error
	// QueryConfig 向云端查询该设备的配置信息。
	QueryConfig(cfgType string) (cfgBody map[string]interface{}, err error)
	// SubscribeMQTTTopic 订阅 MQTT 主题消息。
	SubscribeMQTTTopic(topic string, handler func(topic string, payload []byte) error) error
	// UnsubscribeMQTTTopic 取消某个 MQTT 主题的消息。
	UnsubscribeMQTTTopics(topics ...string) error
	// PublishMQTTTopic 发布 MQTT 主题消息。
	PublishMQTTTopic(topic string, payload interface{}) error
}

func NewDirectlyConnectedDevice(handlers TopicHandlersOfDirectlyConnectedDevice) DirectlyConnectedDevice {
	return &directlyConnectedDevice{
		readingPropsHandler:        handlers.ReadPropsHandler,
		writingPropsHandler:        handlers.WritePropsHandler,
		invokingFuncHandler:        handlers.InvokeFuncHandler,
		pushingConfigHandler:       handlers.PushConfigHandler,
		readingConfigHandler:       handlers.ReadConfigHandler,
		offlineNotificationHandler: handlers.OfflineNotificationHandler,

		heartbeatThreadWg: new(sync.WaitGroup),
	}
}

type directlyConnectedDevice struct {
	// 云端地址

	mqttBrokerHost string
	mqttTCPPort    string
	mqttTLSPort    string
	httpHost       string
	httpPort       string

	// 设备认证相关信息

	productID      string
	productSecret  string
	deviceName     string
	devicePSK      string
	deviceCrtFile  string
	deviceKeyFile  string
	deviceAuthType uint8 // 认证方式 1 设备密钥 2 设备证书 3 动态注册

	// MQTT 连接

	mqtt.Client
	heartbeatInterval     time.Duration
	heartbeatThreadCtx    context.Context
	heartbeatThreadCancel context.CancelFunc
	heartbeatThreadWg     *sync.WaitGroup

	// 主题消息处理逻辑

	readingPropsHandler        ReadPropsHandler
	writingPropsHandler        WritePropsHandler
	invokingFuncHandler        InvokeFuncHandler
	pushingConfigHandler       PushConfigHandler
	readingConfigHandler       ReadConfigHandler
	offlineNotificationHandler OfflineNotificationHandler
}

var _ DirectlyConnectedDevice = (*directlyConnectedDevice)(nil)

func (dvc *directlyConnectedDevice) SetCloudInfo(mqttBrokerHost, mqttTCPPort, mqttTLSPort, httpHost, httpPort string) {
	dvc.mqttBrokerHost = mqttBrokerHost
	dvc.mqttTCPPort = mqttTCPPort
	dvc.mqttTLSPort = mqttTLSPort
	dvc.httpHost = httpHost
	dvc.httpPort = httpPort
}

func (dvc *directlyConnectedDevice) getMqttTcpAddr() string {
	return fmt.Sprintf("tcp://%s:%s", dvc.mqttBrokerHost, dvc.mqttTCPPort)
}

func (dvc *directlyConnectedDevice) getMqttTLSAddr() string {
	return fmt.Sprintf("tls://%s:%s", dvc.mqttBrokerHost, dvc.mqttTLSPort)
}

// SetAuthInfo 设置设备认证信息，参数项根据 authType 选填，非必须项填入空值即可。
func (dvc *directlyConnectedDevice) SetAuthInfo(authType uint8, productID, productSecret, deviceName, devicePSK, deviceCrtFile, deviceKeyFile string) {
	dvc.deviceAuthType = authType
	dvc.productID = productID
	dvc.productSecret = productSecret
	dvc.deviceName = deviceName
	dvc.devicePSK = devicePSK
	dvc.deviceCrtFile = deviceCrtFile
	dvc.deviceKeyFile = deviceKeyFile
}

func (dvc *directlyConnectedDevice) GetDeviceInfo() (productID, deviceName string) {
	productID = dvc.productID
	deviceName = dvc.deviceName
	return
}

func (dvc *directlyConnectedDevice) SetHeartbeatInterval(d time.Duration) {
	dvc.heartbeatInterval = d
}

func (dvc *directlyConnectedDevice) GetHeartbeatInterval() time.Duration {
	return dvc.heartbeatInterval
}

// handleConnLost 处理断连，退出心跳协程。
func (dvc *directlyConnectedDevice) handleConnLost(c mqtt.Client, err error) {
	utils.LogError("MQTT connection lost: %v", err)

	if dvc.heartbeatThreadCancel != nil {
		dvc.heartbeatThreadCancel()
	}
	dvc.heartbeatThreadWg.Wait()
}

func (dvc *directlyConnectedDevice) Online() error {
	var (
		err  error
		opts *mqtt.ClientOptions
	)

	handlers := TopicHandlersOfDirectlyConnectedDevice{
		ReadPropsHandler:           dvc.readingPropsHandler,
		WritePropsHandler:          dvc.writingPropsHandler,
		InvokeFuncHandler:          dvc.invokingFuncHandler,
		PushConfigHandler:          dvc.pushingConfigHandler,
		ReadConfigHandler:          dvc.readingConfigHandler,
		OfflineNotificationHandler: dvc.offlineNotificationHandler,
	}

	if dvc.deviceAuthType == 1 {

		if dvc.productID == "" || dvc.deviceName == "" || dvc.devicePSK == "" {
			return errors.New("missing auth info")
		}

		opts, err = getMQTTCliOptsOfPSKType(dvc.getMqttTcpAddr(), dvc.productID, dvc.deviceName, dvc.devicePSK,
			getOnConnectHandler(dvc, true, handlers, false, TopicHandlersOfGatewayDevice{}, nil), dvc.handleConnLost)
		if err != nil {
			return fmt.Errorf("failed to getMQTTCliOptsOfPSKType: %w", err)
		}

	} else if dvc.deviceAuthType == 2 {

		if dvc.productID == "" || dvc.deviceName == "" || dvc.deviceCrtFile == "" || dvc.deviceKeyFile == "" {
			return errors.New("missing auth info")
		}

		crt, err := tls.LoadX509KeyPair(dvc.deviceCrtFile, dvc.deviceKeyFile)
		if err != nil {
			return fmt.Errorf("failed to LoadX509KeyPair: %w", err)
		}

		opts, err = getMQTTCliOptsOfCrtType(dvc.getMqttTLSAddr(), dvc.productID, dvc.deviceName, crt,
			getOnConnectHandler(dvc, true, handlers, false, TopicHandlersOfGatewayDevice{}, nil), dvc.handleConnLost)
		if err != nil {
			return fmt.Errorf("failed to getMQTTCliOptsOfCrtFileType: %w", err)
		}

	} else if dvc.deviceAuthType == 3 {

		if dvc.productID == "" || dvc.productSecret == "" || dvc.deviceName == "" {
			return errors.New("missing auth info")
		}

		// 请求动态注册接口，获取设备认证信息
		res, err := dynamicRegister(dvc.httpHost, dvc.httpPort, dvc.productID, dvc.deviceName, dvc.productSecret)
		if err != nil {
			return fmt.Errorf("failed to dynamicRegister: %w", err)
		}

		if res.EncryptionType == 1 {

			// 证书认证方式

			if len(res.DeviceCrtPem) == 0 || len(res.DeviceKeyPem) == 0 {
				return fmt.Errorf("device crt pem or device key pem in response is empty")
			}

			crt, err := tls.X509KeyPair([]byte(res.DeviceCrtPem), []byte(res.DeviceKeyPem))
			if err != nil {
				return fmt.Errorf("failed to X509KeyPair: %w", err)
			}

			opts, err = getMQTTCliOptsOfCrtType(dvc.getMqttTLSAddr(), dvc.productID, dvc.deviceName, crt,
				getOnConnectHandler(dvc, true, handlers, false, TopicHandlersOfGatewayDevice{}, nil), dvc.handleConnLost)
			if err != nil {
				return fmt.Errorf("failed to getMQTTCliOptsOfCrtFileType: %w", err)
			}

		} else if res.EncryptionType == 2 {

			// 签名认证方式

			if len(res.DevicePSK) == 0 {
				return fmt.Errorf("device PSK in response is empty")
			}

			opts, err = getMQTTCliOptsOfPSKType(dvc.getMqttTcpAddr(), dvc.productID, dvc.deviceName, res.DevicePSK,
				getOnConnectHandler(dvc, true, handlers, false, TopicHandlersOfGatewayDevice{}, nil), dvc.handleConnLost)
			if err != nil {
				return fmt.Errorf("failed to getMQTTCliOptsOfPSKType: %w", err)
			}

		} else {
			return fmt.Errorf("invalid encryption type from IoT platform")
		}

	} else {
		return fmt.Errorf("unsupported auth type: %d", dvc.deviceAuthType)
	}

	dvc.Client = mqtt.NewClient(opts)
	token := dvc.Client.Connect().(*mqtt.ConnectToken)

	if !token.WaitTimeout(internal.MQTTWaitTimeout) {
		return errors.New("wait ack timeout")
	}

	if token.ReturnCode() == 254 {
		return errors.New("network error")
	}

	if token.ReturnCode() != 0 {
		port, _ := strconv.Atoi(dvc.httpPort)
		httpcli, err := client.NewClient(dvc.mqttBrokerHost, port, client.WithTimeout(time.Second*5))
		if err != nil {
			utils.LogError("new http client failed: %v", err)
			return fmt.Errorf("ACK Code %d", token.ReturnCode())
		}

		description, err := httpcli.GetMQTTConnFailDescription(dvc.productID, dvc.deviceName)
		if err != nil {
			utils.LogError("httpcli.GetMQTTConnFailDescription failed: %v", err)
			return fmt.Errorf("ACK Code %d", token.ReturnCode())
		}

		return errors.New(description)
	}

	return nil
}

// UpdateConnection 更新连接，重设 BrokerURL 或 设备认证信息 后，需要调用此方法。
func (dvc *directlyConnectedDevice) UpdateConnection() (wasOnline bool, err error) {
	wasOnline = dvc.Offline()
	err = dvc.Online()
	return
}

func (dvc *directlyConnectedDevice) Offline() (wasOnline bool) {
	if dvc.IsOnline() {
		wasOnline = true
	}

	// 主动下线不会触发 ConnLost 回调，
	// 此处主动停止心跳协程
	if dvc.heartbeatThreadCancel != nil {
		dvc.heartbeatThreadCancel()
	}
	dvc.heartbeatThreadWg.Wait()

	if dvc.Client != nil {
		dvc.Client.Disconnect(1000) // 异步断连
		time.Sleep(time.Second)     // 等待断连，防止未完全断连时，后续操作就把 Client 实例回收
		if !dvc.IsOnline() {
			utils.LogInfo("MQTT Client OFFLINE SUCCESSFULLY !!!")
		} else {
			utils.LogError("MQTT Client OFFLINE FAILED !!!")
		}
	}

	return
}

func (dvc *directlyConnectedDevice) IsOnline() bool {
	if dvc.Client == nil {
		return false
	}

	return dvc.Client.IsConnected() && dvc.Client.IsConnectionOpen()
}

func (dvc *directlyConnectedDevice) ReportEvent(eventID string, eventTime time.Time, eventProps map[string]interface{}) error {
	// 检测 dvc 连接状态
	if !dvc.IsOnline() {
		return errors.New("device is not online")
	}

	src := link.SrcDevice{
		ProductID:  dvc.productID,
		DeviceName: dvc.deviceName,
	}

	msg := link.NewMsgDvcEvent(src, eventID, eventTime, eventProps)

	token := dvc.Publish(msg.GetMsgTopic(), 0, false, msg.GetPayload())
	if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
		return fmt.Errorf("failed to pushlish message: timeout")
	}
	if token.Error() != nil {
		return fmt.Errorf("failed to pushlish message: %w", token.Error())
	}

	return nil
}

func (dvc *directlyConnectedDevice) ReportConfig(cfgType string, cfgBody map[string]interface{}) error {
	// 检测 dvc 连接状态
	if !dvc.IsOnline() {
		return errors.New("device is not online")
	}

	src := link.SrcDevice{
		ProductID:  dvc.productID,
		DeviceName: dvc.deviceName,
	}

	msg := link.NewMsgCfgReport(src, cfgType, cfgBody)

	token := dvc.Publish(msg.GetMsgTopic(), 0, false, msg.GetPayload())
	if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
		return fmt.Errorf("failed to pushlish message: timeout")
	}
	if token.Error() != nil {
		return fmt.Errorf("failed to pushlish message: %w", token.Error())
	}

	return nil
}

func (dvc *directlyConnectedDevice) QueryConfig(cfgType string) (cfgBody map[string]interface{}, err error) {
	// 检测 dvc 连接状态
	if !dvc.IsOnline() {
		return nil, errors.New("device is not online")
	}

	panic("not implemented") // TODO: 需要等待回复消息
}

func (dvc *directlyConnectedDevice) SubscribeMQTTTopic(topic string, handler func(topic string, payload []byte) error) error {
	// 检测 dvc 连接状态
	if !dvc.IsOnline() {
		return errors.New("device is not online")
	}

	token := dvc.Subscribe(topic, 0, func(c mqtt.Client, m mqtt.Message) {
		if err := handler(m.Topic(), m.Payload()); err != nil {
			utils.LogError("failed to call user's handler of topic: %s", m.Topic())
		}
	})

	if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
		return fmt.Errorf("failed to subscribe topic: timeout")
	}
	if token.Error() != nil {
		return fmt.Errorf("failed to subscribe topic: %w", token.Error())
	}

	return nil
}

func (dvc *directlyConnectedDevice) UnsubscribeMQTTTopics(topics ...string) error {
	// 检测 dvc 连接状态
	if !dvc.IsOnline() {
		return errors.New("device is not online")
	}

	token := dvc.Client.Unsubscribe(topics...)
	if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
		return fmt.Errorf("failed to subscribe topic: timeout")
	}
	if token.Error() != nil {
		return fmt.Errorf("failed to unsubscribe topics: %w", token.Error())
	}

	return nil
}

func (dvc *directlyConnectedDevice) PublishMQTTTopic(topic string, payload interface{}) error {
	// 检测 dvc 连接状态
	if !dvc.IsOnline() {
		return errors.New("device is not online")
	}

	token := dvc.Publish(topic, 0, false, payload)
	if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
		return fmt.Errorf("failed to pushlish message: timeout")
	}
	if token.Error() != nil {
		return fmt.Errorf("failed to pushlish message: %w", token.Error())
	}

	return nil
}
