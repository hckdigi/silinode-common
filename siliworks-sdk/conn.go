package corexdevicesdk

import (
	"context"
	"crypto/tls"
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/http/client"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/http/models"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/private"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/private/utils"
	"gitee.com/hckdigi/silinode-common/siliworks-link"
)

// getOnConnectHandler 获取 MQTT 连接成功钩子函数（重连后依旧执行）。
//
// 入参说明：
//
//   device																直连/网关自身实例
//   isDirectlyConnectedDevice, topicHandlersOfDirectlyConnectedDevice	直连设备相关
//   isGatewayDevice, topicHandlersOfGatewayDevice, gw					网关设备相关
func getOnConnectHandler(device *directlyConnectedDevice,
	isDirectlyConnectedDevice bool, topicHandlersOfDirectlyConnectedDevice TopicHandlersOfDirectlyConnectedDevice,
	isGatewayDevice bool, topicHandlersOfGatewayDevice TopicHandlersOfGatewayDevice, gw *gatewayDevice) mqtt.OnConnectHandler {

	if isDirectlyConnectedDevice {
		// 构造 KLink Parser，实例在函数执行过后不会被销毁，因为返回出的闭包中存在引用。
		parser := getKLinkParserOfDirectlyConnectedDevice(device.productID, device.deviceName, topicHandlersOfDirectlyConnectedDevice, device, false, gw, nil, nil)

		return func(c mqtt.Client) {
			utils.LogInfo("MQTT connection established.")

			topic := getDeviceRootTopic(device.productID, device.deviceName)
			utils.LogInfo("Start to subscribe topics(%s) of directly connected device.", topic)

			// 订阅直连设备主题
			token := c.Subscribe(topic, 0, func(c mqtt.Client, m mqtt.Message) {
				err := parser.Parse(m.Topic(), m.Payload())
				if errors.Is(err, link.ErrNonKlinkMsg) {
					utils.LogError("KLink parser error: receive non-KLink message, topic: %s, payload: %s", m.Topic(), m.Payload())
					return
				}
				if errors.Is(err, link.ErrInvalidMsgField) {
					utils.LogError("KLink parser error: receive invalid KLink message, %v", err)
					return
				}
				if errors.Is(err, link.ErrIncompatibleKlinkVersion) {
					utils.LogError("KLink parser error: receive incompatible KLink message: %v, local version: %s", err, link.KlinkVersion)
					return
				}
				if err != nil {
					utils.LogError("KLink parser error: %v", err)
					return
				}
			})

			isReadyToHeartbeat := true
			if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
				utils.LogError("Failed to subscribe topics(%s) of directly connected device: timeout", topic)
				isReadyToHeartbeat = false
			}
			if token.Error() != nil {
				utils.LogError("Failed to subscribe topics(%s) of directly connected device: %v", topic, token.Error())
				isReadyToHeartbeat = false
			}

			// 订阅成功，启动心跳协程
			if isReadyToHeartbeat {
				device.heartbeatThreadCtx, device.heartbeatThreadCancel = context.WithCancel(context.Background()) // 每次启动前重置，因为之前的 ctx 已经 done。
				device.heartbeatThreadWg.Add(1)
				go heartbeatThread(device)
			}
		}
	}

	if isGatewayDevice {
		// 网关自身消息解析器
		parser := getKLinkParserOfDirectlyConnectedDevice(device.productID, device.deviceName, topicHandlersOfDirectlyConnectedDevice, device,
			true, gw, topicHandlersOfGatewayDevice.TopoChangeHandler, topicHandlersOfGatewayDevice.TopoQueryReplyHandler)

		// 子设备消息解析器
		subdvcParsers := getKLinkParsersOfSubdvcs(true, gw, false, nil, topicHandlersOfGatewayDevice)

		return func(c mqtt.Client) {
			gw.incNumOfConn()
			utils.LogInfo("MQTT connection established(times: %d).", gw.getNumOfConn())

			// 1. 订阅网关自身主题

			topic := getDeviceRootTopic(device.productID, device.deviceName)
			utils.LogInfo("Start to subscribe topics(%s) of gateway device.", topic)
			token := c.Subscribe(topic, 0, func(c mqtt.Client, m mqtt.Message) {
				err := parser.Parse(m.Topic(), m.Payload())
				if errors.Is(err, link.ErrNonKlinkMsg) {
					utils.LogError("KLink parser error: receive non-KLink message, topic: %s, payload: %s", m.Topic(), m.Payload())
					return
				}
				if errors.Is(err, link.ErrInvalidMsgField) {
					utils.LogError("KLink parser error: receive invalid KLink message, %v", err)
					return
				}
				if errors.Is(err, link.ErrIncompatibleKlinkVersion) {
					utils.LogError("KLink parser error: receive incompatible KLink message: %v, local version: %s", err, link.KlinkVersion)
					return
				}
				if err != nil {
					utils.LogError("KLink parser error: %v", err)
					return
				}
			})

			isReadyToHeartbeat := true

			if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
				utils.LogError("Failed to subscribe topics(%s) of gateway device: timeout", topic)
				isReadyToHeartbeat = false
			}
			if token.Error() != nil {
				utils.LogError("Failed to subscribe topics(%s) of gateway device: %v", topic, token.Error())
				isReadyToHeartbeat = false
			}

			// 2. 订阅所有子设备的主题

			for topic, parser := range subdvcParsers {
				utils.LogInfo("Start to subscribe topics(%s) of sub device.", topic)

				token := c.Subscribe(topic, 0, func(c mqtt.Client, m mqtt.Message) {
					err := parser.Parse(m.Topic(), m.Payload())
					if errors.Is(err, link.ErrNonKlinkMsg) {
						utils.LogError("KLink parser error: receive non-KLink message, topic: %s, payload: %s", m.Topic(), m.Payload())
						return
					}
					if errors.Is(err, link.ErrInvalidMsgField) {
						utils.LogError("KLink parser error: receive invalid KLink message, %v", err)
						return
					}
					if errors.Is(err, link.ErrIncompatibleKlinkVersion) {
						utils.LogError("KLink parser error: receive incompatible KLink message: %v, local version: %s", err, link.KlinkVersion)
						return
					}
					if err != nil {
						utils.LogError("KLink parser error: %v", err)
						return
					}
				})

				if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
					utils.LogError("Failed to subscribe topics(%s) of sub device: timeout", topic)
					isReadyToHeartbeat = false
				}
				if token.Error() != nil {
					utils.LogError("Failed to subscribe topics(%s) of sub device: %v", topic, token.Error())
					isReadyToHeartbeat = false
				}
			}

			/**
			 * 首次建连的情况，子设备需要绑定并上线；
			 * 断线重连的情况，子设备无需绑定只需上线。（网关连接断线之后，云端的子设备绑定关系还在，但其子设备都被下线。）
			 */

			// 3. 执行首次建连钩子

			if gw.getNumOfConn() == 1 {
				utils.LogInfo("OnConnect | FirstConnectedHook execution starts ...")
				gw.execOnFirstConnectedHook()
				utils.LogInfo("OnConnect | FirstConnectedHook execution ends ...")
			}

			// 4. 子设备上线

			var subdvcs []DeviceIdentity
			for _, subdvc := range gw.subdvcs {
				if gw.isSubdvcValid == nil || (gw.isSubdvcValid != nil && gw.isSubdvcValid(subdvc.DeviceName)) {
					subdvcs = append(subdvcs, DeviceIdentity{subdvc.ProductID, subdvc.DeviceName})
				}
			}

			utils.LogInfo("OnConnect | Making sub-devices(%v) online ...", subdvcs)
			if err := gw.SubdvcsOnline(subdvcs...); err != nil {
				utils.LogError("OnConnect | Make sub-devices online failed: %s", err.Error())
			} else {
				utils.LogInfo("OnConnect | Make sub-devices online, pushlished the sub-devices online message")
			}

			// 5. 启动心跳协程

			if isReadyToHeartbeat {
				device.heartbeatThreadCtx, device.heartbeatThreadCancel = context.WithCancel(context.Background()) // 每次启动前重置，因为之前的 ctx 已经 done。
				device.heartbeatThreadWg.Add(1)
				go heartbeatThread(device)
			}
		}
	}

	return nil // 错误情况
}

func heartbeatThread(dvc *directlyConnectedDevice) {
	defer func() {
		if v := recover(); v != nil {
			utils.LogError("Heartbeat thread exit: %v", v)
		}
	}()

	defer dvc.heartbeatThreadWg.Done()

	interval := dvc.GetHeartbeatInterval()
	if interval == 0 {
		return
	}

	ticker := time.NewTicker(interval)
	defer ticker.Stop()

	for {
		select {
		case <-dvc.heartbeatThreadCtx.Done():
			utils.LogInfo("Heartbeat thread exit: context done")
			return
		case <-ticker.C:
			msg := link.NewMsgHeartbeat(link.SrcDevice{
				ProductID:  dvc.productID,
				DeviceName: dvc.deviceName,
			})
			if err := dvc.PublishMQTTTopic(msg.GetMsgTopic(), msg.GetPayload()); err != nil {
				utils.LogError("failed to send heartbeat: %v", err)
			}
			// utils.LogInfo("heartbeat sent")
		}
	}
}

// getKLinkParserOfDirectlyConnectedDevice 根据用户传入的逻辑，构造并注册直连设备的 KLink 解析器。
// isGatewayDevice 和 topoChangeHandler 表示是否为网关设备，若为网关设备，内部会进行网关设备的拓扑
// 变更消息解析，收到消息后会调用 topoChangeHandler 进行处理。
func getKLinkParserOfDirectlyConnectedDevice(productID, deviceName string,
	handlers TopicHandlersOfDirectlyConnectedDevice, device *directlyConnectedDevice,
	isGatewayDevice bool, gw *gatewayDevice, topoChangeHandler TopoChangeHandler, topoQueryReplyHandler TopoQueryReplyHandler) *link.Parser {

	src := link.SrcDevice{
		ProductID:  productID,
		DeviceName: deviceName,
	}

	parser := link.NewParser()

	if isGatewayDevice {
		// 拓扑关系变化
		parser.SetMsgHandler(link.MsgTypeGwTopoChange, func(msg link.Msg) {
			m, ok := msg.(*link.MsgGwTopoChange)
			if !ok {
				return
			}

			utils.LogInfo("Received KLink message: %s, content: %+v", "MsgGwTopoChange", m)

			var changedSubdvcs []TopoChangedSubdvc
			for _, d := range m.Subdvcs {
				subdvc := TopoChangedSubdvc{
					DeviceIdentity: DeviceIdentity{
						ProductID:  d.ProductID,
						DeviceName: d.DeviceName,
					},
					Status: uint(d.Status),
					Config: d.Config,
				}
				changedSubdvcs = append(changedSubdvcs, subdvc)
			}

			reply := link.NewMsgGwTopoChangeReply(src, msg.GetMsgID(), link.NewSuccessReply(), topoChangeHandler(changedSubdvcs))
			if err := device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload()); err != nil {
				utils.LogError("Failed to publish MsgGwTopoChangeReply: %v", err)
			} else {
				utils.LogInfo("Published MsgGwTopoChangeReply, payload: %s", reply.GetPayload())
			}
		})

		// 拓扑关系查询回复
		parser.SetMsgHandler(link.MsgTypeGwTopoQueryReply, func(msg link.Msg) {
			m, ok := msg.(*link.MsgGwTopoQueryReply)
			if !ok {
				return
			}

			utils.LogInfo("Received KLink message: %s, content: %+v", "MsgGwTopoQueryReply", m)

			var subdvcs []TopoQueryReplySubdvc
			for _, d := range m.Subdvcs {
				subdvc := TopoQueryReplySubdvc{
					DeviceIdentity: DeviceIdentity{
						ProductID:  d.ProductID,
						DeviceName: d.DeviceName,
					},
					Config: d.Config,
				}
				subdvcs = append(subdvcs, subdvc)
			}

			if err := topoQueryReplyHandler(subdvcs); err != nil {
				err = fmt.Errorf("failed to call user's topoQueryReplyHandler: %w", err)
				utils.LogError(err.Error())
			}
		})

		// 网关代理子设备上下线的回复
		parser.SetMsgHandler(link.MsgTypeGwOnofflineSubdvcReply, func(msg link.Msg) {
			m, ok := msg.(*link.MsgGwOnofflineSubdvcReply)
			if !ok {
				return
			}

			utils.LogInfo("Received KLink message: %s, content: %+v", "MsgGwOnofflineSubdvcReply", m)

			var (
				subdvcs2Subscribe   []DeviceIdentity
				subdvcs2Unsubscribe []DeviceIdentity
			)

			// 1. 根据回复更新 gw.subdvcs 内部状态
			for _, res := range m.Results {

				// 失败
				if res.Result == -1 {
					if res.Action == 1 {
						gw.subdvcOnlineFail(res.ProductID, res.DeviceName, res.FailMsg)
					}
					if res.Action == -1 {
						gw.subdvcOfflineFail(res.ProductID, res.DeviceName, res.FailMsg)
					}
				}

				// 成功
				if res.Result == 1 {

					// 子设备上线成功
					if res.Action == 1 {

						// 1. 修改内部子设备状态为在线
						gw.subdvcOnoffline(res.ProductID, res.DeviceName, 1)

						// 2. 收集欲订阅的设备
						subdvcs2Subscribe = append(subdvcs2Subscribe, DeviceIdentity{res.ProductID, res.DeviceName})
					}

					// 子设备离线成功
					if res.Action == -1 {

						// 1. 修改内部子设备状态为离线
						gw.subdvcOnoffline(res.ProductID, res.DeviceName, -1)

						// 2. 收集欲取消订阅的设备
						subdvcs2Unsubscribe = append(subdvcs2Unsubscribe, DeviceIdentity{res.ProductID, res.DeviceName})
					}

				}
			}

			// 2. 成功上线的设备，订阅主题消息
			if len(subdvcs2Subscribe) > 0 {
				parsers := getKLinkParsersOfSubdvcs(false, gw, true, subdvcs2Subscribe, TopicHandlersOfGatewayDevice{
					ReadSubdvcPropsHandler:  gw.readSubdvcPropsHandler,
					WriteSubdvcPropsHandler: gw.writeSubdvcPropsHandler,
					InvokeSubdvcFuncHandler: gw.invokeSubdvcFuncHandler,
				})

				for topic, parser := range parsers {

					callback := func(c mqtt.Client, m mqtt.Message) {
						err := parser.Parse(m.Topic(), m.Payload())
						if errors.Is(err, link.ErrNonKlinkMsg) {
							utils.LogError("KLink parser error: receive non-KLink message, topic: %s, payload: %s", m.Topic(), m.Payload())
							return
						}
						if errors.Is(err, link.ErrInvalidMsgField) {
							utils.LogError("KLink parser error: receive invalid KLink message, %v", err)
							return
						}
						if errors.Is(err, link.ErrIncompatibleKlinkVersion) {
							utils.LogError("KLink parser error: receive incompatible KLink message: %v, local version: %s", err, link.KlinkVersion)
							return
						}
						if err != nil {
							utils.LogError("KLink parser error: %v", err)
							return
						}
					}

					utils.LogInfo("Subscribing sub-device's topic %s ...", topic)
					token := gw.Subscribe(topic, 0, callback)

					if ok := token.WaitTimeout(internal.MQTTWaitTimeout); !ok {
						utils.LogError("Failed to subscribe sub-device's topic %s : timeout", topic)
						continue
					}
					if token.Error() != nil {
						utils.LogError("Failed to subscribe sub-device's topic %s : %v", topic, token.Error())
						continue
					}
					utils.LogInfo("Subscribe sub-device's topic %s successfully!", topic)
				}
			}

			// 3. 成功下线的设备，取消订阅主题消息
			if len(subdvcs2Unsubscribe) > 0 {

				var topics []string

				for _, subdvc := range subdvcs2Unsubscribe {
					topics = append(topics, getDeviceRootTopic(subdvc.ProductID, subdvc.DeviceName))
				}

				utils.LogInfo("Unsubscribing sub-devices' topics %v ...", topics)
				if err := gw.UnsubscribeMQTTTopics(topics...); err != nil {
					utils.LogError("Failed to unsubscribe sub-devices' topics %v : %v", topics, err)
				} else {
					utils.LogInfo("Unsubscribe sub-devices' topics %v successfully!", topics)
				}
			}

		})
	}

	// 读取设备属性
	parser.SetMsgHandler(link.MsgTypeDvcReadProp, func(msg link.Msg) {
		m, ok := msg.(*link.MsgDvcReadProp)
		if !ok {
			return
		}

		utils.LogInfo("Received KLink message: %s, content: %+v", "MsgDvcReadProp", m)

		// 调用用户处理逻辑
		values, err := handlers.ReadPropsHandler(m.Props)
		if err != nil {
			err = fmt.Errorf("failed to call user's readPropsHandler: %w", err)
			utils.LogError(err.Error())
			reply := link.NewMsgDvcReadPropReply(src, m.GetMsgID(), link.NewFailReply("500", err.Error()), values)
			device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
			return
		}

		// 构造回复消息
		reply := link.NewMsgDvcReadPropReply(src, m.GetMsgID(), link.NewSuccessReply(), values)
		device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
	})

	// 修改设备属性
	parser.SetMsgHandler(link.MsgTypeDvcWriteProp, func(msg link.Msg) {
		m, ok := msg.(*link.MsgDvcWriteProp)
		if !ok {
			return
		}

		utils.LogInfo("Received KLink message: %s, content: %+v", "MsgDvcWriteProp", m)

		results, err := handlers.WritePropsHandler(m.Props)
		if err != nil {
			err = fmt.Errorf("failed to call user's writePropsHandler: %w", err)
			utils.LogError(err.Error())
			reply := link.NewMsgDvcWritePropReply(src, m.GetMsgID(), link.NewFailReply("500", err.Error()), results)
			device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
			return
		}

		reply := link.NewMsgDvcWritePropReply(src, m.GetMsgID(), link.NewSuccessReply(), results)
		device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
	})

	// 调用设备功能
	parser.SetMsgHandler(link.MsgTypeDvcFuncInvk, func(msg link.Msg) {
		m, ok := msg.(*link.MsgDvcFuncInvk)
		if !ok {
			return
		}

		utils.LogInfo("Received KLink message: %s, content: %+v", "MsgDvcFuncInvk", m)

		var nameValues []NameValue
		for _, input := range m.Inputs {
			nameValue := NameValue{
				Name:  input.Name,
				Value: input.Value,
			}
			nameValues = append(nameValues, nameValue)
		}

		outputs, err := handlers.InvokeFuncHandler(m.Func, nameValues)
		if err != nil {
			err = fmt.Errorf("failed to call user's invokeFuncHandler: %w", err)
			utils.LogError(err.Error())
			reply := link.NewMsgDvcFuncInvkReply(src, m.GetMsgID(), link.NewFailReply("500", err.Error()), outputs)
			device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
			return
		}

		reply := link.NewMsgDvcFuncInvkReply(src, m.GetMsgID(), link.NewSuccessReply(), outputs)
		device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
	})

	// 云端下发配置
	parser.SetMsgHandler(link.MsgTypeCfgPush, func(msg link.Msg) {
		m, ok := msg.(*link.MsgCfgPush)
		if !ok {
			return
		}

		utils.LogInfo("Received KLink message: %s, content: %+v", "MsgCfgPush", m)

		if err := handlers.PushConfigHandler(m.GetMsgID(), m.Type, m.Config); err != nil {
			err = fmt.Errorf("failed to call user's pushConfigHandler: %w", err)
			utils.LogError(err.Error())
		}
	})

	// 云端读取配置
	parser.SetMsgHandler(link.MsgTypeCfgRead, func(msg link.Msg) {
		m, ok := msg.(*link.MsgCfgRead)
		if !ok {
			return
		}

		utils.LogInfo("Received KLink message: %s, content: %+v", "MsgCfgRead", m)

		cfgBody, err := handlers.ReadConfigHandler(m.Type)
		if err != nil {
			err = fmt.Errorf("failed to call user's ReadConfigHandler: %w", err)
			utils.LogError(err.Error())
			reply := link.NewMsgCfgReadReply(src, m.GetMsgID(), link.NewFailReply("500", err.Error()), m.Type, nil)
			device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
			return
		}

		reply := link.NewMsgCfgReadReply(src, m.GetMsgID(), link.NewSuccessReply(), m.Type, cfgBody)
		device.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
	})

	// 下线通知
	parser.SetMsgHandler(link.MsgTypeOfflineNotification, func(msg link.Msg) {
		m, ok := msg.(*link.MsgOfflineNotification)
		if !ok {
			return
		}

		utils.LogInfo("Received KLink message: %s, content: %+v", "MsgOfflineNotification", m)

		if err := handlers.OfflineNotificationHandler(m.Reason); err != nil {
			err := fmt.Errorf("failed to call user's OfflineNotificationHandler: %w", err)
			utils.LogError(err.Error())
		}
	})

	return parser
}

// getKLinkParsersOfSubdvcs 获取所有子设备的消息解析器。
//
// 参数列表：
//   isAllSubdvcs		是否获取所有子设备的 parser
//   gw					从中获取到所有的子设备
//   isNewSubdvcs		是否仅获取新增子设备的 parser
//   newSubdvcs			新增子设备
//   handlers			子设备消息处理逻辑
func getKLinkParsersOfSubdvcs(
	isAllSubdvcs bool, gw *gatewayDevice,
	isNewSubdvcs bool, newSubdvcs []DeviceIdentity,
	handlers TopicHandlersOfGatewayDevice) map[string]*link.Parser {

	type Item struct {
		productID  string
		deviceName string
	}

	var items []Item

	if isAllSubdvcs {
		for _, d := range gw.subdvcs {
			item := Item{
				productID:  d.ProductID,
				deviceName: d.DeviceName,
			}
			items = append(items, item)
		}
	}

	if isNewSubdvcs {
		for _, d := range newSubdvcs {
			item := Item{
				productID:  d.ProductID,
				deviceName: d.DeviceName,
			}
			items = append(items, item)
		}
	}

	m := make(map[string]*link.Parser)

	for _, dvc := range items {
		topic := getDeviceRootTopic(dvc.productID, dvc.deviceName)
		parser := link.NewParser()
		m[topic] = parser

		// 读取子设备属性
		parser.SetMsgHandler(link.MsgTypeDvcReadProp, func(msg link.Msg) {
			dm, ok := msg.(*link.MsgDvcReadProp)
			if !ok {
				return
			}

			src := link.SrcDevice{
				ProductID:  dm.GetProductID(),
				DeviceName: dm.GetDeviceName(),
			}

			utils.LogInfo("Received KLink message: %s, content: %+v", "MsgDvcReadProp", m)

			values, err := handlers.ReadSubdvcPropsHandler(dm.GetProductID(), dm.GetDeviceName(), dm.Props)
			if err != nil {
				err = fmt.Errorf("failed to call user's readSubdvcPropsHandler: %w", err)
				utils.LogError(err.Error())
				reply := link.NewMsgDvcReadPropReply(src, dm.GetMsgID(), link.NewFailReply("500", err.Error()), values)
				gw.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
				return
			}

			reply := link.NewMsgDvcReadPropReply(src, dm.GetMsgID(), link.NewSuccessReply(), values)
			gw.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
		})

		// 修改子设备属性
		parser.SetMsgHandler(link.MsgTypeDvcWriteProp, func(msg link.Msg) {
			dm, ok := msg.(*link.MsgDvcWriteProp)
			if !ok {
				return
			}

			src := link.SrcDevice{
				ProductID:  dm.GetProductID(),
				DeviceName: dm.GetDeviceName(),
			}

			utils.LogInfo("Received KLink message: %s, content: %+v", "MsgDvcWriteProp", m)

			results, err := handlers.WriteSubdvcPropsHandler(dm.GetProductID(), dm.GetDeviceName(), dm.Props)
			if err != nil {
				err = fmt.Errorf("failed to call user's writeSubdvcPropsHandler: %w", err)
				utils.LogError(err.Error())
				reply := link.NewMsgDvcWritePropReply(src, dm.GetMsgID(), link.NewFailReply("500", err.Error()), results)
				gw.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
				return
			}

			reply := link.NewMsgDvcWritePropReply(src, dm.GetMsgID(), link.NewSuccessReply(), results)
			gw.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
		})

		// 调用子设备功能
		parser.SetMsgHandler(link.MsgTypeDvcFuncInvk, func(msg link.Msg) {
			dm, ok := msg.(*link.MsgDvcFuncInvk)
			if !ok {
				return
			}

			src := link.SrcDevice{
				ProductID:  dm.GetProductID(),
				DeviceName: dm.GetDeviceName(),
			}

			utils.LogInfo("Received KLink message: %s, content: %+v", "MsgDvcFuncInvk", m)

			var nameValues []NameValue
			for _, input := range dm.Inputs {
				nameValue := NameValue{
					Name:  input.Name,
					Value: input.Value,
				}
				nameValues = append(nameValues, nameValue)
			}

			outputs, err := handlers.InvokeSubdvcFuncHandler(dm.GetProductID(), dm.GetDeviceName(), dm.Func, nameValues)
			if err != nil {
				err = fmt.Errorf("failed to call user's invokeSubdvcFuncHandler: %w", err)
				utils.LogError(err.Error())
				reply := link.NewMsgDvcFuncInvkReply(src, dm.GetMsgID(), link.NewFailReply("500", err.Error()), outputs)
				gw.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
				return
			}

			reply := link.NewMsgDvcFuncInvkReply(src, dm.GetMsgID(), link.NewSuccessReply(), outputs)
			gw.PublishMQTTTopic(reply.GetMsgTopic(), reply.GetPayload())
		})

	}

	return m
}

// getMQTTCliOptsOfPSKType 获取 PSK 签名认证方式的 MQTT 设置选项。
func getMQTTCliOptsOfPSKType(
	brokerURL string,
	productID, deviceName, devicePSK string,
	onConnectHandler mqtt.OnConnectHandler,
	onConnLostHanler mqtt.ConnectionLostHandler) (
	*mqtt.ClientOptions, error) {

	// 明文 PSK
	var rawPSK string
	if bs, err := base64.StdEncoding.DecodeString(devicePSK); err != nil {
		return nil, fmt.Errorf("failed to decode(base64) devicePSK: %w", err)
	} else {
		rawPSK = string(bs)
	}

	// 客户端 ID
	clientID := productID + deviceName
	// 连接 ID
	connID := utils.RandLetterNum(6)

	// 用户名：客户端 ID + SDKAppID + 连接 ID + 签名有效期
	username := fmt.Sprintf("%s;%s;%s;%d", clientID, internal.SDKAppID, connID, time.Now().Add(time.Minute*10).Unix())
	// 密码：使用 PSK 对用户名进行签名
	password := utils.HmacSha256(username, rawPSK)

	opts := mqtt.NewClientOptions().
		AddBroker(brokerURL).
		SetClientID(clientID).
		SetCleanSession(false).
		SetAutoReconnect(true).
		SetMaxReconnectInterval(time.Second * 3).
		SetOnConnectHandler(onConnectHandler).
		SetConnectionLostHandler(onConnLostHanler).
		SetUsername(username).
		SetPassword(password).
		SetPingTimeout(time.Minute / 2)

	return opts, nil
}

// getMQTTCliOptsOfCrtType 获取证书/私钥认证方式的 MQTT 设置选项。
func getMQTTCliOptsOfCrtType(
	brokerURL string,
	productID, deviceName string, crt tls.Certificate,
	onConnectHandler mqtt.OnConnectHandler,
	onConnLostHanler mqtt.ConnectionLostHandler) (
	*mqtt.ClientOptions, error) {

	tlsCfg := &tls.Config{
		Certificates:       []tls.Certificate{crt},
		InsecureSkipVerify: true,
	}

	opts := mqtt.NewClientOptions().
		AddBroker(brokerURL).
		SetClientID(productID + deviceName).
		SetCleanSession(false).
		SetAutoReconnect(true).
		SetMaxReconnectInterval(time.Second * 3).
		SetOnConnectHandler(onConnectHandler).
		SetConnectionLostHandler(onConnLostHanler).
		SetTLSConfig(tlsCfg).
		SetPingTimeout(time.Minute / 2)

	return opts, nil
}

func getDeviceRootTopic(productID, deviceName string) string {
	return fmt.Sprintf("/%s/%s/downlink/#", productID, deviceName)
}

func dynamicRegister(corexHost, corexHttpPort, productID, deviceName, productSecret string) (*models.DynamicRegisterResult, error) {
	port, _ := strconv.Atoi(corexHttpPort)
	cli, err := client.NewClient(corexHost, port, client.WithTimeout(time.Second*5))
	if err != nil {
		return nil, fmt.Errorf("failed to new http client: %w", err)
	}

	return cli.DynamicRegister(deviceName, productID, productSecret)
}
