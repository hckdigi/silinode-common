package corexdevicesdk

import "gitee.com/hckdigi/silinode-common/siliworks-link"

// NameValue 用于键值对数据结构。
type NameValue struct {
	Name  string
	Value interface{}
}

// ReadPropsHandler 是直连设备的读属性消息处理单元。
type ReadPropsHandler func(props []string) (values map[string]interface{}, err error)

// WritePropsHandler 是直连设备的写属性消息处理单元。
type WritePropsHandler func(values map[string]interface{}) (results map[string]interface{}, err error)

// InvokeFuncHandler 是直连设备的功能调用消息处理单元。
type InvokeFuncHandler func(funcName string, inputs []NameValue) (outputs map[string]interface{}, err error)

// PushConfigHandler 是直连设备的配置推送消息处理单元。
type PushConfigHandler func(msgID string, cfgType string, cfgBody map[string]interface{}) error

// ReadConfigHandler 是直连设备的配置读取消息处理单元。
type ReadConfigHandler func(cfgType string) (cfgBody map[string]interface{}, err error)

// OfflineNotificationHandler 是直连设备收到下线通知的处理单元。
type OfflineNotificationHandler func(reason string) error

// TopoChangeHandler 是网关拓扑关系变化处理单元。
type TopoChangeHandler func(changedSubdvcs []TopoChangedSubdvc) []TopoChangedSubdvcResult

// TopoQueryReplyHandler 是网关拓扑查询回复处理单元。
type TopoQueryReplyHandler func(subdvcs []TopoQueryReplySubdvc) error

// ReadSubdvcPropsHandler 读取网关子设备属性消息处理单元。
type ReadSubdvcPropsHandler func(productID, deviceName string, props []string) (values map[string]interface{}, err error)

// WriteSubdvcPropsHandler 修改网关子设备属性消息处理单元。
type WriteSubdvcPropsHandler func(productID, deviceName string, values map[string]interface{}) (results map[string]interface{}, err error)

// InvokeSubdvcFuncHandler 是网关子设备的功能调用消息处理单元。
type InvokeSubdvcFuncHandler func(productID, deviceName string, funcName string, inputs []NameValue) (outputs map[string]interface{}, err error)

// IsSubdvcValid 判断子设备是否可用，可用代表子设备正常接入且正常工作。
type IsSubdvcValid func(deviceName string) bool

// TopicHandlersOfDirectlyConnectedDevice 是直连设备主题消息的用户处理逻辑集合。
type TopicHandlersOfDirectlyConnectedDevice struct {
	ReadPropsHandler
	WritePropsHandler
	InvokeFuncHandler
	PushConfigHandler
	ReadConfigHandler
	OfflineNotificationHandler
}

// TopicHandlersOfGatewayDevice 是网关设备主题消息的用户处理逻辑集合。
type TopicHandlersOfGatewayDevice struct {
	TopoChangeHandler
	TopoQueryReplyHandler
	ReadSubdvcPropsHandler
	WriteSubdvcPropsHandler
	InvokeSubdvcFuncHandler
	IsSubdvcValid
}

// DeviceIdentity 是设备标识。
type DeviceIdentity struct {
	ProductID  string
	DeviceName string
}

// Subdvc 代表子设备。
type Subdvc struct {
	DeviceIdentity
	PSK string
}

// subdvcWithState 是包含在线状态的子设备。
type subdvcWithState struct {
	Subdvc
	// OnlineState 在线状态，1 在线 -1 离线
	OnlineState int
	// OnlineFailMsg 上线动作失败，云端返回的失败原因
	OnlineFailMsg string
	// OfflineFailMsg 下线动作失败，云端返回的失败原因
	OfflineFailMsg string
}

// TopoChangedSubdvc 是发生拓扑变更的设备。
type TopoChangedSubdvc struct {
	DeviceIdentity
	// Status 是变更类型 1 解绑 2 绑定 3 配置变更。
	Status uint
	// Config 是设备最新配置，当 Status 为配置变更时，该参数有效。
	Config map[string]interface{}
}

type TopoChangedSubdvcResult = link.SubdvcOpResult

// TopoQueryReplySubdvc 是拓扑查询回复的单个设备。
type TopoQueryReplySubdvc struct {
	DeviceIdentity
	Config map[string]interface{}
}
