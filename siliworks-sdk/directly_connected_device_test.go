package corexdevicesdk

import "testing"

func TestConnect(t *testing.T) {
	t.Log("Starting connection...")

	dvc := NewDirectlyConnectedDevice(TopicHandlersOfDirectlyConnectedDevice{})

	dvc.SetCloudInfo("172.16.13.6", "1884", "1885", "172.16.13.6", "80")
	dvc.SetAuthInfo(1, "E30ISAOAHP", "", "4779F3420D0E998025A65F2E0E2B3819", "MzkwODE4NzE1MzY2NjA4OTAx", "", "")

	if err := dvc.Online(); err != nil {
		t.Errorf("failed to online: %v", err)
		return
	}

	if err := dvc.PublishMQTTTopic("/E30ISAOAHP/4779F3420D0E998025A65F2E0E2B3819/uplink/test/hello", "Hello, broker!"); err != nil {
		t.Errorf("failed to publish: %v", err)
		return
	}

	select {}
}
