package utils

import (
	"testing"
)

func TestAesEncryptCBC(t *testing.T) {
	key := []byte("asdfasdfasdfasd5")
	bs, err := AESEncryptCBC([]byte("hello world"), []byte("asdfasdfasdfasd5"))
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(bs)

	decrypted, err := AESDecryptCBC(bs, key)
	if err != nil {
		t.Error(err)
		return
	}

	t.Log("decrypted:", string(decrypted))
}
