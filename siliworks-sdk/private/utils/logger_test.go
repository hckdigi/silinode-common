package utils

import "testing"

func TestLog(t *testing.T) {
	LogInfo("Server is listening port %d", 8080)
	LogError("content is too long: %s", "1025 bytes")
}
