package utils

import "log"

const logPrefix = "[CoreX-Device-SDK]"

// LogInfo 打印 INFO 级别日志。
func LogInfo(format string, v ...interface{}) {
	logLevel("INFO", format, v...)
}

// LogError 打印 ERROR 级别日志。
func LogError(format string, v ...interface{}) {
	logLevel("ERRO", format, v...)
}

func logLevel(level, format string, v ...interface{}) {
	log.Printf(logPrefix+" ["+level+"] "+format+"\n", v...)
}
