package utils

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"fmt"
)

// AESEncryptCBC 为 CBC 模式 AES 对称加密算法。参数 key 必须为 16、24 或 32 字节，
// 分别对应 AES-128, AES-192, or AES-256。
func AESEncryptCBC(originData []byte, key []byte) (encrypted []byte, err error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("failed to NewCipher: %w", err)
	}

	blockSize := block.BlockSize()                              // 获取秘钥块的长度
	originData = pkcs5Padding(originData, blockSize)            // 补全码
	blockMode := cipher.NewCBCEncrypter(block, key[:blockSize]) // 加密模式
	encrypted = make([]byte, len(originData))                   // 创建数组
	blockMode.CryptBlocks(encrypted, originData)                // 加密
	return encrypted, nil
}

// AESDecryptCBC 为 CBC 模式 AES 对称加密的解密算法。参数 key 必须为 16、24 或 32 字节，
// 分别对应 AES-128, AES-192, or AES-256。
func AESDecryptCBC(encrypted []byte, key []byte) (decrypted []byte, err error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("failed to NewCipher: %w", err)
	}
	blockSize := block.BlockSize()                              // 获取秘钥块的长度
	blockMode := cipher.NewCBCDecrypter(block, key[:blockSize]) // 加密模式
	decrypted = make([]byte, len(encrypted))                    // 创建数组
	blockMode.CryptBlocks(decrypted, encrypted)                 // 解密
	decrypted = pkcs5UnPadding(decrypted)                       // 去除补全码
	return decrypted, nil
}

func pkcs5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func pkcs5UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}
