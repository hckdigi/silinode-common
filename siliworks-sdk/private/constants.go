package internal

import "time"

const (
	// SDKAppID 是此 Go 版本设备 SDK 的标识。
	SDKAppID = "12201001"
	// MQTTWaitTimeout 是所有 MQTT 操作的超时时间。
	MQTTWaitTimeout = time.Second * 5
	// HTTPWaitTimeout 是所有 HTTP 操作的超时时间。
	HTTPWaitTimeout = time.Second * 5
)
