package corexdevicesdk

import (
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/http/client"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/private"
	"gitee.com/hckdigi/silinode-common/siliworks-sdk/private/utils"
	"gitee.com/hckdigi/silinode-common/siliworks-link"
)

// GatewayDevice 代表一个抽象的网关设备，网关设备的开发者应当根据构造函数获取到该实例，通过
// 实例提供的接口，实现与云端的通信。网关设备继承了直连设备的能力。该接口上所有方法均并发安全。
type GatewayDevice interface {
	DirectlyConnectedDevice
	// AddSubdvc 添加子设备，informCloud 表示是否通知云端，也就是是否需要
	// 向云端发送绑定消息，若是云端下发拓扑变化导致的此操作，则置为 false。
	AddSubdvcs(informCloud bool, subdvcs ...Subdvc) error
	// RmSubdvc 移除子设备，informCloud 表示是否通知云端，也就是是否需要
	// 向云端发送解绑消息，若是云端下发拓扑变化导致的此操作，则置为 false。
	RmSubdvcs(infromCloud bool, subdvcs ...DeviceIdentity) error
	// SubdvcOnline 子设备上线，此操作前提是网关实例内部已包含要上线的设备，
	// 否则返回错误。
	SubdvcsOnline(subdvcs ...DeviceIdentity) error
	// SubdvcOffline 子设备下线，此操作前提是网关实例内部已包含要下线的设备，
	// 否则返回错误。
	SubdvcsOffline(subdvcs ...DeviceIdentity) error
	// ReportSubdvcEvent 上报子设备事件，此操作前提是网关实例内部已包含要上
	// 报的设备，并且设备处于在线状态，否则返回错误。
	ReportSubdvcEvent(subdvc DeviceIdentity, eventID string, eventTime time.Time, eventProps map[string]interface{}) error
	// SendTopoQuery 仅仅发送拓扑查询，处理结果由 TopoQueryReplyHandler 处理。
	SendTopoQuery() error
	// GetInternalInfo 获取网关内部信息。
	GetInternalInfo() json.RawMessage

	// incNumOfConn 无论首次建连还是断线重连，若连接成功，则次数加一。
	incNumOfConn()
	// getNumOfConn 获取连接成功的次数。
	getNumOfConn() int
	// execOnFirstConnectedHook 执行首次建连钩子。
	execOnFirstConnectedHook()
}

// NewGatewayDevice 构造网关设备实例，传入各类 topic 处理器以及初次建连成功钩子。
// 因为构造网关设备执行 Online 操作可能失败，但后续可以重新 UpdateConnection，为不影响程序初始化子设备，可将添加子设备的操作放在钩子中。
// 此函数未直接接收子设备的原因是，传入的子设备会动态增删，以建连之后的时刻为准。
func NewGatewayDevice(
	localHandlers TopicHandlersOfDirectlyConnectedDevice,
	subdvcHandlers TopicHandlersOfGatewayDevice,
	onFirstConnectedHook func(gw GatewayDevice),
) GatewayDevice {

	local := &directlyConnectedDevice{
		readingPropsHandler:        localHandlers.ReadPropsHandler,
		writingPropsHandler:        localHandlers.WritePropsHandler,
		invokingFuncHandler:        localHandlers.InvokeFuncHandler,
		pushingConfigHandler:       localHandlers.PushConfigHandler,
		readingConfigHandler:       localHandlers.ReadConfigHandler,
		offlineNotificationHandler: localHandlers.OfflineNotificationHandler,

		heartbeatThreadWg: new(sync.WaitGroup),
	}

	return &gatewayDevice{
		directlyConnectedDevice: local,
		subdvcs:                 []subdvcWithState{},
		numOfConn:               0,
		onFirstConnectedHook:    onFirstConnectedHook,

		topoChangeHandler:       subdvcHandlers.TopoChangeHandler,
		topoQueryReplyHandler:   subdvcHandlers.TopoQueryReplyHandler,
		readSubdvcPropsHandler:  subdvcHandlers.ReadSubdvcPropsHandler,
		writeSubdvcPropsHandler: subdvcHandlers.WriteSubdvcPropsHandler,
		invokeSubdvcFuncHandler: subdvcHandlers.InvokeSubdvcFuncHandler,
		isSubdvcValid:           subdvcHandlers.IsSubdvcValid,
	}
}

type gatewayDevice struct {
	// 继承直连设备功能
	*directlyConnectedDevice

	// 自身维护的子设备列表
	subdvcs []subdvcWithState

	// 连接次数
	numOfConn int
	// 首次建连钩子
	onFirstConnectedHook func(gw GatewayDevice)

	// 消息处理逻辑
	topoChangeHandler       TopoChangeHandler
	topoQueryReplyHandler   TopoQueryReplyHandler
	readSubdvcPropsHandler  ReadSubdvcPropsHandler
	writeSubdvcPropsHandler WriteSubdvcPropsHandler
	invokeSubdvcFuncHandler InvokeSubdvcFuncHandler
	isSubdvcValid           IsSubdvcValid
}

var _ GatewayDevice = (*gatewayDevice)(nil)

func (gw *gatewayDevice) incNumOfConn() {
	gw.numOfConn++
}

func (gw *gatewayDevice) getNumOfConn() int {
	return gw.numOfConn
}

func (gw *gatewayDevice) execOnFirstConnectedHook() {
	gw.onFirstConnectedHook(gw)
}

func (gw *gatewayDevice) GetInternalInfo() json.RawMessage {
	m := map[string]interface{}{
		"TimesOfconnected": gw.numOfConn,
		"Devices":          gw.subdvcs,
	}

	res, _ := json.Marshal(m)
	return res
}

// Online 重写继承于直连设备的上线方法。
func (gw *gatewayDevice) Online() error {
	var (
		err  error
		opts *mqtt.ClientOptions
	)

	dcdHandlers := TopicHandlersOfDirectlyConnectedDevice{
		ReadPropsHandler:           gw.directlyConnectedDevice.readingPropsHandler,
		WritePropsHandler:          gw.directlyConnectedDevice.writingPropsHandler,
		InvokeFuncHandler:          gw.directlyConnectedDevice.invokingFuncHandler,
		PushConfigHandler:          gw.directlyConnectedDevice.pushingConfigHandler,
		ReadConfigHandler:          gw.directlyConnectedDevice.readingConfigHandler,
		OfflineNotificationHandler: gw.directlyConnectedDevice.offlineNotificationHandler,
	}

	gwHandlers := TopicHandlersOfGatewayDevice{
		TopoChangeHandler:       gw.topoChangeHandler,
		TopoQueryReplyHandler:   gw.topoQueryReplyHandler,
		ReadSubdvcPropsHandler:  gw.readSubdvcPropsHandler,
		WriteSubdvcPropsHandler: gw.writeSubdvcPropsHandler,
		InvokeSubdvcFuncHandler: gw.invokeSubdvcFuncHandler,
	}

	if gw.deviceAuthType == 1 {

		if gw.productID == "" || gw.deviceName == "" || gw.devicePSK == "" {
			return errors.New("missing auth info")
		}

		opts, err = getMQTTCliOptsOfPSKType(gw.getMqttTcpAddr(), gw.productID, gw.deviceName, gw.devicePSK,
			getOnConnectHandler(gw.directlyConnectedDevice, false, dcdHandlers, true, gwHandlers, gw), gw.handleConnLost)
		if err != nil {
			return fmt.Errorf("failed to getMQTTCliOptsOfPSKType: %w", err)
		}

	} else if gw.deviceAuthType == 2 {

		if gw.productID == "" || gw.deviceName == "" || gw.deviceCrtFile == "" || gw.deviceKeyFile == "" {
			return errors.New("missing auth info")
		}

		crt, err := tls.LoadX509KeyPair(gw.deviceCrtFile, gw.deviceKeyFile)
		if err != nil {
			return fmt.Errorf("failed to LoadX509KeyPair: %w", err)
		}

		opts, err = getMQTTCliOptsOfCrtType(gw.getMqttTLSAddr(), gw.productID, gw.deviceName, crt,
			getOnConnectHandler(gw.directlyConnectedDevice, false, dcdHandlers, true, gwHandlers, gw), gw.handleConnLost)
		if err != nil {
			return fmt.Errorf("failed to getMQTTCliOptsOfCrtFileType: %w", err)
		}

	} else if gw.deviceAuthType == 3 {

		if gw.productID == "" || gw.productSecret == "" || gw.deviceName == "" {
			return errors.New("missing auth info")
		}

		// 请求动态注册接口，获取设备认证信息
		res, err := dynamicRegister(gw.httpHost, gw.httpPort, gw.productID, gw.deviceName, gw.productSecret)
		if err != nil {
			return fmt.Errorf("failed to dynamicRegister: %w", err)
		}

		if res.EncryptionType == 1 {

			// 证书认证方式

			if len(res.DeviceCrtPem) == 0 || len(res.DeviceKeyPem) == 0 {
				return fmt.Errorf("device crt pem or device key pem in response is empty")
			}

			crt, err := tls.X509KeyPair([]byte(res.DeviceCrtPem), []byte(res.DeviceKeyPem))
			if err != nil {
				return fmt.Errorf("failed to X509KeyPair: %w", err)
			}

			opts, err = getMQTTCliOptsOfCrtType(gw.getMqttTLSAddr(), gw.productID, gw.deviceName, crt,
				getOnConnectHandler(gw.directlyConnectedDevice, false, dcdHandlers, true, gwHandlers, gw), gw.handleConnLost)
			if err != nil {
				return fmt.Errorf("failed to getMQTTCliOptsOfCrtFileType: %w", err)
			}

		} else if res.EncryptionType == 2 {

			// 签名认证方式

			if len(res.DevicePSK) == 0 {
				return fmt.Errorf("device PSK in response is empty")
			}

			var err error
			opts, err = getMQTTCliOptsOfPSKType(gw.getMqttTcpAddr(), gw.productID, gw.deviceName, res.DevicePSK,
				getOnConnectHandler(gw.directlyConnectedDevice, false, dcdHandlers, true, gwHandlers, gw), gw.handleConnLost)
			if err != nil {
				return fmt.Errorf("failed to getMQTTCliOptsOfPSKType: %w", err)
			}

		} else {
			return fmt.Errorf("invalid encryption type from IoT platform")
		}

	} else {
		return fmt.Errorf("unsupported auth type: %d", gw.deviceAuthType)
	}

	gw.directlyConnectedDevice.Client = mqtt.NewClient(opts)
	token := gw.directlyConnectedDevice.Client.Connect().(*mqtt.ConnectToken)

	if !token.WaitTimeout(internal.MQTTWaitTimeout) {
		return errors.New("wait ack timeout")
	}

	if token.ReturnCode() == 254 {
		return errors.New("network error")
	}

	if token.ReturnCode() != 0 {
		port, _ := strconv.Atoi(gw.httpPort)
		httpcli, err := client.NewClient(gw.mqttBrokerHost, port, client.WithTimeout(time.Second*5))
		if err != nil {
			utils.LogError("new http client failed: %v", err)
			return fmt.Errorf("ACK Code %d", token.ReturnCode())
		}

		description, err := httpcli.GetMQTTConnFailDescription(gw.productID, gw.deviceName)
		if err != nil {
			utils.LogError("httpcli.GetMQTTConnFailDescription failed: %v", err)
			return fmt.Errorf("ACK Code %d", token.ReturnCode())
		}

		return errors.New(description)
	}

	return nil
}

// UpdateConnection 更新连接。此方法必须重写，否则会调用内部直连设备的 Online 方法。
func (gw *gatewayDevice) UpdateConnection() (wasOnline bool, err error) {
	wasOnline = gw.Offline()
	err = gw.Online()
	return
}

func (gw *gatewayDevice) Offline() (wasOnline bool) {
	if gw.IsOnline() {
		wasOnline = true
	}

	// 主动下线不会触发 ConnLost 回调，
	// 此处主动停止心跳协程
	if gw.heartbeatThreadCancel != nil {
		gw.heartbeatThreadCancel()
	}
	gw.heartbeatThreadWg.Wait()

	if gw.Client != nil {
		gw.Disconnect(1000)     // 异步断连
		time.Sleep(time.Second) // 等待断连，防止未完全断连时，后续操作就把 Client 实例回收
		if !gw.IsOnline() {
			utils.LogInfo("MQTT Client OFFLINE SUCCESSFULLY !!!")
		} else {
			utils.LogError("MQTT Client OFFLINE FAILED !!!")
		}

		for i := range gw.subdvcs {
			gw.subdvcs[i].OnlineState = -1
		}
		// gw.Client = nil
	}

	return
}

// AddSubdvcs 绑定子设备。
func (gw *gatewayDevice) AddSubdvcs(informCloud bool, subdvcs ...Subdvc) error {
	// 检测 gw 连接状态
	if informCloud && !gw.IsOnline() {
		return errors.New("gw is not online")
	}

	var authes []link.SubdvcAuth

	// 添加内部设备实例
	for _, subdvc := range subdvcs {

		if informCloud {

			// 组装 auth
			var auth link.SubdvcAuth

			if subdvc.ProductID == "" || subdvc.DeviceName == "" || subdvc.PSK == "" {
				utils.LogError("AddSubdvcs: missing productID or deviceName or PSK: %+v", subdvc)
				continue
			}

			random := utils.RandNum(6)
			exp := time.Now().Add(time.Hour * 24 * 365).Unix()
			txt := fmt.Sprintf("%s%s;%s;%d", subdvc.ProductID, subdvc.DeviceName, random, exp)
			signature := base64.StdEncoding.EncodeToString([]byte(utils.HmacSha256(txt, subdvc.PSK)))

			auth = link.SubdvcAuth{
				ProductID:      subdvc.ProductID,
				DeviceName:     subdvc.DeviceName,
				Random:         random,
				ExpirationTime: int(exp),
				AuthType:       "psk",
				SignMethod:     "hmacsha256",
				Signature:      signature,
			}

			authes = append(authes, auth)
		}

		gw.addSubdvc(subdvc)
	}

	// 向云端发送绑定消息
	if informCloud {
		msg := link.NewMsgGwBindSubdvc(link.SrcDevice{ProductID: gw.productID, DeviceName: gw.deviceName}, authes)
		if err := gw.PublishMQTTTopic(msg.GetMsgTopic(), msg.GetPayload()); err != nil {
			return fmt.Errorf("failed to publish MsgGwBindSubdvc: %w", err)
		}
	}

	return nil
}

func (gw *gatewayDevice) RmSubdvcs(informCloud bool, subdvcs ...DeviceIdentity) error {
	// 检测 gw 连接状态
	if informCloud && !gw.IsOnline() {
		return errors.New("gw is not online")
	}

	if len(subdvcs) == 0 {
		return nil
	}

	var dvcs []link.DeviceIdentity

	// 取消订阅，并移除内部实例
	for _, d := range subdvcs {
		if err := gw.UnsubscribeMQTTTopics(getDeviceRootTopic(d.ProductID, d.DeviceName)); err != nil {
			return fmt.Errorf("unsubscribe device topics failed: %w", err)
		}

		gw.rmSubdvc(d.ProductID, d.DeviceName)

		dvcs = append(dvcs, link.DeviceIdentity{
			ProductID:  d.ProductID,
			DeviceName: d.DeviceName,
		})
	}

	// 云端解绑
	if informCloud {
		msg := link.NewMsgGwUnbindSubdvc(link.SrcDevice{
			ProductID:  gw.productID,
			DeviceName: gw.deviceName,
		}, dvcs)

		if err := gw.PublishMQTTTopic(msg.GetMsgTopic(), msg.GetPayload()); err != nil {
			return fmt.Errorf("failed to publish MsgGwUnbindSubdvc: %w", err)
		}
	}

	return nil
}

// SubdvcsOnline 网关代理子设备上线。
func (gw *gatewayDevice) SubdvcsOnline(subdvcs ...DeviceIdentity) error {

	if len(subdvcs) <= 0 {
		return nil
	}

	// 检查 gw 连接状态

	if !gw.IsOnline() {
		return errors.New("gw is not online")
	}

	// 检查子设备是否已绑定至网关、检查子设备是否真实可用

	var (
		allok  bool = true
		errtxt string
	)

	for _, subdvc := range subdvcs {
		if _, exists := gw.getSubdvcStatus(subdvc.ProductID, subdvc.DeviceName); !exists {
			errtxt += fmt.Sprintf("sub-device(%s) doesn't belong to this gateway, ", subdvc.DeviceName)
			allok = false
		}

		if gw.isSubdvcValid != nil && !gw.isSubdvcValid(subdvc.DeviceName) {
			errtxt += fmt.Sprintf("sub-device(%s) isn't valid, ", subdvc.DeviceName)
			allok = false
		}
	}

	if !allok {
		return errors.New(strings.TrimSuffix(errtxt, ", "))
	}

	// 上报子设备上线

	var devices []link.SubdvcOnoffline

	for _, subdvc := range subdvcs {
		device := link.SubdvcOnoffline{
			DeviceIdentity: link.DeviceIdentity{ProductID: subdvc.ProductID, DeviceName: subdvc.DeviceName},
			Status:         1,
		}
		devices = append(devices, device)
	}

	msg := link.NewMsgGwOnofflineSubdvc(link.SrcDevice{ProductID: gw.productID, DeviceName: gw.deviceName}, devices)
	if err := gw.PublishMQTTTopic(msg.GetMsgTopic(), msg.GetPayload()); err != nil {
		return fmt.Errorf("failed to publish subdvcs-online message: %w", err)
	}

	return nil
}

// SubdvcsOffline 网关代理子设备下线。
func (gw *gatewayDevice) SubdvcsOffline(subdvcs ...DeviceIdentity) error {

	if len(subdvcs) <= 0 {
		return nil
	}

	// 检查 gw 连接状态

	if !gw.IsOnline() {
		return errors.New("gw is not online")
	}

	// 检查设备是否已绑定至网关

	var (
		allok  bool = true
		errtxt string
	)

	for _, subdvc := range subdvcs {
		if _, exists := gw.getSubdvcStatus(subdvc.ProductID, subdvc.DeviceName); !exists {
			errtxt += fmt.Sprintf("sub-device(%s) doesn't belong to this gateway, ", subdvc.DeviceName)
			allok = false
		}
	}

	if !allok {
		return errors.New(strings.TrimSuffix(errtxt, ", "))
	}

	// 上报子设备下线

	var devices []link.SubdvcOnoffline

	for _, subdvc := range subdvcs {
		device := link.SubdvcOnoffline{
			DeviceIdentity: link.DeviceIdentity{ProductID: subdvc.ProductID, DeviceName: subdvc.DeviceName},
			Status:         -1,
		}
		devices = append(devices, device)
	}

	msg := link.NewMsgGwOnofflineSubdvc(link.SrcDevice{ProductID: gw.productID, DeviceName: gw.deviceName}, devices)
	if err := gw.PublishMQTTTopic(msg.GetMsgTopic(), msg.GetPayload()); err != nil {
		return fmt.Errorf("failed to publish subdvcs-offline message: %w", err)
	}

	return nil
}

func (gw *gatewayDevice) ReportSubdvcEvent(subdvc DeviceIdentity, eventID string, eventTime time.Time, eventProps map[string]interface{}) error {
	// 检测 gw 连接状态
	if !gw.IsOnline() {
		return errors.New("gw is not online")
	}

	// 检测设备是否已绑定，并处于在线状态
	online, exists := gw.getSubdvcStatus(subdvc.ProductID, subdvc.DeviceName)
	if !exists {
		return fmt.Errorf("sub device(%s %s) is not exits", subdvc.ProductID, subdvc.DeviceName)
	}
	if !online {
		return fmt.Errorf("sub device(%s %s) is not online", subdvc.ProductID, subdvc.DeviceName)
	}

	// 上报事件
	msg := link.NewMsgDvcEvent(link.SrcDevice{
		ProductID:  subdvc.ProductID,
		DeviceName: subdvc.DeviceName,
	}, eventID, eventTime, eventProps)

	if err := gw.PublishMQTTTopic(msg.GetMsgTopic(), msg.GetPayload()); err != nil {
		return fmt.Errorf("failed to publish MsgDvcEvent: %w", err)
	}

	return nil
}

func (gw *gatewayDevice) SendTopoQuery() error {
	// 检测 gw 连接状态
	if !gw.IsOnline() {
		return errors.New("gw is not online")
	}

	msg := link.NewMsgGwTopoQuery(link.SrcDevice{
		ProductID:  gw.productID,
		DeviceName: gw.deviceName,
	})
	if err := gw.PublishMQTTTopic(msg.GetMsgTopic(), msg.GetPayload()); err != nil {
		return fmt.Errorf("failed to publish MsgGwTopoQuery: %w", err)
	}

	return nil
}

// rmSubdvc 移除 gw 内部子设备。
func (gw *gatewayDevice) rmSubdvc(productID, deviceName string) {

	subdvcs := make([]subdvcWithState, 0, len(gw.subdvcs))

	for _, subdvc := range gw.subdvcs {
		if subdvc.ProductID != productID || subdvc.DeviceName != deviceName {
			subdvcs = append(subdvcs, subdvc)
		}
	}

	gw.subdvcs = subdvcs
}

// addSubdvc 向 gw 内部添加子设备。
func (gw *gatewayDevice) addSubdvc(subdvc Subdvc) {
	_, exists := gw.getSubdvcStatus(subdvc.ProductID, subdvc.DeviceName)
	if exists {
		gw.subdvcOnoffline(subdvc.ProductID, subdvc.DeviceName, -1)
		return
	}

	gw.subdvcs = append(gw.subdvcs, subdvcWithState{
		Subdvc:         subdvc,
		OnlineState:    -1,
		OnlineFailMsg:  "",
		OfflineFailMsg: "",
	})
}

// subdvcOnoffline 设置内部子设备在线状态，并清空上下线失败信息。
func (gw *gatewayDevice) subdvcOnoffline(productID, deviceName string, state int) {
	for i, subdvc := range gw.subdvcs {
		if subdvc.ProductID == productID && subdvc.DeviceName == deviceName {
			gw.subdvcs[i].OnlineState = state

			gw.subdvcs[i].OnlineFailMsg = ""
			gw.subdvcs[i].OfflineFailMsg = ""
		}
	}
}

// subdvcOnlineFail 子设备上线失败，设置失败信息。
func (gw *gatewayDevice) subdvcOnlineFail(productID, deviceName string, failMsg string) {
	for i, subdvc := range gw.subdvcs {
		if subdvc.ProductID == productID && subdvc.DeviceName == deviceName {
			gw.subdvcs[i].OnlineFailMsg = failMsg
		}
	}
}

// subdvcOfflineFail 子设备下线失败，设置失败信息。
func (gw *gatewayDevice) subdvcOfflineFail(productID, deviceName string, failMsg string) {
	for i, subdvc := range gw.subdvcs {
		if subdvc.ProductID == productID && subdvc.DeviceName == deviceName {
			gw.subdvcs[i].OfflineFailMsg = failMsg
		}
	}
}

// getSubdvcStatus 获取子设备状态，包括是否存在、是否在线。
func (gw *gatewayDevice) getSubdvcStatus(productID, deviceName string) (online, exists bool) {

	for _, subdvc := range gw.subdvcs {
		if subdvc.ProductID == productID && subdvc.DeviceName == deviceName {
			if subdvc.OnlineState == 1 {
				return true, true
			} else {
				return false, true
			}
		}
	}

	return false, false
}
