package models

type DeviceListResponse struct {
	Total int      `json:"total"`
	List  []Device `json:"list"`
}
