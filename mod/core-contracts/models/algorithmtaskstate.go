package models

import (
	"encoding/json"
	"fmt"
	"strings"
)

type AlgorithmTaskState string

const (
	Starting  = "STARTING"
	Running   = "RUNNING"
	Waiting   = "WAITING"
	RunFailed = "RUNFAILED"
)

// UnmarshalJSON implements the Unmarshaler interface for the enum type
func (as *AlgorithmTaskState) UnmarshalJSON(data []byte) error {
	// Extract the string from data.
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("AlgorithmTaskState should be a string, got %s", data)
	}

	new := AlgorithmTaskState(strings.ToUpper(s))
	*as = new

	return nil
}

// Validate satisfies the Validator interface
func (as AlgorithmTaskState) Validate() (bool, error) {
	_, found := map[string]AlgorithmTaskState{"RUNNING": Running, "WAITING": Waiting, "RUNFAILED": RunFailed, "STARTING": Starting}[string(as)]
	if !found {
		return false, NewErrContractInvalid(fmt.Sprintf("invalid AlgorithmTaskState %q", as))
	}
	return true, nil
}
func GetAlgorithmTaskState(as string) (AlgorithmTaskState, bool) {
	as = strings.ToUpper(as)
	retValue, err := map[string]AlgorithmTaskState{"RUNNING": Running, "WAITING": Waiting, "RUNFAILED": RunFailed, "STARTING": Starting}[as]
	return retValue, err
}
