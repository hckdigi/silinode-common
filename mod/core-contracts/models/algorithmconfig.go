package models

const (
	AI_EXECUTE_TYPE_EXE   = "Exe"
	AI_EXECUTE_TYPE_IMAGE = "Image"
	AI_EXECUTE_TYPE_SDK   = "Sdk"

	AI_COMMUNICATION_TYPE_MQTT     = "Mqtt"
	AI_COMMUNICATION_TYPE_KAFKA    = "Kafka"
	AI_COMMUNICATION_TYPE_DATABASE = "Database"
	AI_COMMUNICATION_TYPE_FILE     = "File"

	AI_STORE_TYPE_FILE      = "LocalFile"
	AI_STORE_TYPE_FTPSERVER = "FtpServer"
	AI_STORE_TYPE_HTTP      = "Http"

	AI_COMMUNICATION_TOPIC_TYPE_INFERDATA = "InferData"
	AI_COMMUNICATION_TOPIC_TYPE_WARNNING  = "Warnning"
	AI_COMMUNICATION_TOPIC_TYPE_FILE      = "File"

	AI_COMMUNICATION_TOPIC_PROPERTY_TYPE_DATA = "Data"
	AI_COMMUNICATION_TOPIC_PROPERTY_TYPE_FILE = "File"
)

type AlgorithmConfig struct {
	SourceConfig     VideoSourceConfig      `json:"sourceConfig,omitempty"`  //视频来源
	Execute          ExecuteConfig          `json:"execute"`                 //执行方式
	Communication    CommunicationConfig    `json:"communication"`           //通信方式
	Store            StoreConfig            `json:"store,omitempty"`         //文件存储方式
	StreamOutAddress string                 `json:"streamOutAddress"`        //推理流输出地址
	DeviceInfoOut    bool                   `json:"deviceInfoOut,omitempty"` //是否输出设备信息
	ModelInfoOut     bool                   `json:"modelInfoOut,omitempty"`  //是否输出模型信息
	PushToCorex      bool                   `json:"pushToCorex"`             //是否默认推送推理结果到corex
	UserSetting      map[string]interface{} `json:"userSetting,omitempty"`   //用户自定义输出
}

type VideoSourceConfig struct {
	DeviceId     string `json:"deviceId"`
	VideoChannel string `json:"videoChannel"`
}

type ExecuteConfig struct {
	ExecuteType      string                  `json:"executeType"`
	ExeCommand       string                  `json:"exeCommand"`
	ContainerName    string                  `json:"containerName"`
	EnvironmentParam []EnvironmentParamerter `json:"environmentParam"`
}

type EnvironmentParamerter struct {
	Name        string `json:"name"`
	Value       string `json:"value"`
	Description string `json:"description"`
}

type CommunicationConfig struct {
	CommunicationType string         `json:"communicationType"`
	Mqtt              MqttConfig     `json:"mqtt,omitempty"`
	Kafka             KafkaConfig    `json:"kafka,omitempty"`
	Database          DatabaseConfig `json:"database,omitempty"`
	File              FileConfig     `json:"file,omitempty"`
}

type MqttConfig struct {
	Host       string      `json:"host"`
	Port       string      `json:"port"`
	UserName   string      `json:"userName"`
	Password   string      `json:"password"`
	TopicInfos []TopicInfo `json:"topicInfos"`
}

type KafkaConfig struct {
	Host       string      `json:"host"`
	Port       string      `json:"port"`
	TopicInfos []TopicInfo `json:"topicInfos"`
}

type TopicInfo struct {
	Topic      string         `json:"topic"`
	Type       string         `json:"type"`
	Properties []PropertyInfo `json:"properties"`
}

type PropertyInfo struct {
	PropertyPath string `json:"propertyPath"`
	Type         string `json:"type"`
}

type DatabaseConfig struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	UserName string `json:"userName"`
	Password string `json:"password"`
	Table    string `json:"table"`
}

type FileConfig struct {
	FilePath string `json:"filePath"`
}

type StoreConfig struct {
	StoreType string           `json:"storeType"`
	LocalFile LocalStoreConfig `json:"localFile,omitempty"`
	FtpServer FtpServerConfig  `json:"ftpServer,omitempty"`
	Http      HttpServerConfig `json:"http,omitempty"`
}

type LocalStoreConfig struct {
	FilePath string `json:"filePath"`
}

type FtpServerConfig struct {
	FtpServerAddress string `json:"ftpServerAddress,omitempty"`
	UserName         string `json:"userName,omitempty"`
	Password         string `json:"password,omitempty"`
}

type HttpServerConfig struct {
	ServerAddress string `json:"serverAddress,omitempty"`
}
