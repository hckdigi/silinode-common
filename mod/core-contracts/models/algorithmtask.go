package models

type AlgorithmAnalyzeTask struct {
	ID          string             `json:"id"`
	Name        string             `json:"name"`
	Model       AlgorithmModel     `json:"model"`
	Config      AlgorithmConfig    `json:"config"`
	DeviceId    string             `json:"deviceId"`
	DeviceName  string             `json:"deviceName"`
	StartTime   int64              `json:"startTime"`
	RunningTime int64              `json:"runningTime"`
	State       AlgorithmTaskState `json:"state"`
	RunErrMsg   string             `json:"runErrMsg"`
	Description string             `json:"description"`
	CloudId     string             `json:"cloudId"`
	Source      string             `json:"source"`
	CreateTime  int64              `json:"createTime"`
	UpdateTime  int64              `json:"updateTime"`
}
