/*******************************************************************************
 * Copyright 2019 Dell Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 *******************************************************************************/

package models

import (
	"encoding/json"
)

// DeviceProfile represents the attributes and operational capabilities of a device. It is a template for which
// there can be multiple matching devices within a given system.
type DeviceProfile struct {
	DescribedObject    `yaml:",inline"`
	Id                 string                `json:"id,omitempty" yaml:"id,omitempty"`
	Name               string                `json:"name,omitempty" yaml:"name,omitempty"`                 // Non-database identifier (must be unique)
	Manufacturer       string                `json:"manufacturer,omitempty" yaml:"manufacturer,omitempty"` // Manufacturer of the device
	Type               string                `json:"type,omitempty" yaml:"type,omitempty"`
	Model              string                `json:"model,omitempty" yaml:"model,omitempty"`        // Model of the device
	Labels             []string              `json:"labels,omitempty" yaml:"labels,flow,omitempty"` // Labels used to search for groups of profiles
	DeviceResources    []DeviceResource      `json:"deviceResources,omitempty" yaml:"deviceResources,omitempty"`
	DeviceCommands     []ProfileResource     `json:"deviceCommands,omitempty" yaml:"deviceCommands,omitempty"`
	CoreCommands       []Command             `json:"coreCommands,omitempty" yaml:"coreCommands,omitempty"` // List of commands to Get/Put information for devices associated with this profile
	DeviceFunctions    []DeviceFunction      `json:"deviceFunctions,omitempty" yaml:"deviceFunctions,omitempty"`
	AutoEvents         []AutoEvent           `json:"autoEvents,omitempty" yaml:"autoEvents,omitempty"`
	RuleEvents         []RuleEvent           `json:"ruleEvents,omitempty" yaml:"ruleEvents,omitempty"`               //规则事件参数结构
	ProtocolMoulds     []DeviceProtocolMould `json:"protocolMoulds,omitempty" yaml:"protocolMoulds,omitempty"`       //可选设备协议模板
	DeviceServiceName  string                `json:"deviceServiceName,omitempty" yaml:"deviceServiceName,omitempty"` //绑定的设备服务名称
	NumOfDevices       int                   `json:"numOfDevices,omitempty" yaml:"numOfDevices,omitempty"`           // 该模板设备数量 added by xvr, 20210802
	isValidated        bool                  // internal member used for validation check
	Version            string                `json:"version,omitempty" yaml:"version,omitempty"`
	VersionDescription string                `json:"versionDescription" yaml:"versionDescription,omitempty"` //版本描述信息
}

// UnmarshalJSON implements the Unmarshaler interface for the DeviceProfile type
func (dp *DeviceProfile) UnmarshalJSON(data []byte) error {
	var err error
	type Alias struct {
		DescribedObject    `json:",inline"`
		Id                 *string               `json:"id"`
		Name               *string               `json:"name"`
		Manufacturer       *string               `json:"manufacturer"`
		Model              *string               `json:"model"`
		Type               *string               `json:"type"`
		Labels             []string              `json:"labels"`
		DeviceResources    []DeviceResource      `json:"deviceResources"`
		DeviceCommands     []ProfileResource     `json:"deviceCommands"`
		CoreCommands       []Command             `json:"coreCommands"`
		DeviceFunctions    []DeviceFunction      `json:"deviceFunctions"`
		AutoEvents         []AutoEvent           `json:"autoEvents"`
		RuleEvents         []RuleEvent           `json:"ruleEvents"`
		ProtocolMoulds     []DeviceProtocolMould `json:"protocolMoulds"`
		Version            string                `json:"version"`
		VersionDescription string                `json:"versionDescription"`
	}
	a := Alias{}
	// Error with unmarshaling
	if err = json.Unmarshal(data, &a); err != nil {
		return err
	}

	// Check nil fields
	if a.Id != nil {
		dp.Id = *a.Id
	}
	if a.Name != nil {
		dp.Name = *a.Name
	}
	if a.Manufacturer != nil {
		dp.Manufacturer = *a.Manufacturer
	}
	if a.Type != nil {
		dp.Type = *a.Type
	}
	if a.Model != nil {
		dp.Model = *a.Model
	}
	dp.DescribedObject = a.DescribedObject
	dp.Labels = a.Labels
	dp.DeviceResources = a.DeviceResources
	dp.DeviceCommands = a.DeviceCommands
	dp.CoreCommands = a.CoreCommands
	dp.AutoEvents = a.AutoEvents
	dp.RuleEvents = a.RuleEvents
	dp.DeviceFunctions = a.DeviceFunctions
	dp.ProtocolMoulds = a.ProtocolMoulds
	dp.Version = a.Version
	dp.VersionDescription = a.VersionDescription

	dp.isValidated, err = dp.Validate()

	return err

}

// Validate satisfies the Validator interface
func (dp DeviceProfile) Validate() (bool, error) {
	if !dp.isValidated {
		if dp.Id == "" && dp.Name == "" {
			return false, NewErrContractInvalid("Device ID and Name are both blank")
		}
		// Check if there are duplicate names in the device profile command list
		cmds := map[string]int{}
		for _, c := range dp.CoreCommands {
			if _, ok := cmds[c.Name]; !ok {
				cmds[c.Name] = 1
			} else {
				return false, NewErrContractInvalid("duplicate names in device profile commands")
			}
		}
		funcs := map[string]int{}
		for _, f := range dp.DeviceFunctions {
			if _, ok := cmds[f.Name]; !ok {
				funcs[f.Name] = 1
			} else {
				return false, NewErrContractInvalid("duplicate names in device profile functions")
			}
		}
		err := validate(dp)
		if err != nil {
			return false, err
		}
		return true, nil
	}
	return dp.isValidated, nil
}

/*
 * To String function for DeviceProfile
 */
func (dp DeviceProfile) String() string {
	out, err := json.Marshal(dp)
	if err != nil {
		return err.Error()
	}
	return string(out)
}
