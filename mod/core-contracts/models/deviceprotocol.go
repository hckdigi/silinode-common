package models

type DeviceProtocolMould struct {
	Protocol           string             `json:"protocol,omitempty" yaml:"protocol,omitempty"`
	ProtocolProperties []ProtocolProperty `json:"protocolProperties,omitempty" yaml:"protocolProperties,omitempty"`
}

type ProtocolProperty struct {
	Name         string `json:"name,omitempty" yaml:"name,omitempty"`
	DefaultValue string `json:"defaultValue,omitempty" yaml:"defaultValue,omitempty"`
}
