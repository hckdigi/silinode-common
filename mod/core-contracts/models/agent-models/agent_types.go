package agentmodels

type AgentType string

const (
	CoreXAgent   AgentType = "corex"
	TencentAgent AgentType = "tencent"
	AliyunAgent  AgentType = "aliyun"
	HuaweiAgent  AgentType = "huawei"
)
