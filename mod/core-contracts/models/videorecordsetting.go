package models

type VideoRecordChannelSetting struct {
	AdvanceTime int                  `json:"advanceTime"` // 录像计划 预录时间（秒）
	DelayTime   int                  `json:"delayTime"`   // 录像计划 延迟关闭时间（秒）
	AudioRecord bool                 `json:"audioRecord"` // 录像计划 是否记录音频
	DaySetting  []VideoRecordSetting `json:"daySetting"`  // 录像计划 每天的设置
}

type VideoRecordSetting struct {
	ChannelName       string `json:"channelName"`
	WeekDay           string `json:"weekDay"`           // 录像计划 星期几
	RecordStartHour   int    `json:"recordStartHour"`   // 录像计划 开始时间（小时）
	RecordEndHour     int    `json:"recordEndHour"`     // 录像计划 结束时间（小时）
	RecordStartMinute int    `json:"recordStartMinute"` // 录像计划 开始时间（分钟）
	RecordEndMinute   int    `json:"recordEndMinute"`   // 录像计划 结束时间（分钟）
}
