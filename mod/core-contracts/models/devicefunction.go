package models

import (
	"encoding/json"
)

type DeviceFunction struct {
	Description    string   `json:"description" yaml:"description,omitempty"`
	Name           string   `json:"name" yaml:"name,omitempty"`
	Tag            string   `json:"tag" yaml:"tag,omitempty"`
	ParameterNames []string `json:"parameterNames,omitempty" yaml:"parameterNames,omitempty"`
}

// MarshalJSON implements the Marshaler interface in order to make empty strings null
func (do DeviceFunction) MarshalJSON() ([]byte, error) {
	test := struct {
		Description    string   `json:"description"`
		Name           string   `json:"name"`
		Tag            string   `json:"tag"`
		ParameterNames []string `json:"parameterNames,omitempty"`
	}{
		Description: do.Description,
		Name:        do.Name,
		Tag:         do.Tag,
	}
	if len(do.ParameterNames) > 0 {
		test.ParameterNames = do.ParameterNames
	}

	return json.Marshal(test)
}

/*
 * To String function for DeviceResource
 */
func (do DeviceFunction) String() string {
	out, err := json.Marshal(do)
	if err != nil {
		return err.Error()
	}
	return string(out)
}
