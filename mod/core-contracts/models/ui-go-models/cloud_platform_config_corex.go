package uigomodels

type PlatformConfigCoreX struct {
	Enabled bool `json:"enabled"`

	HttpPort           string `json:"httpPort"`
	MQTTBrokerAddr     string `json:"mqttBrokerAddr" validate:"required,ip|hostname"`
	MQTTBrokerTCPPort  string `json:"mqttBrokerTcpPort" validate:"required,number"`
	MQTTBrokerTLSPort  string `json:"mqttBrokerTlsPort" validate:"required,number"`
	DynamicRegisterURL string `json:"dynamicRegisterUrl" validate:"required,url"`

	MQTTAuthProductID  string `json:"mqttAuthProductId" validate:"required,max=50"`
	MQTTAuthDeviceName string `json:"mqttAuthDeviceName" validate:"required,max=50"`

	MQTTAuthType          uint8  `json:"mqttAuthType" validate:"required,oneof=1 2 3"` // 1 psk 2 crt 3 dynamic
	MQTTAuthDevicePSK     string `json:"mqttAuthDevicePsk" validate:"required_if=MQTTAuthType 1,max=50"`
	MQTTAuthCrtFileName   string `json:"mqttAuthCrtFileName" validate:"required_if=MQTTAuthType 2,max=60"`
	MQTTAuthKeyFileName   string `json:"mqttAuthKeyFileName" validate:"required_if=MQTTAuthType 2,max=60"`
	MQTTAuthProductSecret string `json:"mqttAuthProductSecret" validate:"required_if=MQTTAuthType 3,max=50"`

	DeviceModelUpdateStrategy uint8 `json:"deviceModelUpdateStrategy" validate:"required,oneof=1 2"` // 1 auto 2 manual
}
