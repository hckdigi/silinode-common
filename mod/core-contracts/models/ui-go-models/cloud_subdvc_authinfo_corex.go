package uigomodels

type CoreXSubdvcAuthInfo struct {
	DeviceName      string `gorm:"type:varchar(50);not null;uniqueIndex;" json:"deviceName" validate:"required,max=50"`
	ProductID       string `gorm:"type:varchar(50);not null;" json:"productId" validate:"required,max=50"`
	AuthType        uint8  `gorm:"type:tinyint;unsigned;not null;" json:"authType" validate:"required,oneof=1 2 3"`
	DevicePSK       string `gorm:"type:varchar(50);not null;default:'';" json:"devicePsk" validate:"required_if=AuthType 1,max=50"`
	RootCrtFileName string `gorm:"type:varchar(50);not null;default:'';" json:"rootCrtFileName" validate:"required_if=AuthType 2,max=50"`
	CrtFileName     string `gorm:"type:varchar(50);not null;default:'';" json:"crtFileName" validate:"required_if=AuthType 2,max=50"`
	KeyFileName     string `gorm:"type:varchar(50);not null;default:'';" json:"keyFileName" validate:"required_if=AuthType 2,max=50"`
	ProductSecret   string `gorm:"type:varchar(50);not null;default:'';" json:"productSecret" validate:"required_if=AuthType 3,max=50"`
}
