package models

type AlgorithmModel struct {
	Id                   string               `json:"id"`
	Name                 string               `json:"name"`
	Frame                string               `json:"frame"`
	Format               string               `json:"format"`
	Type                 string               `json:"type"`
	Manufacturer         string               `json:"manufacturer"`
	Source               string               `json:"source"`
	State                string               `json:"state"`
	Description          string               `json:"description"`
	CloudVersion         string               `json:"cloudVersion"`
	Version              string               `json:"version"`
	UpdateTime           int64                `json:"updateTime"`
	Path                 string               `json:"path"`
	ExecuteType          string               `json:"executeType"`
	ImageConfig          AlgorithmImageConfig `json:"imageConfig"`
	ExecutableFileConfig AlgorithmFileConfig  `json:"executableFileConfig"`
	Config               AlgorithmConfig      `json:"config"`
	FileSize             string               `json:"fileSize"`
	CloudId string `json:"cloudId"`
	EnvironmentRequire   string               `json:"environmentRequire"`
}

type AlgorithmImageConfig struct {
	ImageAddress string `json:"imageAddress"`
}

type AlgorithmFileConfig struct {
	ExecutableFileName string `json:"executableFileName"`
}

func ConvertCorexCloudAlgorithmType(category int) string {
	switch category {
	case 1:
		return "objectDetection"
	case 2:
		return "imageClassification"
	case 3:
		return "behaviorAnalysis"
	case 4:
		return "anomalyDetection"
	case 5:
		return "targetTracking"
	case 6:
		return "modelOptimization"
	}
	return ""
}

func ConvertCorexCloudExecuteType(method string) string {
	switch method {
	case "file":
		return "Exe"
	case "image":
		return "Image"
	default:
		return ""
	}
}
