package models

import (
	"encoding/json"
	"reflect"

	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients"
)

const (
	// RuleEngine 相关的规则
	EnclosureURL               = "enclosure"
	HostAlarmURL               = "alarms"
	RuleExtensionBase          = clients.ApiBase + "/extension"
	RuleExtensionHost          = RuleExtensionBase + "/host"
	RuleExtensionHostEnclosure = RuleExtensionHost + "/" + EnclosureURL
	RuleExtensionHostAlarm     = RuleExtensionHost + "/" + HostAlarmURL

	// 调用 EMA 服务，完成Nvidia 指令的管理
	EMANvCtrlURL       = clients.ApiBase + "/host/nvControl"
	EMAHostAlarmURL    = clients.ApiBase + "/host/alarm"
	EMAHostProfileURL  = clients.ApiBase + "/host/profile"
	EMAHostPropertyURL = clients.ApiBase + "/host/property"
	// 默认规则名称
	KUIPER_RULE_OF_ENCLOSURE  = "default_gpsElectronicFence"
	KUIPER_RULE_OF_CPU_ALARM  = "default_hostCpuAlarm"
	KUIPER_RULE_OF_MEM_ALARM  = "default_hostMemAlarm"
	KUIPER_RULE_OF_DISK_ALARM = "default_hostDiskAlarm"
	KUIPER_RULE_OF_TEMP_ALARM = "default_hostTempAlarm"

	// 告警事件 eventID
	HOST_CPU_ALARM_EVENT_ID  = "cpuPercentAlarm"
	HOST_MEM_ALARM_EVENT_ID  = "memPercentAlarm"
	HOST_DISK_ALARM_EVENT_ID = "diskPercentAlarm"
	HOST_TEMP_ALARM_EVENT_ID = "hostTemperatureAlarm"

	GPS_INFO_EVENT_ID = "gpsInfo"
	// 属性名称
	HostTemperature = "hostTemperature"
	DiskPercent     = "diskPercent"
	CpuPercent      = "cpuPercent"
	MemPercent      = "memPercent"
	GpsInfo         = "gpsInfo"
)

type NetIfaceInfo struct {
	IfaceName string `json:"ifaceName"`
	IpAddr    string `json:"ipAddr"`
	MacAddr   string `json:"macAddr"`
}

// #############################################
// 系统状态：主机信息、内存信息、产品信息、GPU信息、CPU信息、温度信息、硬盘信息
// 主机信息
type HostInfo struct {
	HostName        string         `json:"hostName,omitempty"`        // 主机名称，
	BootTime        string         `json:"bootTime,omitempty"`        // 运行时间(启动时间)
	PlatformVersion string         `json:"platformVersion,omitempty"` // 系统版本:Linux Ubuntu 18.04
	KernelVersion   string         `json:"kernelVersion,omitempty"`   // 内核版本:4.9.201-tegra
	Interfaces      []NetIfaceInfo `json:"interfaces,omitempty"`      // 主机网络接口; key=interfaceName
}

// 内存信息
type MemoryInfo struct {
	Total   string `json:"total,omitempty"`   // 总大小
	Used    string `json:"used,omitempty"`    // 已用空间
	Free    string `json:"free,omitempty"`    // 可用空间
	Percent string `json:"percent,omitempty"` // 占用率
}

// 产品信息
type ProduceInfo struct {
	ChipType     string `json:"chipType"`     // 载板主芯片型号，如Nvidia Jetson
	ChipModel    string `json:"chipModel"`    // 载板主芯片型号, 如 Nano
	DeviceModel  string `json:"deviceModel"`  // 设备型号；一般指产品型号，如 EGE-690P
	Manufacturer string `json:"manufacturer"` // 制造商: 如Lenovo
}

// GPU信息
type GpuInfo struct {
	Name      string `json:"name,omitempty"`      // 名称
	Frequency string `json:"frequency,omitempty"` // 频率
	Percent   string `json:"percent,omitempty"`   // 占用率

}

// 温度信息
type TemperatureInfo struct {
	Cpu string `json:"cpu,omitempty"` // CPU
	Gpu string `json:"gpu,omitempty"` // GPU
	Aux string `json:"aux,omitempty"` // AUX
}

type SensorTemperature struct {
	Name        string `json:"name"`
	Temperature string `json:"temperature"`
}

// SensorPowerValue 功率信息
type SensorPowerValue struct {
	Current string `json:"current"` // 瞬时功率
	Average string `json:"average"` // 平均功率
}

// SensorPower 功率取值(之所以没有采用map方式，便于序列化处理)
type SensorPower struct {
	Name  string           `json:"name"`  // 处理器名称，如ALL、GPU
	Power SensorPowerValue `json:"power"` // 处理器的功率值
}

// 硬盘信息，可能存在多个
type DiskInfo struct {
	Name    string `json:"name,omitempty"`    // 设备名称
	ExtType string `json:"extType,omitempty"` // 文件系统类型
	Total   string `json:"total,omitempty"`   // 总大小
	Used    string `json:"used,omitempty"`    // 已用空间
	Free    string `json:"free,omitempty"`    // 可用空间
	Percent string `json:"percent,omitempty"` // 占用率
}

// Cpu信息
type CpuInfo struct {
	Name      string `json:"name,omitempty"`      // 名称
	Frequency string `json:"frequency,omitempty"` // 频率
	Percent   string `json:"percent,omitempty"`   // 占用率
}

// 核心模组参数信息；
type CoreModelInfo struct {
	Name        string `json:"name"`        // 模组名称, XavierNX
	Type        string `json:"type"`        // 模组型号: P3668 P3509-000
	SNumber     string `json:"sNumber"`     // 模组序列号：14xxxxxxxx
	SoftVersion string `json:"softVersion"` // 模组软件版本：Jetpack4.6[L4T 32.6.1]
}

// 系统状态; 定时上报的状态中仅取部分值即可
type SystemStatus struct {
	Host    HostInfo      `json:"host,omitempty"`
	Mem     MemoryInfo    `json:"mem,omitempty"`
	Product ProduceInfo   `json:"product,omitempty"`
	Model   CoreModelInfo `json:"model,omitempty"`

	Temps    []SensorTemperature `json:"temps,omitempty"`
	Powers   []SensorPower       `json:"powers,omitempty"`
	Gpus     []GpuInfo           `json:"gpus,omitempty"`
	Cpus     []CpuInfo           `json:"cpus,omitempty"`
	Disks    []DiskInfo          `json:"disks,omitempty"`
	NvStatus NVCtrl              `json:"nvStatus,omitempty"`
	GpsInfo  string              `json:"gpsInfo,omitempty"`
	// 获取通用的实时指标
	Temperature int `json:"hostTemperature"`
	CpuPercent  int `json:"cpuPercent"`
	MemPercent  int `json:"memPercent"`
	DiskPercent int `json:"diskPercent"`
}

// ########################################################
// 服务版本包括：1）系统总版本，2) 各微服务的版本；
// 1)系统总版本：从 ema(edgexd)获取， key:
// 2)各微服务版本: 从consul中获取。
type SoftwareInfo struct {
	Versions map[string]string `json:"versions"`
}

// #######################################################

const (
	NVPNanoMaxn = 0 // Nano 全速模式, 10W
	NVPNano5W   = 1 // Nano 5W

	NVPNX15W2Core = 0 // Xavier NX
	NVPNX15W4Core = 1 // Xavier NX
	NVPNX15W6Core = 2 // Xavier NX
	NVPNX10W2Core = 3 // Xavier NX
	NVPNX10W4Core = 4 // Xavier NX
	//
	FanModeDefault = 0 // 默认模式, Auto=Enable，Jetson_clock开启之后，就全速运作
	FanModeSystem  = 1 // 系统模式，Auto=Enable，需要满足条件 + jetson_clock
	FanModeManual  = 2 // 手动模式, Auto=Disbale

	// Xavier
	FanModeQuiet = "quiet" // 默认模式， FanModeDefault
	FanModeCool  = "cool"  // FanModeSystem

	WorkModeNono = 0
	WorkModeMini = 1 // edgex-mini工作模式
	WorkModeFull = 2 // edgex-full工作模式

	DefaultDeviceSN = "HCK-NxxP-220430-0000"
)

func NVP_Nano(m int) string {
	switch m {
	case NVPNanoMaxn:
		return "MAXN"
	case NVPNano5W:
		return "5W"
	}
	return ""
}

func NVP_XavierNX(m int) string {
	switch m {
	case NVPNX15W2Core:
		return "15W 2CORE"
	case NVPNX15W4Core:
		return "15W 4CORE"
	case NVPNX15W6Core:
		return "15W 6CORE"
	case NVPNX10W2Core:
		return "10W 2CORE"
	case NVPNX10W4Core:
		return "10W 4CORE"
	}
	return ""
}

func NVFanMode_Nano(fm int) string {
	switch fm {
	case FanModeDefault:
		return "default"
	case FanModeSystem:
		return "system"
	case FanModeManual:
		return "manual"
	}
	return ""
}

func NVFanMode_XavierNX(fm int) string {
	switch fm {
	case FanModeDefault:
		return FanModeQuiet
	default:
		return FanModeCool
	}
}

type NVCtrl struct {
	NvpModel    int  `json:"nvpModel"`    // 功率模式，不同核心取值范围不同
	JetsonClock bool `json:"jetsonClock"` // 高频使能
	FanSpeed    int  `json:"fanSpeed"`    // 风扇转速，0/10/20/.../100; 三种模式下均可调
	FanMode     int  `json:"fanMode"`     // 风扇模式: 默认、自动、手动
}

// Position 代表一个 GPS 坐标点。
type Position struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

// CloudDevicePosition 云端设定的设备当前位置。
type CloudDevicePosition struct {
	IsSet     bool   `json:"isSet"`
	Latitude  string `json:"latitude"`
	Longitude string `json:"longitude"`
}

// EdgeX 网关设备自身的告警阈值
type AlarmThreshold struct {
	Cpu  int8 `json:"cpuPercent"`
	Mem  int8 `json:"memPercent"`
	Disk int8 `json:"diskPercent"`
	Temp int8 `json:"hostTemperature"`
}
type WorkModeType int

type AdvanceConfig struct {
	HwCodecEnable bool   `json:"hwCodecEnable"`
	HwCodec       string `json:"hwCodec,omitempty"`
}

// EdgeX 网关设备的配置信息
type HostConfig struct {
	DeviceNumber      string         `json:"deviceNumber"`                // 网关编号
	DeviceName        string         `json:"deviceName,omitempty"`        // 网关名称
	DeviceDescription string         `json:"deviceDescription,omitempty"` // 网关描述
	WorkMode          WorkModeType   `json:"workMode,omitempty"`          // 工作模式
	QueryInterval     int            `json:"queryInterval,omitempty"`     // 状态采集周期
	Alarms            AlarmThreshold `json:"alarms,omitempty"`            // 告警阈值
	Enclosures        []Position     `json:"enclosures,omitempty"`        // 围栏配置
	CloudDevicePosition CloudDevicePosition `json:"cloudDevicePosition,omitempty"` // 云端设定的设备当前位置
	NVCtrls           NVCtrl         `json:"nvCtrls,omitempty"`           // Nvidia
	ProduceInfo       ProduceInfo    `json:"produceInfo"`                 // 产品信息配置
	AdvanceCfg        AdvanceConfig  `json:"advanceCfg,omitempty"`        // 高级配置
	TDengineKeep      int            `json:"tDengineKeep"`                // TDengine 数据保留天数
	OffdataMaxHours   int            `json:"offdataMaxHours"`             // 离线数据最大上报小时数
}

// 主机的事件定义
type EventItem struct {
	// EventId     string   `json:"event_id,omitempty"`
	EventParams []string `json:"eventParams,omitempty"`
}

type HostEvents struct {
	Events map[string]EventItem `json:"events,omitempty"`
}

type HostNotification struct {
	DeviceName  string                 `json:"deviceName"`
	EventId     string                 `json:"eventId"`
	EventParams map[string]interface{} `json:"eventParams"`
}

// String returns a JSON encoded string representation of this ProfileProperty
func (hc HostConfig) String() string {
	out, err := json.Marshal(hc)
	if err != nil {
		return err.Error()
	}
	return string(out)
}

func (at AlarmThreshold) IsEmpty() bool {
	return reflect.DeepEqual(at, AlarmThreshold{})
}

func (nc NVCtrl) IsEmpty() bool {
	return reflect.DeepEqual(nc, NVCtrl{})
}

func (m WorkModeType) String() string {
	switch m {
	case WorkModeMini:
		return "edgex-mini"
	case WorkModeFull:
		return "edgex-full"
	default:
		return "Unsupported Mode"
	}
}
