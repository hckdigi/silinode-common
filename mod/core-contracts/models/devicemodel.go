package models

import (
	"encoding/json"
	"reflect"
)

type DeviceModel struct {
	Id            string   `json:"id"` // 设备型号id
	Name          string   `json:"name"`
	Manufacturer  string   `json:"manufacturer"`  // 设备厂商
	Model         string   `json:"model"`         // 设备型号
	Type          string   `json:"type"`          // 设备类型
	Version       string   `json:"version"`       // 版本
	CloudVersion  string   `json:"cloudVersion"`  // 云端版本
	DeviceService string   `json:"deviceService"` // 设备服务名称
	Labels        []string `json:"labels"`        // 设备标签
	Remark        string   `json:"remark"`        // 备注
	State         string   `json:"state"`         // 设备模板状态
	Source        string   `json:"source"`        // 设备模板来源
	CloudId       string   `json:"cloudId"`       // 云端资源id

	// Added by zsy 1207
	DeviceProfile DeviceProfile `json:"deviceProfile"` // 绑定的设备模板

	NumOfDevices int `json:"numOfDevices"` // 该设备型号下设备数量
}

// MarshalJSON implements the Marshaler interface in order to make empty strings null
func (ds DeviceModel) MarshalJSON() ([]byte, error) {
	test := struct {
		Id            string         `json:"id"` // 设备型号id
		Name          string         `json:"name"`
		Manufacturer  string         `json:"manufacturer"`  // 设备厂商
		Model         string         `json:"model"`         // 设备型号
		Type          string         `json:"type"`          // 设备类型
		Version       string         `json:"version"`       // 版本
		DeviceService string         `json:"deviceService"` // 设备服务名称
		Labels        []string       `json:"labels"`        // 设备标签
		Remark        string         `json:"remark"`        // 备注
		State         string         `json:"state"`         // 设备模板状态
		Source        string         `json:"source"`        // 设备模板来源
		CloudId       string         `json:"cloudId"`       // 云端资源id
		DeviceProfile *DeviceProfile `json:"deviceProfile"` // 绑定的设备模板
		NumOfDevices  int            `json:"numOfDevices"`  // 该设备型号下设备数量
	}{
		Id:            ds.Id,
		Name:          ds.Name,
		Manufacturer:  ds.Manufacturer,
		Model:         ds.Model,
		Type:          ds.Type,
		Version:       ds.Version,
		Labels:        ds.Labels,
		DeviceService: ds.DeviceService,
		Remark:        ds.Remark,
		State:         ds.State,
		Source:        ds.Source,
		CloudId:       ds.CloudId,
		DeviceProfile: &ds.DeviceProfile,
		NumOfDevices:  ds.NumOfDevices,
	}

	if reflect.DeepEqual(*test.DeviceProfile, DeviceProfile{}) {
		test.DeviceProfile = nil
	}

	return json.Marshal(test)
}

// UnmarshalJSON implements the Unmarshaler interface for the DeviceProfile type
func (dp *DeviceModel) UnmarshalJSON(data []byte) error {
	var err error
	type Alias struct {
		DescribedObject `json:",inline"`
		Id              *string       `json:"id"`
		Name            *string       `json:"name"`
		Manufacturer    *string       `json:"manufacturer"`
		Model           *string       `json:"model"`
		Type            *string       `json:"type"`
		Version         *string       `json:"version"`
		Labels          []string      `json:"labels"`
		DeviceService   *string       `json:"deviceService"`
		Remark          *string       `json:"remark"`
		State           *string       `json:"state"`
		Source          *string       `json:"source"`
		CloudId         *string       `json:"cloudId"`
		DeviceProfile   DeviceProfile `json:"deviceProfile"`
	}
	a := Alias{}
	// Error with unmarshaling
	if err = json.Unmarshal(data, &a); err != nil {
		return err
	}

	// Check nil fields
	if a.Id != nil {
		dp.Id = *a.Id
	}
	if a.Name != nil {
		dp.Name = *a.Name
	}
	if a.Manufacturer != nil {
		dp.Manufacturer = *a.Manufacturer
	}
	if a.Model != nil {
		dp.Model = *a.Model
	}
	if a.Type != nil {
		dp.Type = *a.Type
	}
	if a.Version != nil {
		dp.Version = *a.Version
	}
	if a.DeviceService != nil {
		dp.DeviceService = *a.DeviceService
	}
	if a.Remark != nil {
		dp.Remark = *a.Remark
	}
	if a.State != nil {
		dp.State = *a.State
	}
	if a.Source != nil {
		dp.Source = *a.Source
	}
	if a.CloudId != nil {
		dp.CloudId = *a.CloudId
	}
	dp.Labels = a.Labels
	dp.DeviceProfile = a.DeviceProfile

	return err

}

/*
 * To String function for DeviceModel
 */
func (dp DeviceModel) String() string {
	out, err := json.Marshal(dp)
	if err != nil {
		return err.Error()
	}
	return string(out)
}
