package models

type RuleEvent struct {
	Name       string              `json:"name" yaml:"name,omitempty"`
	Properties []RuleEventProperty `json:"properties" yaml:"properties,omitempty"`
}

type RuleEventProperty struct {
	Name       string `json:"name" yaml:"name,omitempty"`
	Type       string `json:"type" yaml:"type,omitempty"`
	ObjectName string `json:"objectName" yaml:"objectName,omitempty"`
}
