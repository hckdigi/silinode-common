package metadata

import (
	"context"
	"encoding/json"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/interfaces"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	"net/url"
)

// DeviceProfileClient defines the interface for interactions with the DeviceProfile endpoint on metadata.
type DeviceModelClient interface {
	// Add a new device model
	Add(ctx context.Context, dm *models.DeviceModel) (string, error)
	// Delete eliminates a device model for the specified ID
	Delete(ctx context.Context, id string) error
	//DeleteByName eliminates a device model for the specified name
	DeleteByName(ctx context.Context, name string) error
	// DeviceModel loads the device model for the specified ID
	DeviceModel(ctx context.Context, id string) (models.DeviceModel, error)
	// DeviceModels lists all device models
	DeviceModels(ctx context.Context) ([]models.DeviceModel, error)
	// DeviceModel loads the device model for the specified manufacturer and type and model
	DeviceModelForManufacturerAndTypeAndModel(ctx context.Context, manufacturer, deviceType, model string) (models.DeviceModel, error)
	// Update a device model
	Update(ctx context.Context, dm models.DeviceModel) error
	// Add a new device model by cloud
	AddByCloud(ctx context.Context, manufacturer, deviceType, model, version string) (string, error)
}

type deviceModelRestClient struct {
	urlClient interfaces.URLClient
}

// Return an instance of DeviceProfileClient
func NewDeviceModelClient(urlClient interfaces.URLClient) DeviceModelClient {
	return &deviceModelRestClient{
		urlClient: urlClient,
	}
}

// Helper method to request and decode a device model
func (dmc *deviceModelRestClient) requestDeviceModel(
	ctx context.Context,
	urlSuffix string) (models.DeviceModel, error) {

	data, err := clients.GetRequest(ctx, urlSuffix, dmc.urlClient)
	if err != nil {
		return models.DeviceModel{}, err
	}

	dm := models.DeviceModel{}
	err = json.Unmarshal(data, &dm)
	return dm, err
}

// Helper method to request and decode a device model slice
func (dmc *deviceModelRestClient) requestDeviceModelSlice(
	ctx context.Context,
	urlSuffix string) ([]models.DeviceModel, error) {

	data, err := clients.GetRequest(ctx, urlSuffix, dmc.urlClient)
	if err != nil {
		return []models.DeviceModel{}, err
	}

	dmSlice := make([]models.DeviceModel, 0)
	err = json.Unmarshal(data, &dmSlice)
	return dmSlice, err
}

func (d deviceModelRestClient) Add(ctx context.Context, dm *models.DeviceModel) (string, error) {
	return clients.PostJSONRequest(ctx, "", dm, d.urlClient)
}

func (d deviceModelRestClient) Delete(ctx context.Context, id string) error {
	return clients.DeleteRequest(ctx, "/id/"+id, d.urlClient)
}

func (d deviceModelRestClient) DeleteByName(ctx context.Context, name string) error {
	return clients.DeleteRequest(ctx, "/name/"+url.QueryEscape(name), d.urlClient)
}

func (d deviceModelRestClient) DeviceModel(ctx context.Context, id string) (models.DeviceModel, error) {
	return d.requestDeviceModel(ctx, "/id/"+id)
}

func (d deviceModelRestClient) DeviceModels(ctx context.Context) ([]models.DeviceModel, error) {
	return d.requestDeviceModelSlice(ctx, "")
}

func (d deviceModelRestClient) DeviceModelForManufacturerAndTypeAndModel(ctx context.Context, manufacturer, deviceType, model string) (models.DeviceModel, error) {
	return d.requestDeviceModel(ctx, "/manufacturer/"+url.QueryEscape(manufacturer)+"/devicetype/"+url.QueryEscape(deviceType)+"/model/"+url.QueryEscape(model))
}

func (d deviceModelRestClient) Update(ctx context.Context, dm models.DeviceModel) error {
	return clients.UpdateRequest(ctx, "", dm, d.urlClient)
}

func (d deviceModelRestClient) AddByCloud(ctx context.Context, manufacturer, deviceType, model, version string) (string, error) {
	dm := models.DeviceModel{
		Manufacturer: manufacturer,
		Type:         deviceType,
		Model:        model,
		Version:      version,
	}
	return clients.PostJSONRequest(ctx, "/cloud/agent", dm, d.urlClient)
}
