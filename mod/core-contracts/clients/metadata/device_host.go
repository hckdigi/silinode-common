package metadata

import (
	"context"
	"encoding/json"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/interfaces"
)

type DeviceHostClient interface {
	// get the profile of Host Device
	HostProfiles(ctx context.Context) (map[string]interface{}, error)
	HostProperties(ctx context.Context, props []string) ([]interface{}, error)
}

type deviceHostRestClient struct {
	urlClient interfaces.URLClient
}

func NewDeviceHostClient(urlClient interfaces.URLClient) DeviceHostClient {
	return &deviceHostRestClient{
		urlClient: urlClient,
	}
}

func (dhc *deviceHostRestClient) HostProfiles(ctx context.Context) (map[string]interface{}, error) {
	data, err := clients.GetRequest(ctx, "/profile", dhc.urlClient)
	if err != nil {
		return nil, err
	}

	dataMap := make(map[string]interface{})
	err = json.Unmarshal(data, &dataMap)
	return dataMap, err
}

func (dpc *deviceHostRestClient) HostProperties(ctx context.Context, props []string) ([]interface{}, error) {
	dataSlice := make([]interface{}, 0)
	for _, prop := range props {
		data, err := clients.GetRequest(ctx, "/property/"+prop, dpc.urlClient)
		if err != nil {
			return nil, err
		}
		di := new(interface{})
		json.Unmarshal(data, &di)
		dataSlice = append(dataSlice, di)
	}
	return dataSlice, nil
}
