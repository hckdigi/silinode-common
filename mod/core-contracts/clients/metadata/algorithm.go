package metadata

import (
	"context"
	"encoding/json"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/interfaces"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/requests/states/algorithmmodel"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/requests/states/algorithmtask"
)

type AlgorithmClient interface {
	AlgorithmModelList(ctx context.Context) ([]models.AlgorithmModel, error)
	GetAlgorithmModelById(ctx context.Context, id string) (models.AlgorithmModel, error)
	GetAlgorithmModelByName(ctx context.Context, name string) (models.AlgorithmModel, error)
	AddAlgorithmModel(ctx context.Context, model *models.AlgorithmModel) (string, error)
	UpdateAlgorithmModelState(ctx context.Context, id string, req algorithmmodel.UpdateModelStateRequest) (string, error)
	UpdateAlgorithmModelVersion(ctx context.Context, id, version string) (string, error)
	DeleteAlgorithmModelById(ctx context.Context, id string) error
	DeleteAlgorithmModelByName(ctx context.Context, name string) error

	AlgorithmTaskList(ctx context.Context) ([]models.AlgorithmAnalyzeTask, error)
	GetAlgorithmTaskById(ctx context.Context, id string) (models.AlgorithmAnalyzeTask, error)
	GetAlgorithmTaskByName(ctx context.Context, name string) ([]models.AlgorithmAnalyzeTask, error)
	AddAlgorithmTask(ctx context.Context, task *models.AlgorithmAnalyzeTask) (string, error)
	AddAlgorithmTaskByCloud(ctx context.Context, task *models.AlgorithmAnalyzeTask) (string, error)
	UpdateAlgorithmTaskState(ctx context.Context, id string, req algorithmtask.UpdateTaskStateRequest) (string, error)
	UpdateAlgorithmTask(ctx context.Context, task *models.AlgorithmAnalyzeTask) (string, error)
	UpdateAlgorithmTaskByCloud(ctx context.Context, task *models.AlgorithmAnalyzeTask) (string, error)
	DeleteAlgorithmTaskById(ctx context.Context, id string) error
	//DeleteAlgorithmTaskByName(ctx context.Context, name string) error
}

type algorithmRestClient struct {
	urlClient interfaces.URLClient
}

// NewDeviceClient creates an instance of DeviceClient
func NewAlgorithmClient(urlClient interfaces.URLClient) AlgorithmClient {
	return &algorithmRestClient{
		urlClient: urlClient,
	}
}

// Helper method to request and decode a device
func (d *algorithmRestClient) requestAlgorithmTask(ctx context.Context, urlSuffix string) (models.AlgorithmAnalyzeTask, error) {
	data, err := clients.GetRequest(ctx, urlSuffix, d.urlClient)
	if err != nil {
		return models.AlgorithmAnalyzeTask{}, err
	}

	task := models.AlgorithmAnalyzeTask{}
	err = json.Unmarshal(data, &task)
	return task, err
}

// Helper method to request and decode a device slice
func (d *algorithmRestClient) requestAlgorithmTaskSlice(ctx context.Context, urlSuffix string) ([]models.AlgorithmAnalyzeTask, error) {
	data, err := clients.GetRequest(ctx, urlSuffix, d.urlClient)
	if err != nil {
		return []models.AlgorithmAnalyzeTask{}, err
	}

	tSlice := make([]models.AlgorithmAnalyzeTask, 0)
	err = json.Unmarshal(data, &tSlice)
	return tSlice, err
}

// Helper method to request and decode a device
func (d *algorithmRestClient) requestAlgorithmModel(ctx context.Context, urlSuffix string) (models.AlgorithmModel, error) {
	data, err := clients.GetRequest(ctx, urlSuffix, d.urlClient)
	if err != nil {
		return models.AlgorithmModel{}, err
	}

	model := models.AlgorithmModel{}
	err = json.Unmarshal(data, &model)
	return model, err
}

// Helper method to request and decode a device slice
func (d *algorithmRestClient) requestAlgorithmModelSlice(ctx context.Context, urlSuffix string) ([]models.AlgorithmModel, error) {
	data, err := clients.GetRequest(ctx, urlSuffix, d.urlClient)
	if err != nil {
		return []models.AlgorithmModel{}, err
	}

	mSlice := make([]models.AlgorithmModel, 0)
	err = json.Unmarshal(data, &mSlice)
	return mSlice, err
}

func (d *algorithmRestClient) AlgorithmModelList(ctx context.Context) ([]models.AlgorithmModel, error) {
	return d.requestAlgorithmModelSlice(ctx, "/model")
}

func (d *algorithmRestClient) GetAlgorithmModelById(ctx context.Context, id string) (models.AlgorithmModel, error) {
	return d.requestAlgorithmModel(ctx, "/model/id/"+id)
}

func (d *algorithmRestClient) GetAlgorithmModelByName(ctx context.Context, name string) (models.AlgorithmModel, error) {
	return d.requestAlgorithmModel(ctx, "/model/name/"+name)
}

func (d *algorithmRestClient) AddAlgorithmModel(ctx context.Context, model *models.AlgorithmModel) (string, error) {
	return clients.PostJSONRequest(ctx, "/model", model, d.urlClient)
}

func (d *algorithmRestClient) UpdateAlgorithmModelState(ctx context.Context, id string, req algorithmmodel.UpdateModelStateRequest) (string, error) {
	jsonStr, err := json.Marshal(req)
	if err != nil {
		return "", err
	}
	return clients.PutRequest(ctx, "/model/state/id/"+id, jsonStr, d.urlClient)
}

func (d *algorithmRestClient) UpdateAlgorithmModelVersion(ctx context.Context, id, version string) (string, error) {
	req := algorithmmodel.UpdateModelVersionRequest{
		Id:      id,
		Version: version,
	}
	return clients.PostJSONRequest(ctx, "/model/cloud/update", req, d.urlClient)
}

func (d *algorithmRestClient) DeleteAlgorithmModelById(ctx context.Context, id string) error {
	return clients.DeleteRequest(ctx, "/model/id/"+id, d.urlClient)
}

func (d *algorithmRestClient) DeleteAlgorithmModelByName(ctx context.Context, name string) error {
	return clients.DeleteRequest(ctx, "/model/name/"+name, d.urlClient)
}

func (d *algorithmRestClient) AlgorithmTaskList(ctx context.Context) ([]models.AlgorithmAnalyzeTask, error) {
	return d.requestAlgorithmTaskSlice(ctx, "/task")
}

func (d *algorithmRestClient) GetAlgorithmTaskById(ctx context.Context, id string) (models.AlgorithmAnalyzeTask, error) {
	return d.requestAlgorithmTask(ctx, "/task/id/"+id)
}

func (d *algorithmRestClient) GetAlgorithmTaskByName(ctx context.Context, name string) ([]models.AlgorithmAnalyzeTask, error) {
	return d.requestAlgorithmTaskSlice(ctx, "/task/name/"+name)
}

func (d *algorithmRestClient) AddAlgorithmTask(ctx context.Context, task *models.AlgorithmAnalyzeTask) (string, error) {
	return clients.PostJSONRequest(ctx, "/task", task, d.urlClient)
}

func (d *algorithmRestClient) AddAlgorithmTaskByCloud(ctx context.Context, task *models.AlgorithmAnalyzeTask) (string, error) {
	return clients.PostJSONRequest(ctx, "/task?source=cloud", task, d.urlClient)
}

func (d *algorithmRestClient) UpdateAlgorithmTaskState(ctx context.Context, id string, req algorithmtask.UpdateTaskStateRequest) (string, error) {
	jsonStr, err := json.Marshal(req)
	if err != nil {
		return "", err
	}
	return clients.PutRequest(ctx, "/task/state/id/"+id, jsonStr, d.urlClient)
}

func (d *algorithmRestClient) UpdateAlgorithmTask(ctx context.Context, task *models.AlgorithmAnalyzeTask) (string, error) {
	jsonStr, err := json.Marshal(task)
	if err != nil {
		return "", err
	}
	return clients.PutRequest(ctx, "/task/id/"+task.ID, jsonStr, d.urlClient)
}

func (d *algorithmRestClient) UpdateAlgorithmTaskByCloud(ctx context.Context, task *models.AlgorithmAnalyzeTask) (string, error) {
	jsonStr, err := json.Marshal(task)
	if err != nil {
		return "", err
	}
	return clients.PutRequest(ctx, "/task/id/"+task.ID+"?source=cloud", jsonStr, d.urlClient)
}

func (d *algorithmRestClient) DeleteAlgorithmTaskById(ctx context.Context, id string) error {
	return clients.DeleteRequest(ctx, "/task/id/"+id, d.urlClient)
}

func (d *algorithmRestClient) DeleteAlgorithmTaskByName(ctx context.Context, name string) error {
	return clients.DeleteRequest(ctx, "/task/name/"+name, d.urlClient)
}
