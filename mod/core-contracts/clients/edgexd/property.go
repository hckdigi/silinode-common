package edgexd

import (
	"context"
	"encoding/json"
	"strings"

	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/interfaces"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/requests/configuration"
)

type PropertyClient interface {
	Ping(ctx context.Context) (string, error)
	SystemMetrics(ctx context.Context) (string, error)
	SoftwareInfos(ctx context.Context) (string, error)

	HostProfiles(ctx context.Context) (map[string]interface{}, error)
	HostProperties(ctx context.Context, props []string) ([]interface{}, error)

	Operation(ctx context.Context, operation models.Operation) (string, error)
	// Configuration obtains configuration information from the target service.
	Configuration(ctx context.Context, services []string) (string, error)
	// SetConfiguration issues a set configuration request.
	SetConfiguration(ctx context.Context, services []string, request configuration.SetConfigRequest) (string, error)
	// Metrics obtains metrics information from the target service.
	Metrics(ctx context.Context, services []string) (string, error)
	// Health issues requests to get service health status
	Health(ctx context.Context, services []string) (string, error)

	DeployConfiguration(ctx context.Context, services []string, request map[string]interface{}) (string, error)
}

type propertyRestClient struct {
	urlClient interfaces.URLClient
}

// NewEventClient creates an instance of EventClient
func NewPropertyClient(urlClient interfaces.URLClient) PropertyClient {
	return &propertyRestClient{
		urlClient: urlClient,
	}
}

func (prc *propertyRestClient) Ping(ctx context.Context) (string, error) {
	body, err := clients.GetRequest(ctx, clients.ApiPingRoute, prc.urlClient)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func (rc *propertyRestClient) Operation(ctx context.Context, operation models.Operation) (string, error) {
	return clients.PostJSONRequest(ctx, clients.ApiOperationRoute, operation, rc.urlClient)
}

func (rc *propertyRestClient) Configuration(ctx context.Context, services []string) (string, error) {
	suffix := createSuffix(services)
	body, err := clients.GetRequest(ctx, clients.ApiConfigRoute+suffix, rc.urlClient)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func (rc *propertyRestClient) SetConfiguration(ctx context.Context, services []string, request configuration.SetConfigRequest) (string, error) {
	suffix := createSuffix(services)
	return clients.PostJSONRequest(ctx, clients.ApiConfigRoute+suffix, request, rc.urlClient)
}

func (rc *propertyRestClient) Metrics(ctx context.Context, services []string) (string, error) {
	suffix := createSuffix(services)
	body, err := clients.GetRequest(ctx, clients.ApiMetricsRoute+suffix, rc.urlClient)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

func (rc *propertyRestClient) Health(ctx context.Context, services []string) (string, error) {
	suffix := createSuffix(services)
	body, err := clients.GetRequest(ctx, clients.ApiHealthRoute+suffix, rc.urlClient)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

// DeployConfig
func (rc *propertyRestClient) DeployConfiguration(ctx context.Context, services []string, request map[string]interface{}) (string, error) {
	suffix := createSuffix(services)
	return clients.PostJSONRequest(ctx, clients.ApiConfigRoute+"/deploy"+suffix, request, rc.urlClient)
}

func createSuffix(services []string) string {
	suffix := "/" + strings.Join(services, ",")
	return suffix
}

func (prc *propertyRestClient) SystemMetrics(ctx context.Context) (string, error) {
	body, err := clients.GetRequest(ctx, clients.ApiBase+"/system/metrics", prc.urlClient)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func (prc *propertyRestClient) SoftwareInfos(ctx context.Context) (string, error) {
	body, err := clients.GetRequest(ctx, clients.ApiBase+"/system/software", prc.urlClient)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func (prc *propertyRestClient) HostProfiles(ctx context.Context) (map[string]interface{}, error) {
	data, err := clients.GetRequest(ctx, clients.ApiBase+"/host/profile", prc.urlClient)
	if err != nil {
		return nil, err
	}

	dataMap := make(map[string]interface{})
	err = json.Unmarshal(data, &dataMap)
	return dataMap, err
}

func (prc *propertyRestClient) HostProperties(ctx context.Context, props []string) ([]interface{}, error) {
	dataSlice := make([]interface{}, 0)
	for _, prop := range props {
		data, err := clients.GetRequest(ctx, clients.ApiBase+"/host/property/"+prop, prc.urlClient)
		if err != nil {
			return nil, err
		}
		di := new(interface{})
		json.Unmarshal(data, &di)
		dataSlice = append(dataSlice, di)
	}
	return dataSlice, nil
}
