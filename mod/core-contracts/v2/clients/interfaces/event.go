//
// Copyright (C) 2020 IOTech Ltd
//
// SPDX-License-Identifier: Apache-2.0

package interfaces

import (
	"context"

	"gitee.com/hckdigi/silinode-common/mod/core-contracts/errors"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/v2/dtos/common"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/v2/dtos/requests"
)

// EventClient defines the interface for interactions with the Event endpoint on the EdgeX Foundry core-data service.
type EventClient interface {
	// Add adds new events
	Add(ctx context.Context, reqs []requests.AddEventRequest) ([]common.BaseWithIdResponse, errors.EdgeX)
}
