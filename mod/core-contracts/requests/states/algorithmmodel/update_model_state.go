package algorithmmodel

const (
	ALGORITHM_MODEL_STATE_HEALTH    = "health"
	ALGORITHM_MODEL_STATE_MISSMODEL = "missModel"
)

type UpdateModelStateRequest struct {
	State string `json:"state"`
}

type UpdateModelVersionRequest struct {
	Id      string `json:"id"`
	Version string `json:"version"`
}
