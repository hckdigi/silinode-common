package algorithmtask

import (
	"encoding/json"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
)

type UpdateTaskStateRequest struct {
	State       models.AlgorithmTaskState `json:"state"`
	ErrorMsg    string                    `json:"errorMsg"`
	isValidated bool                      // internal member used for validation check
}

func (u UpdateTaskStateRequest) MarshalJSON() ([]byte, error) {
	test := struct {
		State models.AlgorithmTaskState `json:"state,omitempty"`
		ErrorMsg string                    `json:"errorMsg"`
	}{
		State: u.State,
		ErrorMsg: u.ErrorMsg,
	}

	return json.Marshal(test)
}

// UnmarshalJSON implements the Unmarshaler interface for the type
func (u *UpdateTaskStateRequest) UnmarshalJSON(data []byte) error {
	var err error
	type Alias struct {
		State models.AlgorithmTaskState `json:"state"`
		ErrorMsg string                    `json:"errorMsg"`
	}
	a := Alias{}

	// Error with unmarshal
	if err = json.Unmarshal(data, &a); err != nil {
		return err
	}

	u.State = a.State
	u.ErrorMsg = a.ErrorMsg
	u.isValidated, err = u.Validate()

	return err
}

// Validate satisfies the Validator interface
func (u UpdateTaskStateRequest) Validate() (bool, error) {
	if !u.isValidated {
		return u.State.Validate()
	}
	return u.isValidated, nil
}
