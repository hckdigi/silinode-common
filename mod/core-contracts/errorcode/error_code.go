package errorcode

const (
	InternalError      = "InternalError"
	UnavaliableService = "UnavaliableService"

	InvalidContentType     = "BadRequest.InvalidContentType"
	LimitExceeded          = "LimitExceeded"
	InvalidParameter       = "InvalidParameter"
	EmptyAddressable       = "InvalidParameter.EmptyAddressable"
	EmptyName              = "InvalidParameter.EmptyName"
	DuplicateName          = "InvalidParameter.DuplicateName"
	InvalidFileFormat      = "InvalidParameter.InvalidFileFormat"
	DeviceProfileFileMiss  = "InvalidParameter.DeviceProfileFileMiss"
	AlgorithmModelFileMiss = "InvalidParameter.AlgorithmModelFileMiss"
	DeviceServiceFileMiss  = "InvalidParameter.DeviceServiceFileMiss"
	InvalidEmailAddress    = "InvalidParameter.InvalidEmailAddress"
	InvalidTimeFormat      = "InvalidParameter.InvalidTimeFormat"
	TimeConflict           = "InvalidParameter.TimeConflict"
	InvalidFrequencyFormat = "InvalidParameter.InvalidFrequencyFormat"
	InvalidProvider        = "InvalidParameter.InvalidProvider"
	InavlidName            = "InvalidParameter.InvalidName"
	InvalidVersion         = "InvalidParameter.InvalidVersion"

	ResourceNotFound         = "ResourceNotFound"
	DeviceNotDound           = "Device.NotFound"
	AddressableNotFound      = "Addressable.NotFound"
	DeviceProfileNotFound    = "DeviceProfile.NotFound"
	DeviceServiceNotFound    = "DeviceService.NotFound"
	EventNotFound            = "Event.NotFound"
	ProvisionWatcherNotFound = "ProvisionWatcher.NotFound"
	ValueDescriptorsNotFound = "ValueDescriptors.NotFound"
	NotificationNotFound     = "Notification.NotFound"
	SubscriptionNotFound     = "Subscription.NotFound"
	TransmissionNotFound     = "Transmission.NotFound"
	IntervalNotFound         = "Interval.NotFound"
	IntervalActionNotFound   = "IntervalAction.NotFound"
	DeviceModelNotFound      = "DeviceModel.NotFound"
	AlgorithmModelNotFound   = "AlgorithmModel.NotFound"
	AlgorithmTaskNotFound    = "AlgorithmTask.NotFound"

	DeviceModelInvalidState    = "DeviceModel.InvalidState"
	AlgorithmModelInvalidState = "AlgorithmModel.InvalidState"

	DeviceProfileSynchronizationFailed = "DeviceProfile.SynchronizationFailed"

	UnsupportedCommand          = "Device.UnsupportedCommand"
	DeviceDisabled              = "Device.Disabled"
	DeviceSynchronizationFailed = "Device.SynchronizationFailed"
	DeviceDescriptionTooLong    = "Device.DescriptionTooLong"
	DeviceLabelTooLong          = "Device.LabelTooLong"
	DeviceResponseTimeout       = "Device.ResponseTimeout"

	DeviceServiceDisabled = "DeviceService.Disabled"

	DeviceModelDuplicateManufacturerAndTypeAndModel = "DeviceModel.DuplicateManufacturerAndTypeAndModel"
	DeviceModelInuse                                = "UnsupportedOperation.DeviceModelInUse"
	AddressableInUse                                = "UnsupportedOperation.AddressableInUse"
	DeviceProfileInUse                              = "UnsupportedOperation.DeviceProfileInUse"
	ValueDescriptionInUse                           = "UnsupportedOperation.ValueDescriptionInUse"
	IntervalInUse                                   = "UnsupportedOperation.IntervalInUse"
	AlgorithmModelInUse                             = "UnsupportedOperation.AlgorithmModelInUse"
	DeviceInUse                                     = "UnsupportedOperation.DeviceInUse"
	UnsupportedSystem                               = "UnsupportedOperation.UnsupportedSystem"
	MissDevice                                      = "UnsupportedOperation.MissDevice"
	AlgorithmTaskLimitExceeded                      = "UnsupportedOperation.RunningAlgorithmTaskLimitExceeded"

	DatabaseError = "InternelError.DatabaseError"

	UserNoAccess      = "User.NoAccess"      // 用户无权限
	UserNoLogin       = "User.NoLogin"       // 用户未登录或登录失效
	UserErrToken      = "User.ErrToken"      // 身份校验失败
	UserNotFound      = "User.NotFound"      // 用户不存在
	UserNameNotUnique = "User.NameNotUnique" // 用户名已存在
	UserErrNameOrPwd  = "User.ErrNameOrPwd"  // 用户名或密码错误

	OperationDeleteFailed = "Operation.DeleteFailed" // 删除失败

	RoleNameNotUnique = "Role.NameNotUnique" // 角色名已存在
	RoleInUsed        = "Role.InUsed"        // 角色被使用中

	RouteNotUnique = "Route.NotUnique" // 路由已被占用

	APINotUnique = "API.NotUnique" // API 已被占用

	CloudConnFailed = "Cloud.ConnFailed" // 云端连接失败
)
