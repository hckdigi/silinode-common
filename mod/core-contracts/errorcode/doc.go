// errorcode 包枚举了所有 EdgeX 服务返还给前端的错误代码，
// 该代码作为 HTTP 的 body 使用。
//
// 目前的前后端交互策略，不分系统级别错误和业务级别错误，
// 只要是错误，状态码均为非 200 系。
package errorcode
