commit 07d61b246d98f61721fafcbc5ed11d9264e2e8b7
Merge: 1a58ea2 318eed5
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Wed Sep 7 20:45:52 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts

commit 1a58ea27ee28ad5984cbd67685868ae1239b8706
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Wed Sep 7 20:45:35 2022 +0800

    feat: 1）增加了powers信息

commit 318eed5fe3ba494951743a610e9c171addc3754c
Merge: 525c832 2df9f43
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Tue Aug 30 08:04:30 2022 +0000

    Merge branch 'develop' into 'master'
    
    feat: 云端删除子设备接口增加 deleteAssociate 参数，表明是否删除关联资源
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!69

commit 2df9f436383f64f5b7f986850606857e8075ab1b
Author: psy <panshengyu@kaiwuren.com>
Date:   Tue Aug 30 16:01:29 2022 +0800

    feat: 云端删除子设备接口增加 deleteAssociate 参数，表明是否删除关联资源
    
    - 云端删除子设备接口增加 deleteAssociate 参数，表明是否删除关联资源

commit 525c832fe61729f1743c44c7aac493ae19a55c6b
Merge: 39577ad 7b9fa5f
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Tue Aug 30 01:45:59 2022 +0000

    Merge branch 'develop' into 'master'
    
    feat: 算法任务增加是否默认推https://gitee.com/hckdigi/silinode-common/mod/core-contracts/-/merge_requests/new送推理结果到corex字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!68

commit 7b9fa5f4f2148d47dadcbba5cf7950d33b16d73b
Author: psy <panshengyu@kaiwuren.com>
Date:   Tue Aug 30 09:40:22 2022 +0800

    feat: 算法任务增加是否默认推送推理结果到corex字段
    
    - 算法任务增加是否默认推送推理结果到corex字段

commit 39577adb7926618422beee8f2acdaf1c7f81d08d
Merge: 04aa402 403c7d0
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Tue Aug 23 01:21:02 2022 +0000

    Merge branch 'develop' into 'master'
    
    perf: 修改算法资源数据结构，EnvironmentParam字段改为结构数组形式
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!67

commit 403c7d078c722654ecb72c1578081491d3ba7660
Author: psy <panshengyu@kaiwuren.com>
Date:   Tue Aug 23 09:18:08 2022 +0800

    perf: 修改算法资源数据结构，EnvironmentParam字段改为结构数组形式
    
    - 修改算法资源数据结构，EnvironmentParam字段改为结构数组形式

commit 04aa4027ca55deaa8bcae627fde25498604f836b
Merge: c326495 f9fe8a3
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Mon Aug 22 02:12:12 2022 +0000

    Merge branch 'develop' into 'master'
    
    perf: 修改算法资源数据结构，增加执行参数配置、通信方式配置以及推理流标识
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!66

commit f9fe8a3e71aad7f2b17aa84a6f3d3afe4654a974
Author: psy <panshengyu@kaiwuren.com>
Date:   Mon Aug 22 10:11:13 2022 +0800

    perf: 修改算法资源数据结构，增加执行参数配置、通信方式配置以及推理流标识
    
    - 修改算法资源数据结构，增加执行参数配置、通信方式配置以及推理流标识

commit c326495363c4dbd3a7d9706a41e6932c2af9825d
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Sat Aug 20 13:58:44 2022 +0800

    feat: 高级配置中增加了 hwCodecEnable 参数

commit 7d39c0444a283247a628c6795910d390c344252f
Merge: 71a56fb cbf0a9d
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Wed Aug 17 21:44:19 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts
    
     Conflicts:
    	models/hostconfig.go

commit 71a56fbb1ac35c0ec9db86cb1070146fc6975a7c
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Wed Aug 17 21:40:51 2022 +0800

    feat: 1）修改status中的productInfo，2）在HostConfig中增加ProductInfo的基本配置

commit cbf0a9de8fa8648c787976b1402cabbead9e0384
Merge: e278395 e786ee4
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Tue Aug 9 02:05:48 2022 +0000

    Merge branch 'develop' into 'master'
    
    perf: 增加云端删除子设备接口
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!65

commit e786ee48b9e5304df9621fe01ac152310fdb0f74
Author: psy <panshengyu@kaiwuren.com>
Date:   Tue Aug 9 10:04:25 2022 +0800

    perf: 增加云端删除子设备接口
    
    - 增加云端删除子设备接口

commit e2783956682eca2ff5d7aa6ea6251ba5b8b085b2
Merge: aca5ec7 a58a430
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Mon Aug 8 06:40:30 2022 +0000

    Merge branch 'develop' into 'master'
    
    perf: 优化设备录像计划设置字段数据结构
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!64

commit a58a43029afbb5022f1d57637c24a34b820c14af
Author: psy <panshengyu@kaiwuren.com>
Date:   Mon Aug 8 14:39:51 2022 +0800

    perf: 优化设备录像计划设置字段数据结构
    
    - 优化设备录像计划设置字段数据结构

commit aca5ec72c8585e3f1fa33fa80834799ab4b5de0b
Merge: 1f9e137 9a17a5f
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Mon Aug 8 06:37:59 2022 +0000

    Merge branch 'develop' into 'master'
    
    perf: 优化设备录像计划设置字段数据结构
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!63

commit 9a17a5ffad655570a02fed69eb309a7757256936
Author: psy <panshengyu@kaiwuren.com>
Date:   Mon Aug 8 14:36:45 2022 +0800

    perf: 优化设备录像计划设置字段数据结构
    
    - 优化设备录像计划设置字段数据结构

commit 1f9e137fbd0548d22caffca01d495f4ec2d4522c
Merge: ad83df1 5b5ede3
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Thu Aug 4 10:19:43 2022 +0000

    Merge branch 'develop' into 'master'
    
    feat: add fields to host config struct
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!62

commit 5b5ede3cb076c60bde90dd122c158ddb5860a98b
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Thu Aug 4 18:12:56 2022 +0800

    feat: add fields to host config struct

commit ad83df1336ddabea1f14b552da67baa18fd9551c
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Tue Jul 26 22:49:59 2022 +0800

    feat: 恢复 deviceName 的配置，前向兼容

commit 79ecaa77020235e30560b20aceb269578360d061
Merge: d666c9f caea1da
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Mon Jul 25 21:50:15 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts

commit d666c9fc8b04ffdfad4b9bf65b4b2215a5b8b835
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Mon Jul 25 21:50:10 2022 +0800

    feat: 增加 hwcodec 配置选项

commit caea1da9cf59827e81165d5d9b972319ab15a690
Merge: a3d515a 164b0ea
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Mon Jul 25 06:14:53 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 修改视频设备视频通道录像设置字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!61

commit 164b0ea1fbba7fe7b1632fcdef70e990fcc0c11c
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Jul 25 14:09:09 2022 +0800

    perf: 修改视频设备视频通道录像设置字段
    
    - 修改视频设备视频通道录像设置字段

commit a3d515a14296228fb5ce1dbc23a3a4b4404f265f
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Sat Jul 23 10:27:52 2022 +0800

    feat: 增加ChipType、ChipModel，删除DeviceName；

commit 3ded407fd5de26ad3d6318a67f6445899d7a1df6
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Thu Jul 21 20:29:26 2022 +0800

    feat: 调整hostconfig定义，增加CoreModel、NetInfo等

commit 3c2b8ee3dc26baf8044a1bd54c52b8adb32ec555
Merge: faaa718 94467cb
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Thu Jul 21 08:42:38 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加视频设备视频通道录像设置字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!60

commit 94467cbf4db4473204b9bee72e6a1546e18ba25c
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jul 21 16:32:44 2022 +0800

    feat: 增加视频设备视频通道录像设置字段
    
    - 增加视频设备视频通道录像设置字段

commit faaa718d5cb8b8f6152011b0d6b3042e128f9cff
Merge: 5c240f7 43b2f59
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Fri Jul 15 01:44:21 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加算法任务创建时间、更新时间字段，增加云端更新算法任务接口
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!59

commit 43b2f5973fdf875d4a4cce6d3454d50a82f2fff2
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Fri Jul 15 09:43:20 2022 +0800

    feat: 增加算法任务创建时间、更新时间字段，增加云端更新算法任务接口
    
    - 增加算法任务创建时间、更新时间字段
    - 增加云端更新算法任务接口

commit 5c240f753117ea7cb24d07150be460f012da49eb
Merge: d89291f 0a4f328
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Thu Jul 14 02:43:26 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加云端创建算法任务，更新算法任务接口，增加算法任务创建来源字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!58

commit 0a4f328e8ae74101be448cce621891fe798e528f
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jul 14 10:42:18 2022 +0800

    feat: 增加云端创建算法任务，更新算法任务接口，增加算法任务创建来源字段
    
    - 增加云端创建算法任务，更新算法任务接口

commit d89291f842545bf89f6015f7cf8af7d34f80ab85
Merge: 42a1760 6f55cff
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Thu Jul 7 08:32:33 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法资源、算法任务、设备型号增加CloudId字段，存储云端资源id
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!57

commit 6f55cffb7f9ddfb58c2ad7b91feeecd77e03ee8d
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jul 7 16:10:46 2022 +0800

    feat: 算法资源、算法任务、设备型号增加CloudId字段，存储云端资源id
    
    - 算法资源、算法任务、设备型号增加CloudId字段，存储云端资源id

commit 42a176096adea48e7b582becf41c9595c2b57448
Merge: 230b90d 3685539
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Wed Jul 6 09:51:04 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复根据id查询设备型号接口路由不正确的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!56

commit 36855399b25ea8f31e8e8db7c8384683fc0ab045
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Jul 6 17:49:53 2022 +0800

    fix: 修复根据id查询设备型号接口路由不正确的问题
    
    - 修复根据id查询设备型号接口路由不正确的问题

commit 230b90d1ec7ef4c03d834fea7446c239945f3771
Merge: 9e16bb7 1c7b062
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Wed Jul 6 07:32:02 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加云端创建设备型号接口
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!55

commit 1c7b062ad803b888075146bed57efd2233113352
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Jul 6 09:17:23 2022 +0800

    feat: 增加云端创建设备型号接口
    
    - 增加云端创建设备型号接口

commit 9e16bb777f34ccb2e9ab454f981f861a52433ed4
Merge: c920ee6 07fcb75
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Wed Jun 29 02:27:16 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复云端下发算法资源类别转换大小写问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!54

commit 07fcb757e2c9801ab5dbc1925f60b06e465fea47
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Jun 29 10:24:05 2022 +0800

    fix: 修复云端下发算法资源类别转换大小写问题
    
    - 修复云端下发算法资源类别转换大小写问题

commit c920ee60a2ef7393c1081577431cc06468145e8e
Merge: 7be8086 3b01a0a
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Tue Jun 28 11:17:54 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法资源增加一个Path字段用于展示算法资源文件路径
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!53

commit 3b01a0abb87876b1a89a286ab7d77a77939c6632
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Jun 28 19:14:41 2022 +0800

    feat: 算法资源增加一个Path字段用于展示算法资源文件路径
    
    - 算法资源增加一个Path字段用于展示算法资源文件路径

commit 7be808629671207cdf1da673b825a843ae50949d
Merge: 75701c0 1373cb8
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Fri Jun 24 12:12:52 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复设备模板deviceServiceName读取不到的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!52

commit 1373cb8059beef3b90c0b1b9586bed6d6bf18ef4
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Fri Jun 24 20:07:45 2022 +0800

    fix: 修复设备模板deviceServiceName读取不到的问题
    
    - 修复设备模板deviceServiceName读取不到的问题

commit 75701c01fd208a3cb4ef1f673ed846b31d3d4cfd
Merge: 1256a58 29ea335
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Thu Jun 23 08:01:21 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法资源、设备型号增加云端版本字段，增加云端算法资源类型和执行方式转换方法
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!51

commit 29ea335b015b7a70198b648d9e5cfaccc085d346
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jun 23 15:58:49 2022 +0800

    feat: 算法资源、设备型号增加云端版本字段，增加云端算法资源类型和执行方式转换方法
    
    - 算法资源、设备型号增加云端版本字段
    - 增加云端算法资源类型和执行方式转换方法

commit 1256a58f0763508c4ff2d223c3063c63d6a641d6
Merge: a250389 e0c3308
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Wed Jun 22 08:16:23 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 设备模板增加绑定的设备服务名称字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!50

commit e0c33083cca2614d7d3e6f0f62f4e7eb3bc2378f
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Jun 22 16:15:34 2022 +0800

    feat: 增加几个错误码
    
    - 增加几个错误码

commit dba40de6a9d71d0e2d35f60a00ef026fd8f09ff0
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Jun 22 16:09:28 2022 +0800

    feat: 设备模板增加绑定的设备服务名称字段
    
    - 设备模板增加绑定的设备服务名称字段

commit a250389fae25551287365473f6b92e0ada7b6672
Merge: a12e4a7 8551e2f
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Thu Jun 9 09:53:26 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加更新设备在线状态接口
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!49

commit 8551e2fcb05621954955b03caa09023f201e65aa
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jun 9 17:50:53 2022 +0800

    feat: 增加更新设备在线状态接口
    
    - 增加更新设备在线状态接口

commit a12e4a7c7c98e1e2feb55d36cbef88628e7cb537
Merge: 447be9b cc6170c
Author: zengzhiwu <zengzhiwu@kaiwuren.com>
Date:   Mon Jun 6 02:48:54 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: add some agent type
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!48

commit cc6170ccc40eb6688c81bc6fa35dbb9229cfb252
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Mon Jun 6 10:23:59 2022 +0800

    feat: add some agent type

commit 447be9b931d65dd440d5c4e42d2333ee99e44f6f
Merge: 2e2702f c2aca8f
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Jun 1 01:37:43 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: CoreX云端配置信息增加HttpPort字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!47

commit c2aca8fb7705cada2b8cd974e7ef0d70af46be95
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Jun 1 09:37:29 2022 +0800

    feat: CoreX云端配置信息增加HttpPort字段
    
    - CoreX云端配置信息增加HttpPort字段

commit 2e2702f052867d38010749c53defac9db83f5382
Merge: 18b2735 8c34e1e
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu May 19 08:22:36 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加更新算法资源状态接口
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!46

commit 8c34e1e12d8cab0d32e57255284fc93eb4ee4dd0
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu May 19 16:22:00 2022 +0800

    feat: 增加更新算法资源状态接口
    
    - 增加更新算法资源状态接口

commit 18b273522a34dde53732fead6277bb2fff479902
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Tue May 17 17:54:37 2022 +0800

    fix: 修复 host property 接口定义错误

commit 6ae4bbfca709d91e287f82289447dceb7d3ebef2
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Tue May 17 15:34:21 2022 +0800

    feat: 添加 gpsInfo 属性值

commit 3ea398bb32645681bbe63802381f4e17235ea948
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Tue May 17 15:32:58 2022 +0800

    feat: 添加 gpsInfo 属性值

commit f271ba60ce894a91d024235683311c9351aabfcc
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Thu May 12 18:37:36 2022 +0800

    feat：edgexdClient 增加支持 SMA原有接口功能

commit f5bfacb3d5684ea33c88501318946ebf1ac9170a
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Sun May 8 10:52:38 2022 +0800

    feat：增加 EMA 的client

commit b31464f33686f46f3587e8b1d24a226fd243d7ec
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Sat May 7 18:35:43 2022 +0800

    feat：增加 EMA 的client

commit f805f9ee45f385921e56e6766cd54ba66229eb51
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 27 13:50:23 2022 +0800

    fix：修改 SystemStatus中 Gpus信息

commit 28e92ef81ea67d851e0c4ddf8362d088046fbbf7
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Sun Apr 24 21:35:39 2022 +0800

    feat：更新工作模式的定义

commit d20947b5fb22c9765f45c9ef4d4293faff44c630
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Thu Apr 21 11:04:29 2022 +0800

    feat：增加工作模式、指标采集周期 的字段

commit 0fdb493d3ec0d5812eebaa7f4b7f699dfd74f3f9
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Wed Apr 20 17:58:31 2022 +0800

    feat: add model CoreXSubdvcAuthInfo

commit c4905f164af3f16d4373da77216f72efe8d18480
Merge: 8ef5035 c6a48e4
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Apr 20 03:36:04 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复更新算法任务状态接口http method错误的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!45

commit c6a48e485baf32e71e31ce6cf304044e9c6a1c14
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Apr 20 11:35:37 2022 +0800

    fix: 修复更新算法任务状态接口http method错误的问题
    
    - 修复更新算法任务状态接口http method错误的问题

commit 8ef5035f4f93d3c6f361134d80569ed64c7145ad
Merge: a9d92a4 b183af9
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Apr 19 09:36:26 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法任务状态增加启动中状态
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!44

commit b183af96ae48be814ce021a8624d6c88535d00b7
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Apr 19 17:36:12 2022 +0800

    perf: 算法任务状态增加启动中状态
    
    - 算法任务状态增加启动中状态

commit a9d92a44fd476b7644a6a92dd52cf60cf0332e17
Merge: ad8b064 723ef40
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Apr 19 09:30:22 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法任务状态增加启动中状态
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!43

commit 723ef40a2039749c5d3a8247be64019dc3994779
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Apr 19 17:27:03 2022 +0800

    perf: 算法任务状态增加启动中状态
    
    - 算法任务状态增加启动中状态

commit ad8b064b8f707efb459886252db999379d21bbc2
Merge: a599282 316c09c
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Fri Apr 15 11:52:06 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts

commit a5992827e6f539de745877699641c23d69dc873d
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Fri Apr 15 11:51:58 2022 +0800

    fix: 统一属性字段定义，采用驼峰式命名。

commit 316c09c5a6b83f70ebfdabd9d7642ee6002565e5
Merge: 07d3bc3 f291c1c
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Apr 14 08:11:18 2022 +0000

    Merge branch 'dev' into 'master'
    
    修复设备协议参数类型错误的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!42

commit f291c1c04ac353db0cc8b5d68ecfe5b48d5dab17
Merge: 3205fef 07d3bc3
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Apr 14 16:11:17 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts into dev

commit 3205fef77a13de74dca06ebfaddc1b3b8defa953
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Apr 14 16:10:59 2022 +0800

    fix: 修复设备协议参数类型错误的问题
    
    - 修复设备协议参数类型错误的问题

commit 07d3bc3a4c2d51144780d431b5f04f11970cd7dc
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 13 20:17:59 2022 +0800

    feat: 增加 host 通知事件类型

commit 7a03f6eac50661f47e5df581f6384f744c5603f2
Merge: 07ca965 4ca010c
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 13 18:09:29 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts

commit 07ca9659ffcfe3aed2699b1eec91fe7fb5b4ada6
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 13 18:09:11 2022 +0800

    feat: 增加 host 通知事件类型

commit 4ca010c08f7329a0328ec6848527206f61a0237f
Merge: ab8b045 fb04818
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Apr 13 08:26:08 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法任务存储方式增加http方式配置
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!41

commit fb0481812c56a7cf298b2809e94556fc752f5a16
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Apr 13 16:24:27 2022 +0800

    feat: 算法任务存储方式增加http方式配置
    
    - 算法任务存储方式增加http方式配置

commit ab8b045b04a003ade63166e9f64bdb6df3f55d5b
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 13 15:59:50 2022 +0800

    feat: 增加了host 通用性运行指标

commit cf08520e8f115e286b162ee913ac77780b73fbae
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 13 12:34:35 2022 +0800

    feat: 增加了host event定义

commit cc7ef1bd485749305bec11779ba76e040e88a400
Merge: 94f9897 121f936
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 13 11:24:11 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts

commit 94f9897e26169024863b4d6872518a5331b34bf2
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 13 11:23:53 2022 +0800

    feat: 增加了device_host 代理

commit 121f936c3abf92a74bbdec17d165516743c1c540
Merge: 5ab23dd 592fdd6
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Apr 12 01:32:53 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 设备模板可选设备协议模板字段增加默认值字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!40

commit 592fdd61d87645b102f5fb9c96bfee21aa8f7697
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Apr 12 09:32:27 2022 +0800

    feat: 设备模板可选设备协议模板字段增加默认值字段
    
    - 设备模板可选设备协议模板字段增加默认值字段

commit 5ab23ddcf6a4ac7ee0cf8090e51226314f30721e
Merge: 5f35970 2f2323d
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Mon Apr 11 22:24:45 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts

commit 5f359708960159c06e328c3ebbd9eaefe503a6ee
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Mon Apr 11 22:24:14 2022 +0800

    feat: 在系统状态中 增加了 NvControl的运行状态

commit 2f2323dfa7c26cea5f9c832b63cb5ad788a5bb15
Merge: 9f5aea8 42d272d
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Apr 11 08:40:04 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 优化 设备协议模板 定义
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!39

commit 42d272d9d15a1a448c435199b26a4d66b2027017
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Apr 11 16:39:51 2022 +0800

    perf: 优化 设备协议模板 定义
    
    - 优化 设备协议模板 定义

commit 9f5aea8eb52246ef9fdb55023bf8f64603ad596b
Merge: 55eab11 92813a9
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Apr 11 08:35:32 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 设备模板增加可选设备协议模板字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!38

commit 92813a9c015b61a9b3e19ce960a9570da999b30a
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Apr 11 16:35:09 2022 +0800

    feat: 设备模板增加可选设备协议模板字段
    
    - 设备模板增加可选设备协议模板字段

commit 55eab117b22de0265cd20566301e545cc140f585
Merge: 4e2acf9 b66c759
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Apr 11 08:11:07 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 设备模板增加可选设备协议模板字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!37

commit b66c7598cd25466e99688d4eab8c5a5144062db4
Merge: 84e4193 4e2acf9
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Apr 11 15:47:03 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts into dev

commit 84e4193919dd36076e54b19328ac13599c3c92e9
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Apr 11 15:46:42 2022 +0800

    feat: 设备模板增加可选设备协议模板字段
    
    - 设备模板增加可选设备协议模板字段

commit 4e2acf9e1e16900f0097c1e7b2a756690ebfb09b
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Sun Apr 10 17:08:46 2022 +0800

    feat: 增加 EMANvCtrlURL 路由

commit 1bf8a1d4ce93e94b78c4f04aa72d5185c8f63633
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Sun Apr 10 15:07:45 2022 +0800

    feat: 补充主机的 Status、Config 的字段属性定义；支持Nvidia指令、状态查询

commit f37f3c719f93f54c47bc69a3e5cdee790b2beb64
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Sun Apr 10 14:19:40 2022 +0800

    feat: 补充主机的 Status、Config 的字段属性定义；支持Nvidia指令、状态查询

commit ee4f5b6547d2260949b9c5506feadd1192dc794d
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 6 22:41:14 2022 +0800

    feat: hostconfig.go 增加 ruleengine 相关的API

commit 6715899498b1b305bbc6551ce8587a42f6496cb1
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 6 20:02:08 2022 +0800

    feat: hostconfig.go 增加 ruleengine 相关的API

commit c02166f62dfb81de4d932dc1fb6183f50a88d383
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 6 20:02:08 2022 +0800

    feat: hostconfig.go 增加 ruleengine 相关的API

commit 58c4c03233b6ce4646778579085c57c1ad7de2a9
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed Apr 6 13:44:18 2022 +0800

    feat: 增加 hostconfig.go 处理EdgeX网关设备的配置项目

commit fe407fa4359ab4222c214c007c7a91eebc45d2ed
Merge: 4139dc0 bb62adf
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Apr 6 02:00:51 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法任务配置增加推理流输出地址配置
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!36

commit bb62adf71ddd711ca3fec0718f84d5ad748552a5
Merge: 187a400 4139dc0
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Apr 6 09:59:14 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts into dev

commit 187a4000ed6310813e079927f50e4c9e47046d92
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Apr 6 09:58:50 2022 +0800

    perf: 算法任务配置增加推理流输出地址配置

commit 4139dc0659c9ee96da2f36956b2041619174379b
Merge: 48a3f41 94612ee
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Sat Apr 2 07:25:19 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法任务配置修改ftpServer方式存储配置
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!35

commit 94612ee9bb3455cbea625ad2e0961b0104256752
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Sat Apr 2 15:23:51 2022 +0800

    perf: 算法任务配置修改ftpServer方式存储配置

commit 48a3f413b46c1ee0809d9d22a38f77840be3f8f4
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Thu Mar 31 17:48:34 2022 +0800

    feat: add model PlatformConfigCoreX

commit 405438937938ff4f6c30cf7ecaae54c87e4051f7
Merge: 3be7c69 fbf6607
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 31 06:46:52 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法任务配置修改，增加mqtt/kafka topic 增加属性及其类型设置
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!34

commit fbf660770ec154be9a5f118409537d3ed57fc579
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 31 14:39:53 2022 +0800

    perf: 算法任务配置修改，增加mqtt/kafka topic 增加属性及其类型设置
    - 算法任务配置修改，增加mqtt/kafka topic 增加属性及其类型设置

commit 3be7c69c074d3135f96ec1c7d8569b96082741d4
Merge: ef816a8 6313726
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Mar 30 03:05:32 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法任务配置修改，增加mqtt/kafka多topic订阅设置，算法模型增加执行方式配置
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!33

commit 63137264b0cff062604ef32c9ab474be14e11ad1
Merge: 302625b ef816a8
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Mar 30 11:04:32 2022 +0800

    Merge branch 'master' of ssh://gitlab.kaiwuren.com:9002/iotcloud/edgex/edgex-mod/go-mod-core-contracts into dev

commit 302625bca06ccbbe974c423e6f9bf213fa861db5
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Mar 30 11:03:33 2022 +0800

    perf: 算法任务配置修改，增加mqtt/kafka多topic订阅设置，算法模型增加执行方式配置
    - 算法任务配置修改，增加mqtt/kafka多topic订阅设置
    - 算法模型增加执行方式配置

commit ef816a8c302e0a67e88df07ebccbb6d7ff31eb15
Merge: ec4cbfa 714bfb8
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Fri Mar 25 01:33:46 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法任务配置修改，增加执行方式配置，通信方式配置，存储方式配置,用户自定义配置
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!32

commit 714bfb8c781e9fce9f1094d145f4f141c94c8e4e
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 24 18:01:16 2022 +0800

    feat: 算法任务配置修改，增加执行方式配置，通信方式配置，存储方式配置,用户自定义配置
    - 算法任务配置修改，增加执行方式配置，通信方式配置，存储方式配置,用户自定义配置

commit ec4cbfaa2bebc424b35e025fb7ebece4719aa922
Author: Xavier <zhaoxingya@kaiwuren.com>
Date:   Thu Mar 24 09:52:10 2022 +0000

    Update constants.go

commit 335e0d1fe90cd3ebf307e5a12482b434e37ddcae
Merge: 42d306e 76b0676
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Fri Mar 18 07:43:13 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 修改算法任务输出配置参数
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!31

commit 76b0676c582975588c72e646423cf8825c66b83b
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Fri Mar 18 15:42:54 2022 +0800

    perf: 修改算法任务输出配置参数
    
    - 修改算法任务输出配置参数

commit 42d306efeeccdff8936971fea7b0dd2c1831b53f
Merge: f010b3f a50e6a8
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Fri Mar 18 01:32:03 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法任务增加数据输出配置字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!30

commit a50e6a8f4f720a4b8c0957108d9591870be8b6c3
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Fri Mar 18 09:31:32 2022 +0800

    feat: 算法任务增加数据输出配置字段
    - 算法任务增加数据输出配置字段

commit f010b3f5c7750f3b8c5e1c22c6fec362bce7d8a8
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Thu Mar 17 20:37:17 2022 +0800

    feat: 支持 Post 请求返回状态码 201 的情况

commit c19c3bde79f883734fb491e41574845233229f2d
Merge: 69ec7c9 ac7e965
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 17 08:44:08 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法任务增加图片输出保存路径字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!29

commit ac7e965967ab9528e9033b66ca7d224821ca2940
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 17 16:43:45 2022 +0800

    feat: 算法任务增加图片输出保存路径字段
    - 算法任务增加图片输出保存路径字段

commit 69ec7c9a34c39fa042d33f94db6535eb32db564f
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Thu Mar 17 11:06:39 2022 +0800

    feat: add service key for rulesengine

commit 4d6b98e455b13e608da7b7225314dcd21b0363a9
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Tue Mar 15 16:26:05 2022 +0800

    feat: add position model

commit 17070f0fb288a4e4e7269ca34aeb892b52c0beb6
Merge: 20e7c97 1f16745
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 10 06:39:13 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法任务增加描述字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!28

commit 1f16745df89c3dd7124f4fa1948430a50fb27101
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 10 14:38:51 2022 +0800

    feat: 算法任务增加描述字段
    - 算法任务增加描述字段

commit 20e7c97d46dd81aeafca48f893b90b0d0ef194b1
Merge: 35cc967 ac377ea
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 10 03:18:38 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 算法模型增加厂商、状态字段，增加几个错误码
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!27

commit ac377eac7948ff641a556b46e83547d310819ccd
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 10 11:18:21 2022 +0800

    feat: 算法模型增加厂商、状态字段，增加几个错误码
    - 算法模型增加厂商、状态字段，增加几个错误码

commit 35cc967a3eaebadfb932f74ab1cb36ed199b1cc3
Merge: c82921b 2cf841a
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 3 01:46:19 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: SMA服务接口增加获取系统信息接口
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!26

commit 2cf841a80832949dbb1d945ace3260a867379492
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Mar 3 09:45:56 2022 +0800

    feat: SMA服务接口增加获取系统信息接口
    
    - SMA服务接口增加获取系统信息接口

commit c82921b3ac00f708dd3bb7d295285fba0dd123a3
Merge: 8162e25 9ce96e8
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Mar 2 06:26:14 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复更新算法任务状态接口路由错误的问题，增加错误码
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!25

commit 9ce96e83c9c2eab7a4306595fca5cbe4cdb70b25
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Mar 2 14:25:56 2022 +0800

    fix: 修复更新算法任务状态接口路由错误的问题，增加错误码
    
    - 修复更新算法任务状态接口路由错误的问题
    - 增加错误码

commit 8162e2545a58256f57bb6186e281f7f2171a9de1
Merge: af27a9e e067daf
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Mar 2 03:59:53 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 优化算法任务状态字段校验，增加更新算法任务状态接口
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!24

commit e067daf4ae3a465e291a5d778f10f1255340a260
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Mar 2 11:59:26 2022 +0800

    perf: 优化算法任务状态字段校验，增加更新算法任务状态接口
    
    - 优化算法任务状态字段校验
    - 增加更新算法任务状态接口

commit af27a9eec83b784d38d6861eeedfc83af30028c8
Merge: 83b7a20 cfb3adb
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 08:09:53 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法模型类增加字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!23

commit cfb3adbce178ac4c8e4ba729441f7ca4074d4c45
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 16:09:40 2022 +0800

    perf: 算法模型类增加字段
    
    - 算法模型类增加字段

commit 83b7a20c102782652c56a62ad60eaeab653decab
Merge: 3bdaf10 3156dcc
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 07:47:38 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法任务类增加字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!22

commit 3156dcce36ddc3828809dc44ec78fad13e5ddec0
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 15:47:20 2022 +0800

    perf: 算法任务类增加字段
    
    - 算法任务类增加字段

commit 3bdaf10188af94de5fc926dd2fce7c4dc962d78b
Merge: 6fff75f a14b09b
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 07:19:47 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 算法模型类增加id字段方便管理
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!21

commit a14b09b73c01019228bc5909112c7ed9ca9007a6
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 15:19:36 2022 +0800

    perf: 算法模型类增加id字段方便管理
    
    - 算法模型类增加id字段方便管理

commit 6fff75f1f6b003f65dc87499645f223791b0fd74
Merge: d11df4a 9146180
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 07:12:13 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加视频分析服务相关的常数定义
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!20

commit 9146180b80dcbf23ef75fdc289118b602c16c231
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 15:11:56 2022 +0800

    feat: 增加视频分析服务相关的常数定义
    
    - 增加视频分析服务相关的常数定义

commit d11df4ab0c611fcf3ec252dadbe57894df823cca
Merge: e74d5a9 da11cc7
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 07:07:08 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加算法相关client接口定义以及类型定义，device增加一个获取视频设备rtsp流地址的接口
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!19

commit da11cc7ca7dafa02a6b96d347ac45cc95b82886d
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 28 15:02:38 2022 +0800

    feat: 增加算法相关client接口定义以及类型定义，device增加一个获取视频设备rtsp流地址的接口
    
    - 增加算法相关client接口定义以及类型定义
    - device增加一个获取视频设备rtsp流地址的接口

commit e74d5a98ecb50fd791450fc5f745eb5f697ab5d6
Merge: 4c0cc06 3deb1e3
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Feb 23 09:39:01 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加一个云端方式创建设备的接口（不用再次绑定设备）,设备反序列化时，判断lastConnect时间与当前时间差，超过12小时视为离线
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!18

commit 3deb1e3375b2b225be131baf413cfd27e768a228
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Feb 23 17:30:51 2022 +0800

    feat: 增加一个云端方式创建设备的接口（不用再次绑定设备）,设备反序列化时，判断lastConnect时间与当前时间差，超过12小时视为离线
    
    - 设备增加在线状态字段

commit 4c0cc06d72efde072db74148aa8e3a4fda2769a4
Merge: e860508 b1e2536
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Wed Feb 23 08:24:34 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 设备增加在线状态字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!17

commit b1e2536a8abff69864c122f8bda4b12b3ddc9b2b
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Feb 23 15:43:34 2022 +0800

    feat: 设备增加在线状态字段
    
    - 设备增加在线状态字段

commit e2095b6d5824f2236218ba5c49c3ea4d8c543f8a
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Mon Feb 21 16:57:28 2022 +0800

    feat: 设备模板内增加事件定义
    
    - 设备模板内增加事件定义

commit e8605085eb705fcec8f41c2f293f1352b1f3fd13
Merge: 8c7d63a e2095b6
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Mon Feb 21 08:56:26 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 设备模板内增加事件定义
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!16

commit 8c7d63ac5beaf8d215cc923a553769a813aaca17
Merge: ec1e4db 5051d00
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Wed Feb 16 03:38:01 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 设备型号类增加版本属性
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!15

commit 5051d00d7931e8100c833fde05fb783051e81479
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Feb 16 11:04:57 2022 +0800

    feat: 设备型号类增加版本属性
    
    - 设备型号类增加版本属性

commit ec1e4db593fcd3d8892a9897e4efd5a827932713
Merge: 86a16f4 5693b6b
Author: Xavier <zhaoxingya@kaiwuren.com>
Date:   Wed Feb 9 07:20:18 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复根据厂商、类型、型号查询设备型号接口路由错误的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!14

commit 5693b6ba905aebafc0d4f3647baa8d9c6fcfbfdd
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Feb 9 15:19:45 2022 +0800

    fix: 修复根据厂商、类型、型号查询设备型号接口路由错误的问题
    
    - 修复根据厂商、类型、型号查询设备型号接口路由错误的问题

commit 86a16f440db684c95322e48c132690ed47a29bca
Merge: 124ef00 07df46e
Author: Xavier <zhaoxingya@kaiwuren.com>
Date:   Wed Feb 9 07:09:06 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 临时打印一下接口路径
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!13

commit 07df46e5c703d4ec9efd5964bed6cdaaa7704ff9
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Feb 9 15:07:23 2022 +0800

    fix: 临时打印一下接口路径
    
    - 临时打印一下接口路径

commit 124ef004d7dcf0669c6d78df84f0dd01c8fbb737
Merge: 9fbc013 e28106e
Author: Xavier <zhaoxingya@kaiwuren.com>
Date:   Wed Feb 9 06:54:51 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复根据厂商、类型、型号寻找设备型号时未对参数进行url编码导致路由错误的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!12

commit e28106e4a706b14f32a4384e6cef6a5e3719869c
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Feb 9 14:53:47 2022 +0800

    fix: 修复根据厂商、类型、型号寻找设备型号时未对参数进行url编码导致路由错误的问题
    
    - 修复根据厂商、类型、型号寻找设备型号时未对参数进行url编码导致路由错误的问题

commit 9fbc013d0225289d5b5160ba8250d03c58bafecf
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Mon Feb 7 16:14:23 2022 +0800

    feat: add errcode

commit dc906c3769d5152448d8ec5be420064ff0e5e321
Merge: beb2889 ed3dad3
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Wed Jan 19 08:52:07 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复反序列化device未设置Service和Profile，导致其他服务调用接口后字段缺失的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!11

commit ed3dad3d736eee9ee3814b7010e9e0880db3ba83
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Wed Jan 19 16:48:54 2022 +0800

    fix: 修复反序列化device未设置Service和Profile，导致其他服务调用接口后字段缺失的问题
    
    - 修复反序列化device未设置Service和Profile，导致其他服务调用接口后字段缺失的问题

commit beb28898e88482f226264737e08c0c483583fcab
Merge: bd2fff7 6cc6581
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Tue Jan 18 11:27:38 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复序列化device未序列化model字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!10

commit 6cc65816b6a8bcdb00349fc8a8b3ad0bbbd56303
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Jan 18 19:25:54 2022 +0800

    fix: 修复序列化device未序列化model字段
    
    - 修复序列化device未序列化model字段

commit bd2fff77a7996f86ede11da371a0595786a79414
Merge: 986031d 2756208
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Tue Jan 18 09:16:32 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复创建设备时，反序列化设备时因未传Service和Profile而导致失败的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!9

commit 2756208c20b0554f4028ab08eef73aacf2731819
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Jan 18 17:14:08 2022 +0800

    fix: 修复创建设备时，反序列化设备时因未传Service和Profile而导致失败的问题
    
    - 修复创建设备时，反序列化设备时因未传Service和Profile而导致失败的问题

commit 986031d2266de1f52d2efd4dbc4f5b113434bda9
Merge: bc4691d 8bbffb5
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Tue Jan 18 08:58:37 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复创建设备时，反序列化设备时因未传Service和Profile而导致失败的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!8

commit 8bbffb5cca70e8a16b32062f43bd84da461dd72a
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Jan 18 16:57:21 2022 +0800

    fix: 修复创建设备时，反序列化设备时因未传Service和Profile而导致失败的问题
    
    - 修复创建设备时，反序列化设备时因未传Service和Profile而导致失败的问题

commit bc4691d1a45c15b370d5aa1253ce8fb0ad6eba13
Merge: 3d0dc8c 1c74ce8
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Fri Jan 14 01:33:45 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 设备型号增加来源字段，设备模板修复反序列化未设置设备类型的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!7

commit 1c74ce876e97c65887a5e9fd3fb2c4170c0b304b
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Fri Jan 14 09:31:33 2022 +0800

    feat: 设备型号增加来源字段，设备模板修复反序列化未设置设备类型的问题
    
    - 设备型号增加来源字段
    - 设备模板修复反序列化未设置设备类型的问题

commit 3d0dc8cb84a2aa8f0a8709e6d9e454d9522ba4f2
Merge: 224b83d a15167d
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Thu Jan 13 07:58:44 2022 +0000

    Merge branch 'dev' into 'master'
    
    perf: 增加设备型号模块错误码定义，增加设备型号下设备数量字段及序列化方法
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!6

commit a15167d135ad72c99773a6a8aa6f1e62f13a41b3
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jan 13 15:31:25 2022 +0800

    perf: 增加设备型号模块错误码定义，增加设备型号下设备数量字段及序列化方法
    
    - 增加设备型号模块错误码定义
    - 增加设备型号下设备数量字段及序列化方法

commit 224b83d189a97f9bd991a0f9b26070a252bd60c2
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Wed Jan 12 09:54:01 2022 +0800

    docs: errorcode package

commit c01faedaaf76b2e14551b7bd49fa3d251779dbbe
Author: Xavier Zhao <xvrzhao@gmail.com>
Date:   Tue Jan 11 17:34:24 2022 +0800

    feat: add some error codes

commit 7d2c5a9c8d469b0c229c48af20d75672280894cc
Merge: 9fe9f28 a413c39
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Tue Jan 11 08:06:51 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 将错误码定义移入
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!5

commit a413c39a9479bc26ebf729c0652eee4d703569e0
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Jan 11 16:02:22 2022 +0800

    feat: 将错误码定义移入
    
    - 将错误码定义移入

commit 9fe9f289def85d493bf83b94b5384bab9d588332
Merge: 17bc237 a8249b5
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Tue Jan 11 05:55:31 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 增加deviceModel接口调用client，deviceProfile增加型号、版本、版本描述字段，device增加设备型号，是否是视频类设备字段
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!4

commit a8249b578baa2f885be129052a02a66b8b447825
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Tue Jan 11 11:09:30 2022 +0800

    feat: 增加deviceModel接口调用client，deviceProfile增加型号、版本、版本描述字段，device增加设备型号，是否是视频类设备字段
    
    - 增加deviceModel接口调用client
    - deviceProfile增加型号、版本、版本描述字段
    - device增加设备型号，是否是视频类设备字段

commit 17bc23796910bab2a71dd781af703b84c8d75f95
Merge: 72b514a acdcb83
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Thu Jan 6 08:30:44 2022 +0000

    Merge branch 'dev' into 'master'
    
    fix: 修复新增的消息类别无法通过验证的问题
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!3

commit acdcb83946a99b61db7809f228b4e8412013be26
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jan 6 16:29:04 2022 +0800

    fix: 修复新增的消息类别无法通过验证的问题
    
    - 修复新增的消息类别无法通过验证的问题

commit 72b514a6e94e45eac398e147e3b0d46410c3acad
Merge: d844e41 2f7a86f
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Thu Jan 6 01:51:42 2022 +0000

    Merge branch 'dev' into 'master'
    
    feat: 告警消息增加几种消息类别
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!2

commit 2f7a86fce0c44f1b0e1fb20797fdb36be71f391b
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jan 6 09:50:28 2022 +0800

    perf: 告警消息消息类别改为英文
    
    - 告警消息消息类别改为英文

commit c364da3d18ec59c407436875a90e98d64ed93173
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Jan 6 09:45:06 2022 +0800

    feat: 告警消息增加几种消息类别
    
    - 告警消息增加几种消息类别

commit d844e41a24744bf1a896d26dba627f50ee27fd53
Merge: 5485706 fde5aa1
Author: 张守勇 <zhangshouyong@kaiwuren.com>
Date:   Thu Dec 16 08:39:34 2021 +0000

    Merge branch 'dev' into 'master'
    
    Dev
    
    See merge request iotcloud/edgex/edgex-mod/go-mod-core-contracts!1

commit fde5aa1cea6b5cc7e4c617a5b944e852f3ec1968
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Dec 16 16:16:29 2021 +0800

    feat: 增加设备型号定义
    
    - 增加设备型号定义

commit ca353e526dad67f6361fc2eab867c5295288ef8d
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Dec 16 15:57:18 2021 +0800

    perf: 修改command client增加对设备功能的调用接口的方式从put改为post
    
    - 修改command client增加对设备功能的调用接口的方式从put改为post

commit 034f3d79e01de8bd2062cb14496c56d2ac5be932
Author: panshengyu <panshengyu@kaiwuren.com>
Date:   Thu Dec 16 15:51:59 2021 +0800

    feat: 设备模板增加设备功能定义和默认的自动事件，command client增加对设备功能的调用接口
    
    - 设备模板增加设备功能定义
    - 设备模板增加默认的自动事件
    - command client增加对设备功能的调用接口

commit 5485706f55e35f7a45360485326218fd4554f7ed
Author: zhangshouyong <zhangshouyong@kaiwuren.com>
Date:   Fri Aug 27 11:12:15 2021 +0800

    feat: smaClient 增加 deployConfig 方法

commit c3eae4de83984876b2ffab06dd348c4e90731a1a
Author: zhangshouyong <zhangshouyong@kaiwuren.com>
Date:   Thu Aug 19 17:25:34 2021 +0800

    feat: smaClient 增加 ping 方法

commit ba04bd9215919387444d09ccd522a07edc54f3d3
Author: zhangshouyong <zhangshouyong@kaiwuren.com>
Date:   Fri Aug 13 22:01:45 2021 +0800

    chore: 自定义版本 v1.4.1

commit 8ca6dca95a6531c42df774e922437eb629ffc8e4
Author: zhangshouyong <zhangshouyong@kaiwuren.com>
Date:   Fri Aug 13 21:57:47 2021 +0800

    init: 官方v0.1.119

commit 7b170a6c8f305828ea0c735750a42e17ad28766f
Author: zhangshouyong <zhangshouyong@kaiwuren.com>
Date:   Fri Aug 13 10:57:44 2021 +0000

    更新.gitignore
