version 1.4.1

commit 70c9494d0570ed9326a4b1a1f8abeeecf5820c9a
Author: zhang <zhangshouyong@kaiwuren.com>
Date:   Wed May 11 14:10:42 2022 +0800

    feat：增加 UTC-8时间配置

commit 8da289134d7a781c656af2db0bdeac80cb1d5fd9
Author: Xavier <zhaoxingya@kaiwuren.com>
Date:   Wed Jan 26 02:21:12 2022 +0000

    Update variables.go

commit 54e294b8838954c4d520603aff303ca5336140a0
Author: zhangshouyong <zhangshouyong@kaiwuren.com>
Date:   Fri Aug 13 22:31:18 2021 +0800

    chore: 基本版本，修改包名 v1.4.1

commit 1ea35be7eed8fe548b72fd977498e765d207d2cd
Author: zhangshouyong <zhangshouyong@kaiwuren.com>
Date:   Fri Aug 13 22:24:24 2021 +0800

    init：官方版本v0.0.60（适用于hani版本）

commit 43c5c85340c3613b1bf16d3e93dbf84a3532bff6
Author: zhangshouyong <zhangshouyong@kaiwuren.com>
Date:   Fri Aug 13 10:52:10 2021 +0000

    更新Dockerfile
