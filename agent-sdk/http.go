package edgexagentsdk

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/utils"
)

func bootHttpService() {
	gin.SetMode(gin.ReleaseMode)

	r := gin.New()
	r.Use(gin.Recovery())

	r.POST("/api/agent-sdk/event-report", eventReportHandler)
	r.POST("/api/agent-sdk/gateway-event-report", gatewayEventReportHandler)
	r.POST("/api/agent-sdk/bind-device", bindDeviceHandler)
	r.POST("/api/agent-sdk/unbind-device", unbindDeviceHandler)
	r.POST("/api/agent-sdk/device-online-status", setDeviceOnlineStatusHandler)
	r.GET("/api/agent-sdk/edgex-online-status", getEdgexOnlineStatusHandler)

	go func() {
		if err := r.Run(":8080"); err != nil {
			err = fmt.Errorf("failed to start http server: %w", err)
			utils.LogError(err.Error())
			panic(err)
		}
	}()
}

func eventReportHandler(c *gin.Context) {
	type Payload struct {
		DeviceName string                 `json:"deviceName" binding:"required"`
		EventID    string                 `json:"eventId" binding:"required"`
		EventProps map[string]interface{} `json:"eventProps" binding:"required"`
	}
	var payload Payload

	if err := c.ShouldBindJSON(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	utils.LogInfo("Received HTTP event: %s, content: %+v", "EventReport", payload)

	if err := deviceEventReportCallback(payload.DeviceName, payload.EventID, payload.EventProps); err != nil {
		err = fmt.Errorf("failed to call user's deviceEventReportCallback: %w", err)
		utils.LogError(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "success",
	})
}

func gatewayEventReportHandler(c *gin.Context) {
	type Payload struct {
		EventID    string                 `json:"eventId" binding:"required"`
		EventProps map[string]interface{} `json:"eventProps" binding:"required"`
	}
	var payload Payload

	if err := c.ShouldBindJSON(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	utils.LogInfo("Received HTTP event: %s, content: %+v", "GatewayEventReport", payload)

	if err := gatewayEventReportCallback(payload.EventID, payload.EventProps); err != nil {
		err = fmt.Errorf("failed to call user's gatewayEventReportCallback: %w", err)
		utils.LogError(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "success",
	})
}

func bindDeviceHandler(c *gin.Context) {
	type Payload struct {
		ProductID  string `json:"productId" binding:"required"`
		DeviceName string `json:"deviceName" binding:"required"`
		DevicePSK  string `json:"devicePsk" binding:"required"`
	}
	var payload Payload

	if err := c.ShouldBindJSON(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	utils.LogInfo("Received HTTP event: %s, content: %+v", "BindDevice", payload)

	if err := bindDeviceCallback(payload.ProductID, payload.DeviceName, payload.DevicePSK); err != nil {
		err = fmt.Errorf("failed to call user's bindDeviceCallback: %w", err)
		utils.LogError(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "success",
	})
}

func unbindDeviceHandler(c *gin.Context) {
	type Payload struct {
		ProductID  string `json:"productId" binding:"required"`
		DeviceName string `json:"deviceName" binding:"required"`
	}
	var payload Payload

	if err := c.ShouldBindJSON(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	utils.LogInfo("Received HTTP event: %s, content: %+v", "UnbindDevice", payload)

	if err := unbindDeviceCallback(payload.ProductID, payload.DeviceName); err != nil {
		err = fmt.Errorf("failed to call user's unbindDeviceCallback: %w", err)
		utils.LogError(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "success",
	})
}

func setDeviceOnlineStatusHandler(c *gin.Context) {
	type Payload struct {
		ProductID  string `json:"productId" binding:"required"`
		DeviceName string `json:"deviceName" binding:"required"`
		Status     int    `json:"status" binding:"required,oneof=-1 1"`
	}
	var payload Payload

	if err := c.ShouldBindJSON(&payload); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	utils.LogInfo("Received HTTP event: %s, content: %+v", "SetDeviceOnlineStatus", payload)

	if err := setDeviceOnlineStatusCallback(payload.ProductID, payload.DeviceName, payload.Status); err != nil {
		err = fmt.Errorf("failed to call user's setDeviceOnlineStatusCallback: %w", err)
		utils.LogError(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "success",
	})
}

func getEdgexOnlineStatusHandler(c *gin.Context) {
	// utils.LogInfo("Received HTTP event: %s", "GetEdgexOnlineStatus")

	isOnline, onlineFailureError, err := getEdgexOnlineStatusCallback()
	if err != nil {
		err = fmt.Errorf("failed to call user's getEdgexOnlineStatusCallback: %w", err)
		utils.LogError(err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{
			"code": 1,
			"msg":  err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"code": 0,
		"data": gin.H{
			"isOnline":           isOnline,
			"onlineFailureError": onlineFailureError,
		},
	})
}
