package hal

import (
	"context"
	"fmt"
	"time"

	"gitee.com/hckdigi/silinode-common/agent-sdk/private/clients"
)

const (
	// localPropHardwareInfo 是本地硬件参数。
	localPropHardwareInfo = "hardwareInfo"
	// localPropSoftwareInfo 是本地软件参数。
	localPropSoftwareInfo = "softwareInfo"
)

/*
	读取Host主机的属性
*/
func ReadLocalProps(props []string) (map[string]interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resMap := make(map[string]interface{})

	for _, prop := range props {
		switch prop {
		case localPropHardwareInfo:
			res, err := clients.EdgexdClient.SystemMetrics(ctx)
			if err != nil {
				return nil, fmt.Errorf("failed to call EdgexdClient.SystemMetrics: %w", err)
			}
			resMap[prop] = res
		case localPropSoftwareInfo:
			res, err := clients.EdgexdClient.SoftwareInfos(ctx)
			if err != nil {
				return nil, fmt.Errorf("failed to call EdgexdClient.SoftwareInfos: %w", err)
			}
			resMap[prop] = res
		default:
			// return nil, fmt.Errorf("HAL does not support reading the prop: %s", prop)
			res, err := clients.EdgexdClient.HostProperties(ctx, []string{prop})
			if err != nil {
				return nil, fmt.Errorf("failed to get Host Property[%s]: %w", prop, err)
			}
			if len(res) > 0 {
				resMap[prop] = res[0]
			}
		}
	}

	return resMap, nil
}

// ReadLocalProfile: 获取 Host主机的 设备模板定义。（与EdgeX内部的设备模板定义不同）
func ReadLocalProfile() (map[string]interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	res, err := clients.EdgexdClient.HostProfiles(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to get Host profile: %w", err)
	}
	return res, nil
}
