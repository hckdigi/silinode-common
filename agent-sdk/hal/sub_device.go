package hal

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/errorcode"
	"strings"
	"time"

	"gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/clients"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/tsl"
	"gitee.com/hckdigi/silinode-common/siliworks-link"
)

// ReadSubDeviceProps 读取子设备属性。
func ReadSubDeviceProps(deviceName string, props []string) (map[string]interface{}, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	dvc, err := clients.MetaDeviceClient.DeviceForName(ctx, deviceName)
	if err != nil {
		return nil, fmt.Errorf("failed to call MetaDeviceClient.DeviceForName: %w", err)
	}

	propsMap := make(map[string]interface{})

	for _, prop := range props {

		/**
		 * 处理单个属性 prop
		 */

		// 1. 找到 prop 对应的 cmd
		var cmd *models.Command
		cmdName := tsl.Capitalize(prop)
		for i := range dvc.Profile.CoreCommands {
			if cmdName == dvc.Profile.CoreCommands[i].Name {
				cmd = &dvc.Profile.CoreCommands[i]
				break
			}
		}

		if cmd == nil {
			return nil, fmt.Errorf("cannot found the command: %s", prop)
		}

		// 2. 调用 cmd 读取 prop
		resp, err := clients.CommandClient.GetDeviceCommandByNames(ctx, dvc.Name, cmd.Name)
		if err != nil {
			return nil, fmt.Errorf("CommandClient.Get failed: %w", err)
		}

		// 3. 解析 resp
		//
		// 注意：
		// 若 prop 为基本类型，event 包含一个 reading；
		// 若 prop 为对象属性，event 包含多个 reading，每个 reading 都为对象中的一个属性。
		var event models.Event
		_ = json.Unmarshal([]byte(resp), &event)

		// 此方法返回的 map 中，在此处只有一个 key，也就是 prop。
		// 若 prop 为对象属性，则 value 底层类型为 map[string]interface{}，interface{} 底层为对象下属性的真实类型，
		// 若 prop 为基本属性，则 value 底层类型为 prop 的真实类型。
		mm, err := tsl.TransformEvents(event)
		if err != nil {
			return nil, fmt.Errorf("TransformEvents failed: %w", err)
		}

		propsMap[prop] = mm[prop]
	}

	return propsMap, nil
}

// WriteSubDeviceProps 修改设备属性。
func WriteSubDeviceProps(deviceName string, props map[string]interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	dvc, err := clients.MetaDeviceClient.DeviceForName(ctx, deviceName)
	if err != nil {
		return fmt.Errorf("failed to call MetaDeviceClient.DeviceForName: %w", err)
	}

	cmdNameMap := make(map[string]models.Command)
	for _, cmd := range dvc.Profile.CoreCommands {
		cmdNameMap[cmd.Name] = cmd
	}

	for prop, propValue := range props {
		// 1. 获取属性对应的指令
		propCmd, ok := cmdNameMap[tsl.Capitalize(prop)]
		if !ok {
			return fmt.Errorf("cannot found the command: %s", "pof_"+prop)
		}

		// 2. 将属性值转成调用指令时所需要的格式
		inputMap, err := tsl.ProcessSinglePropertyInput(dvc.Id, prop, propValue)
		if err != nil {
			return fmt.Errorf("ProcessSinglePropertyInput failed: %w", err)
		}
		buf := new(bytes.Buffer)
		_ = json.NewEncoder(buf).Encode(inputMap)

		// 3. 调用指令
		_, err = clients.CommandClient.PutDeviceCommandByNames(ctx, dvc.Name, propCmd.Name, buf.String())
		if err != nil {
			return fmt.Errorf("CommandClient.Put failed: %w", err)
		}
	}

	return nil
}

// InvokeSubDeviceFunc 调用子设备功能。
func InvokeSubDeviceFunc(deviceName, funcName string, inputs []link.FuncInput) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	dvc, err := clients.MetaDeviceClient.DeviceForName(ctx, deviceName)
	if err != nil {
		return fmt.Errorf("failed to call MetaDeviceClient.DeviceForName: %w", err)
	}

	var commandName string
	for _, cmd := range dvc.Profile.CoreCommands {
		if funcName == cmd.Name {
			commandName = cmd.Name
			break
		}
	}

	if commandName == "" {
		return fmt.Errorf("cannot found the command: %s", funcName)
	}

	_inputs, err := tsl.ProcessPropertiesInput(dvc.Id, inputs)
	if err != nil {
		return fmt.Errorf("ProcessPropertiesInput failed: %w", err)
	}

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(_inputs)

	_, err = clients.CommandClient.PutDeviceCommandByNames(ctx, dvc.Name, commandName, buf.String())
	if err != nil {
		return fmt.Errorf("CommandClient.Put failed: %w", err)
	}

	return nil
}

// DeviceModel 为设备型号唯一标识。
type DeviceModel struct {
	Manufacturer    string `json:"manufacturer"`
	ProductCategory string `json:"productCategory"`
	ProductModelNum string `json:"productModelNum"`
	Version         string `json:"version"`
}

// AddSubDevice 添加设备实例。
// deviceModel 为设备型号唯一标识，据此查询到设备所属 deviceProfile 和 deviceService。
func AddSubDevice(deviceModel DeviceModel, deviceName, protocolName string, protocolProps map[string]string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	// 设备通信协议
	protocols := map[string]models.ProtocolProperties{
		tsl.ConvertProtoName(protocolName): protocolProps,
	}

	// 查询 deviceModel
	dm, err := clients.MetaDeviceModelClient.DeviceModelForManufacturerAndTypeAndModel(ctx, deviceModel.Manufacturer, deviceModel.ProductCategory, deviceModel.ProductModelNum)
	if err != nil {
		if strings.Contains(err.Error(), errorcode.DeviceModelNotFound) {
			id, err := clients.MetaDeviceModelClient.AddByCloud(ctx, deviceModel.Manufacturer, deviceModel.ProductCategory, deviceModel.ProductModelNum, deviceModel.Version)
			if err != nil {
				return fmt.Errorf("failed to add device model(manufacturer: %s, category: %s, number: %s): %w",
					deviceModel.Manufacturer, deviceModel.ProductCategory, deviceModel.ProductModelNum, err)
			}
			dm, err = clients.MetaDeviceModelClient.DeviceModel(ctx, id)
			if err != nil {
				return fmt.Errorf("failed to query device model(id: %s, manufacturer: %s, category: %s, number: %s): %w",
					id, deviceModel.Manufacturer, deviceModel.ProductCategory, deviceModel.ProductModelNum, err)
			}
		} else {
			return fmt.Errorf("failed to query device model(manufacturer: %s, category: %s, number: %s): %w",
				deviceModel.Manufacturer, deviceModel.ProductCategory, deviceModel.ProductModelNum, err)
		}
	}

	// 新注册的设备实例
	newDevice := models.Device{
		Id:             "",
		Name:           deviceName,
		AdminState:     models.Unlocked,
		OperatingState: models.Enabled,
		Protocols:      protocols,
		Model:          dm,
	}

	_, err = clients.MetaDeviceClient.AddByCloud(ctx, &newDevice)
	if err != nil {
		return fmt.Errorf("failed to call MetaDeviceClient.Add: %w", err)
	}

	return nil
}

// DeleteSubDevice 删除设备实例。
func DeleteSubDevice(deviceName string) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	// modify by leo 20230615 
	//if err := clients.MetaDeviceClient.DeleteByNameAndCloud(ctx, deviceName); err != nil {
	deleteAssociate := true
	if err := clients.MetaDeviceClient.DeleteByNameAndCloud(ctx, deviceName,deleteAssociate); err != nil {
		return fmt.Errorf("failed to call MetaDeviceClient.DeleteByName: %w", err)
	}

	return nil
}
