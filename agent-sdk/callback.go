package edgexagentsdk

import (
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	agentmodels "gitee.com/hckdigi/silinode-common/mod/core-contracts/models/agent-models"
)

var (
	// deviceEventCallback 子设备事件动作回调（用于消息总线的事件）。
	deviceEventCallback func(models.Event) error

	// deviceOfflineEventAgentType 用来标识从消息总线订阅哪个 agent 的离线事件 topic。
	deviceOfflineEventAgentType agentmodels.AgentType
	// deviceOfflineEventCallback 子设备离线事件动作回调（用于消息总线的事件）。
	deviceOfflineEventCallback func(event map[string]interface{}) error

	// deviceEventReportCallback 子设备事件上报动作回调（用于规则引擎调用）。
	deviceEventReportCallback func(deviceName, eventID string, eventProps map[string]interface{}) error
	// gatewayEventReportCallback 网关自身事件上报动作回调。
	gatewayEventReportCallback func(eventID string, eventProps map[string]interface{}) error
	// bindDeviceCallback 绑定子设备动作回调，目前仅支持设备密钥认证方式。
	bindDeviceCallback func(productID, deviceName, devicePSK string) error
	// unbindDeviceCallback 解绑子设备动作回调。
	unbindDeviceCallback func(productID, deviceName string) error
	// setDeviceOnlineStatusCallback 设置子设备在线状态动作回调。
	setDeviceOnlineStatusCallback func(productID, deviceName string, status int) error
	// getEdgexOnlineStatusCallback 获取 EdgeX 网关自身的远端连接状态。
	getEdgexOnlineStatusCallback func() (isOnline bool, onlineFailureError string, err error)
)

func isAllCallbackRegistered() bool {
	if deviceEventCallback == nil || deviceOfflineEventAgentType == "" || deviceOfflineEventCallback == nil ||
		deviceEventReportCallback == nil || gatewayEventReportCallback == nil ||
		bindDeviceCallback == nil || unbindDeviceCallback == nil ||
		setDeviceOnlineStatusCallback == nil || getEdgexOnlineStatusCallback == nil {
		return false
	}

	return true
}

// RegisterCallbackOfDeviceEvent 注册子设备事件动作回调（用于消息总线事件的处理）。
func RegisterCallbackOfDeviceEvent(cb func(models.Event) error) {
	if deviceEventCallback == nil {
		deviceEventCallback = cb
	}
}

// RegisterCallbackOfDeviceOfflineEvent 注册子设备离线事件动作的回调（用于消息总线离线事件的处理），
// agentType 用来标识从消息总线订阅哪个 agent 的离线事件 topic，cb 为订阅到离线事件的处理逻辑。
func RegisterCallbackOfDeviceOfflineEvent(agentType agentmodels.AgentType, cb func(event map[string]interface{}) error) {
	if deviceOfflineEventAgentType == "" || deviceOfflineEventCallback == nil {
		deviceOfflineEventAgentType = agentType
		deviceOfflineEventCallback = cb
	}
}

// RegisterCallbackOfDeviceEventReport 注册子设备事件上报动作回调（用于规则引擎或其他灵活方式的事件上报）。
func RegisterCallbackOfDeviceEventReport(cb func(deviceName, eventID string, eventProps map[string]interface{}) error) {
	if deviceEventReportCallback == nil {
		deviceEventReportCallback = cb
	}
}

// RegisterCallbackOfGatewayEventReport 注册网关事件上报动作的回调。
func RegisterCallbackOfGatewayEventReport(cb func(eventID string, eventProps map[string]interface{}) error) {
	if gatewayEventReportCallback == nil {
		gatewayEventReportCallback = cb
	}
}

// RegisterCallbackOfBindDevice 注册绑定子设备动作回调。
func RegisterCallbackOfBindDevice(cb func(productID, deviceName, devicePSK string) error) {
	if bindDeviceCallback == nil {
		bindDeviceCallback = cb
	}
}

// RegisterCallbackOfUnbindDevice 注册解绑子设备动作回调。
func RegisterCallbackOfUnbindDevice(cb func(productID, deviceName string) error) {
	if unbindDeviceCallback == nil {
		unbindDeviceCallback = cb
	}
}

// RegisterCallbackOfSetDeviceOnlineStatus 注册设置子设备在线状态动作回调。
func RegisterCallbackOfSetDeviceOnlineStatus(cb func(productID, deviceName string, status int) error) {
	if setDeviceOnlineStatusCallback == nil {
		setDeviceOnlineStatusCallback = cb
	}
}

// RegisterCallbackOfGetEdgexOnlineStatus 注册获取 EdgeX 网关自身云端连接状态动作回调。
func RegisterCallbackOfGetEdgexOnlineStatus(cb func() (isOnline bool, onlineFailureError string, err error)) {
	if getEdgexOnlineStatusCallback == nil {
		getEdgexOnlineStatusCallback = cb
	}
}
