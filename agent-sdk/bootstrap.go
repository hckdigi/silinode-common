package edgexagentsdk

import (
	"errors"
	"fmt"
)

// Boot 启动 sdk，内部将执行消息总线订阅和启动微服务接口。
func Boot() error {
	// 检测回调是否全部注册
	if !isAllCallbackRegistered() {
		return errors.New("callbacks is not fully registered")
	}

	// 启动 zeromq 订阅
	if err := bootMqSubscriber(); err != nil {
		return fmt.Errorf("failed to bootMqSubscriber: %w", err)
	}

	// 启动 http 服务
	bootHttpService()

	return nil
}
