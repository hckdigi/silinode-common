# EdgeX Agent SDK

## 架构

![架构图](./architecture.png)

## 使用

### 上行处理

```go
import gitee.com/hckdigi/silinode-common/agent-sdk

// 注册事件处理逻辑
edgexagentsdk.RegisterCallbackOfDeviceEvent(...)
edgexagentsdk.RegisterCallbackOfBindDevice(...)
edgexagentsdk.RegisterCallbackOfUnbindDevice(...)
edgexagentsdk.RegisterCallbackOfSetDeviceOnlineStatus(...)

// 启动，内部将执行消息总线订阅和启动微服务接口
if err := edgexagentsdk.Boot(); err != nil {
    // handle error
}
```

### 下行处理

```go
import gitee.com/hckdigi/silinode-common/agent-sdk/hal

props, err := hal.ReadSubDeviceProps(...)   // 读设备属性
hal.AddSubDevice(...)                       // 添加设备实例
// ...
```

## API 列表

```sh
# 上行 API
$ go doc -all .

# 下行 API
$ go doc -all ./hal
```
