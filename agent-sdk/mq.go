package edgexagentsdk

import (
	"encoding/json"
	"fmt"

	"gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	agentmodels "gitee.com/hckdigi/silinode-common/mod/core-contracts/models/agent-models"
	"gitee.com/hckdigi/silinode-common/mod/messaging/messaging"
	"gitee.com/hckdigi/silinode-common/mod/messaging/pkg/types"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/utils"
)

func bootMqSubscriber() error {
	cli, err := newMqClient()
	if err != nil {
		return fmt.Errorf("failed to newMqClient: %w", err)
	}

	if err = cli.Connect(); err != nil {
		return fmt.Errorf("failed to connnect to mq: %w", err)
	}

	eventsTopicChannel := make(chan types.MessageEnvelope)
	offlineEventsTopicChannel := make(chan types.MessageEnvelope)
	errCh := make(chan error)

	if err := cli.Subscribe([]types.TopicChannel{
		{
			Topic:    "events",
			Messages: eventsTopicChannel,
		},
		{
			Topic:    generateOfflineEventsTopic(deviceOfflineEventAgentType),
			Messages: offlineEventsTopicChannel,
		},
	}, errCh); err != nil {
		return fmt.Errorf("failed to subscribe mq topic: %w", err)
	}

	go func() {
		for {
			err := <-errCh
			utils.LogError("ZeroMQ subscriber encountered an error: %v", err)
		}
	}()

	go func() {
		for {
			msg := <-eventsTopicChannel

			var evt internal.Event
			if err := json.Unmarshal(msg.Payload, &evt); err != nil {
				utils.LogError("Failed to unmarshal ZeroMQ message: %v", err)
				continue
			}

			utils.LogInfo("Received ZeroMQ event: %+v", evt)

			go func(e models.Event) {
				if err := deviceEventCallback(e); err != nil {
					utils.LogError("Failed to call user's deviceEventCallback: %v", err)
				}
			}(evt.Event)
		}
	}()

	go func() {
		for {
			msg := <-offlineEventsTopicChannel

			var evt map[string]interface{}
			if err := json.Unmarshal(msg.Payload, &evt); err != nil {
				utils.LogError("Failed to unmarshal ZeroMQ message: %v", err)
				continue
			}

			utils.LogInfo("Received ZeroMQ offline event: %+v", evt)

			go func(e map[string]interface{}) {
				if err := deviceOfflineEventCallback(e); err != nil {
					utils.LogError("Failed to call user's deviceOfflineEventCallback: %v", err)
				}
			}(evt)
		}
	}()

	return nil
}

func newMqClient() (messaging.MessageClient, error) {
	return messaging.NewMessageClient(types.MessageBusConfig{
		SubscribeHost: types.HostInfo{
			Host:     "edgex-core-data",
			Port:     5563,
			Protocol: "tcp",
		},
		Type:     "zero",
		Optional: map[string]string{
			// "Username":       "",
			// "Password":       "",
			// "ClientId":       "edgex-agent-sdk-" + utils.RandLetterNum(6),
			// "Qos":            "0",
			// "KeepAlive":      "10",
			// "Retained":       "false",
			// "AutoReconnect":  "true",
			// "ConnectTimeout": "5",
			// "SkipCertVerify": "false",
		},
	})
}

func generateOfflineEventsTopic(at agentmodels.AgentType) string {
	return fmt.Sprintf("offline_data_%s", string(at))
}
