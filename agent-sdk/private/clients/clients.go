package clients

import (
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/agent"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/command"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/edgexd"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/metadata"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/urlclient/local"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private"
)

var (
	MetaDeviceClient        metadata.DeviceClient
	MetaDeviceProfileClient metadata.DeviceProfileClient
	MetaDeviceServiceClient metadata.DeviceServiceClient
	MetaDeviceModelClient   metadata.DeviceModelClient
	CommandClient           command.CommandClient
	SMAClient               agent.AgentClient
	MetaHostClient          metadata.DeviceHostClient
	EdgexdClient            edgexd.PropertyClient
)

func init() {
	MetaDeviceClient = metadata.NewDeviceClient(local.New(internal.UrlMetaDevice))
	MetaDeviceProfileClient = metadata.NewDeviceProfileClient(local.New(internal.UrlMetaDeviceProfile))
	MetaDeviceServiceClient = metadata.NewDeviceServiceClient(local.New(internal.UrlMetaDeviceService))
	MetaDeviceModelClient = metadata.NewDeviceModelClient(local.New(internal.UrlMetaDeviceModel))
	CommandClient = command.NewCommandClient(local.New(internal.UrlCommand))
	SMAClient = agent.NewAgentClient(local.New(internal.UrlSma))
	MetaHostClient = metadata.NewDeviceHostClient(local.New(internal.UrlMetaHost))
	EdgexdClient = edgexd.NewPropertyClient(local.New(internal.UrlEdgexd))
}
