package dpToTslModel

import (
	"fmt"
	contract "gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	"gopkg.in/yaml.v2"
)

func ReadDeviceProfile(yamlFile string) (contract.DeviceProfile, error) {
	var profile contract.DeviceProfile
	err := yaml.Unmarshal([]byte(yamlFile), &profile)
	if err != nil {
		return profile, fmt.Errorf("invalid Device Profile:  %v", err)
	}

	// TODO: this section will be removed after the deprecated fields are truly removed
	handleDeprecatedFields(&profile)
	return profile, nil
}

func handleDeprecatedFields(profile *contract.DeviceProfile) {
	for _, pr := range profile.DeviceCommands {
		for i, ro := range pr.Get {
			pr.Get[i] = handleRODeprecatedFields(ro)
		}
		for i, ro := range pr.Set {
			pr.Set[i] = handleRODeprecatedFields(ro)
		}
	}
}

func handleRODeprecatedFields(ro contract.ResourceOperation) contract.ResourceOperation {
	if ro.DeviceResource != "" {
		ro.Object = ro.DeviceResource
	} else if ro.Object != "" {
		ro.DeviceResource = ro.Object
	}
	if ro.DeviceCommand != "" {
		ro.Resource = ro.DeviceCommand
	} else if ro.Resource != "" {
		ro.DeviceCommand = ro.Resource
	}
	return ro
}
