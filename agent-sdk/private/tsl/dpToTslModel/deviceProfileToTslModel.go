package dpToTslModel

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	contract "gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/tsl/models"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/tsl/tslModelToDp"
)

func DeviceProfileTpTslModel(yamlFile string) (models.TSLModel, error) {
	deviceProfile, err := ReadDeviceProfile(yamlFile)
	if err != nil {
		return models.TSLModel{}, err
	}
	return TransformDP2TslModel(deviceProfile)
}

func TransformDP2TslModel(profile contract.DeviceProfile) (models.TSLModel, error) {
	tslModel := models.TSLModel{}
	tslModel.Id = profile.Name
	properties, err := transformProperties(profile.DeviceResources)
	if err != nil {
		return tslModel, err
	}
	tslModel.Properties = properties
	resourceMap := make(map[string]contract.DeviceResource)
	for _, resource := range profile.DeviceResources {
		resourceMap[resource.Name] = resource
	}
	functions, err := transformFunctions(profile.CoreCommands, resourceMap)
	if err != nil {
		return tslModel, err
	}
	tslModel.Functions = functions
	return tslModel, nil
}

func transformProperties(deviceResources []contract.DeviceResource) ([]models.Property, error) {
	tagMap := make(map[string][]contract.DeviceResource)
	result := make([]models.Property, 0)
	for _, deviceResource := range deviceResources {
		if deviceResource.Tag == "" {
			property, err := baseTypePropertyConvert(deviceResource)
			if err != nil {
				return nil, err
			}
			result = append(result, *property)
		} else {
			if resources, ok := tagMap[deviceResource.Tag]; ok {
				resources = append(resources, deviceResource)
				tagMap[deviceResource.Tag] = resources
			} else {
				resources = make([]contract.DeviceResource, 0)
				resources = append(resources, deviceResource)
				tagMap[deviceResource.Tag] = resources
			}
		}
	}
	for tag, resources := range tagMap {
		resourceIdMap := make(map[string]contract.DeviceResource)
		for _, resource := range resources {
			resourceIdMap[resource.Name] = resource
		}
		objectProperty := tslModelToDp.ObjectProperty{}
		err := json.Unmarshal([]byte(tag), &objectProperty)
		if err != nil {
			return nil, fmt.Errorf("deserialize tag failed, #%v", err)
		}
		object, _ := structPropertyConvert(&objectProperty, resourceIdMap)
		result = append(result, *object)
	}
	return result, nil
}

func structPropertyConvert(property *tslModelToDp.ObjectProperty, resourceIdMap map[string]contract.DeviceResource) (*models.Property, error) {
	if property.Type != "object" && property.Type != "geoPoint" {
		if resource, ok := resourceIdMap[property.Id]; ok {
			return baseTypePropertyConvert(resource)
		} else {
			return nil, errors.New("unknown resource, id:" + resource.Name)
		}
	} else {
		object := models.Property{}
		object.Id = property.Id
		object.Name = property.Name
		object.ValueType = make(map[string]interface{})
		childProperties := make([]*models.Property, 0)
		for _, child := range property.Children {
			childProperty, err := structPropertyConvert(child, resourceIdMap)
			if err != nil {
				return nil, err
			}
			childProperties = append(childProperties, childProperty)
		}
		object.ValueType["type"] = property.Type
		object.ValueType["properties"] = childProperties
		return &object, nil
	}

}

func baseTypePropertyConvert(deviceResource contract.DeviceResource) (*models.Property, error) {
	property := models.Property{}
	propertyCommonConvert(deviceResource, &property)
	tmp := strings.Split(deviceResource.Description, "##")
	if len(tmp) > 1 {
		property.Name = strings.Join(tmp[:len(tmp)-1], " ")
		if strings.Contains(tmp[len(tmp)-1], "$type:") {
			str := strings.Replace(tmp[len(tmp)-1], "$type:", "", 1)
			tmp = strings.Split(str, "$")
			property.ValueType["type"] = tmp[0]
			if len(tmp) > 1 && tmp[1] != "" {

				switch tmp[0] {
				case "enum":
					valueMap := make([]map[string]interface{}, 0)
					err := json.Unmarshal([]byte(tmp[1]), &valueMap)
					if err != nil {
						return nil, err
					}
					property.ValueType["elements"] = valueMap
				case "date":
					valueMap := make(map[string]interface{}, 0)
					err := json.Unmarshal([]byte(tmp[1]), &valueMap)
					if err != nil {
						return nil, err
					}
					for key, value := range valueMap {
						property.ValueType[key] = value
					}
				case "array":
					valueMap := make(map[string]interface{}, 0)
					err := json.Unmarshal([]byte(tmp[1]), &valueMap)
					if err != nil {
						return nil, err
					}
					property.ValueType["elementType"] = valueMap
				}
			}
		} else {
			property.Name += tmp[len(tmp)-1]
		}
	} else {
		property.Name = deviceResource.Description
	}
	return &property, nil
}

func propertyCommonConvert(deviceResource contract.DeviceResource, property *models.Property) {
	property.Id = deviceResource.Name
	if len(deviceResource.Attributes) > 0 {
		property.Expands = make(map[string]interface{})
		for key, value := range deviceResource.Attributes {
			property.Expands[key] = value
		}
	}
	propertyValue := deviceResource.Properties.Value
	unit := deviceResource.Properties.Units
	property.ValueType = make(map[string]interface{})
	valueTypeExpand := make(map[string]interface{})
	property.ValueType["type"] = dpDataTypeToTslModelDataType(propertyValue.Type)
	if propertyValue.Size != "" {
		valueTypeExpand["size"] = propertyValue.Size
	}
	if propertyValue.Base != "" {
		valueTypeExpand["base"] = propertyValue.Base
	}
	if propertyValue.DefaultValue != "" {
		valueTypeExpand["defaultValue"] = propertyValue.DefaultValue
	}
	if propertyValue.Mask != "" {
		valueTypeExpand["mask"] = propertyValue.Mask
	}
	if propertyValue.Maximum != "" {
		property.ValueType["maxValue"] = propertyValue.Maximum
	}
	if propertyValue.Minimum != "" {
		property.ValueType["minValue"] = propertyValue.Minimum
	}
	if propertyValue.Offset != "" {
		valueTypeExpand["offset"] = propertyValue.Offset
	}
	if propertyValue.ReadWrite != "" {
		if propertyValue.ReadWrite == "R" {
			valueTypeExpand["readOnly"] = true
		} else {
			valueTypeExpand["readOnly"] = false
		}
	}
	if propertyValue.Scale != "" {
		valueTypeExpand["scale"] = propertyValue.Scale
	}
	if propertyValue.Shift != "" {
		valueTypeExpand["shift"] = propertyValue.Shift
	}
	if propertyValue.FloatEncoding != "" {
		valueTypeExpand["floatEncoding"] = propertyValue.FloatEncoding
	}
	if propertyValue.Assertion != "" {
		valueTypeExpand["assertion"] = propertyValue.Assertion
	}
	if unit.DefaultValue != "" {
		property.ValueType["unit"] = unit.DefaultValue
	}
	if len(valueTypeExpand) > 0 {
		property.ValueType["expands"] = valueTypeExpand
	}
}

func transformFunctions(commands []contract.Command, resourceMap map[string]contract.DeviceResource) ([]models.Function, error) {
	result := make([]models.Function, 0)
	for _, command := range commands {
		if strings.HasPrefix(command.Name, tslModelToDp.FUNCTION_FUNC_PREFFIX) {
			function := models.Function{}
			function.Id = strings.Replace(command.Name, tslModelToDp.FUNCTION_FUNC_PREFFIX, "", 1)
			if len(command.Put.Responses) > 0 {
				function.Name = command.Put.Responses[0].Description
				if len(command.Put.ParameterNames) > 0 {
					inputResources := make([]contract.DeviceResource, 0)
					for _, parameterName := range command.Put.ParameterNames {
						if resource, ok := resourceMap[parameterName]; !ok {
							return nil, errors.New("parameterName cannot be found")
						} else {
							inputResources = append(inputResources, resource)
						}
					}
					inputs, err := transformProperties(inputResources)
					if err != nil {
						return nil, err
					}
					function.Inputs = inputs
				}
				if len(command.Put.Responses[0].ExpectedValues) > 0 {
					outputResources := make([]contract.DeviceResource, 0)
					for _, expectedValue := range command.Put.Responses[0].ExpectedValues {
						if resource, ok := resourceMap[expectedValue]; !ok {
							return nil, errors.New("expectedValue cannot be found")
						} else {
							outputResources = append(outputResources, resource)
						}
					}
					outputs, err := transformProperties(outputResources)
					if err != nil {
						return nil, err
					}
					function.Ouputs = outputs
				}
			}
			result = append(result, function)
		}
	}
	return result, nil
}

func dpDataTypeToTslModelDataType(dpType string) string {
	switch strings.ToLower(dpType) {
	case "uint8", "uint16", "uint32", "int8", "int16", "int32", "uint8array", "uint16array",
		"uint32array", "int8array", "int16array", "int32array":
		return "int"
	case "uint64", "int64", "uint64array", "int64array":
		return "long"
	case "float32", "float32array":
		return "float"
	case "float64", "float64array":
		return "double"
	case "bool", "boolarray":
		return "boolean"
	case "string":
		return "string"
	default:
		return "string"
	}
}
