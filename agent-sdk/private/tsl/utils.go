package tsl

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	contract "gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/clients"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/tsl/dpToTslModel"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/tsl/models"
	"gitee.com/hckdigi/silinode-common/agent-sdk/private/tsl/tslModelToDp"
	"gitee.com/hckdigi/silinode-common/siliworks-link"
)

type PropertyValue struct {
	Id   string `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

// 保存物模型
func SaveTslModel(productId string, tslModel string) error {
	deviceProfileTxt, err := tslModelToDp.ThingsModelToDp(tslModel)
	if err != nil {
		return fmt.Errorf("tslModel convert to deviceProfile failed: %w", err)
	}
	deviceProfile, err := dpToTslModel.ReadDeviceProfile(deviceProfileTxt)
	if err != nil {
		return fmt.Errorf("cannot transform deviceProfile: %w", err)
	}
	deviceProfile.Id = productId
	_, err = clients.MetaDeviceProfileClient.Add(context.Background(), &deviceProfile)
	if err != nil {
		if strings.Contains(err.Error(), "Duplicate profile name") {
			err = clients.MetaDeviceProfileClient.Update(context.Background(), deviceProfile)
		}
	}
	if err != nil {
		return fmt.Errorf("add deviceProfile failed: %w", err)
	}
	return nil
}

// 更新物模型
func UpdateTslModel(tslModel string) error {
	deviceProfileTxt, err := tslModelToDp.ThingsModelToDp(tslModel)
	if err != nil {
		return fmt.Errorf("tslModel convert to deviceProfile failed: %w", err)
	}
	deviceProfile, err := dpToTslModel.ReadDeviceProfile(deviceProfileTxt)
	if err != nil {
		return fmt.Errorf("cannot transform deviceProfile: %w", err)
	}
	err = clients.MetaDeviceProfileClient.Update(context.Background(), deviceProfile)
	if err != nil {
		return fmt.Errorf("update deviceProfile failed: %w", err)
	}
	return nil
}

// 获取物模型
func GetTslModel(productId string) (models.TSLModel, error) {
	deviceProfile, err := clients.MetaDeviceProfileClient.DeviceProfile(context.Background(), productId)
	if err != nil {
		return models.TSLModel{}, fmt.Errorf("cannot find deviceProfile: %w", err)
	}
	tslModel, err := dpToTslModel.TransformDP2TslModel(deviceProfile)
	if err != nil {
		return models.TSLModel{}, fmt.Errorf("transform deviceProfile to tslModel failed: %w", err)
	}
	return tslModel, nil
}

// 删除物模型
func DeleteTslModel(productId string) error {
	return clients.MetaDeviceProfileClient.Delete(context.Background(), productId)
}

func ConvertProtoName(protocol string) string {
	switch strings.ToLower(protocol) {
	case "mqtt":
		return "mqtt"
	case "modbus-tcp":
		return "modbus-tcp"
	case "modbus-rtu":
		return "modbus-rtu"
	case "http":
		return "HTTP"
	case "onvif":
		return "onvif"
	case "gb28181":
		return "gb28181"
	case "rtsp":
		return "rtsp"
	default:
		return "other"

	}
}

// events 转 map
func TransformEvents(event contract.Event) (map[string]interface{}, error) {
	result := make(map[string]interface{})

	for _, reading := range event.Readings {
		result[reading.Name] = reading.Value
	}
	return result, nil
}

func structPropertyValueConvert(
	property *tslModelToDp.ObjectProperty,
	resourceIdMap map[string]contract.DeviceResource,
	readingMap map[string]contract.Reading) (map[string]interface{}, error) {
	result := make(map[string]interface{})
	if property.Type != "object" && property.Type != "geoPoint" {
		if reading, ok := readingMap[property.Id]; ok {
			value, err := propertyStringValueConvert(resourceIdMap[property.Id], reading)
			if err != nil {
				return nil, err
			}
			result[property.Id] = value
			return result, nil
		} else {
			return nil, errors.New("unknown resource, id:" + property.Id)
		}
	} else {
		tmp := make(map[string]interface{})
		for _, child := range property.Children {
			childValueMap, err := structPropertyValueConvert(child, resourceIdMap, readingMap)
			if err != nil {
				return nil, err
			}
			tmp[child.Id] = childValueMap[child.Id]
		}
		result[property.Id] = tmp
		return result, nil
	}
}

// 物模型设备属性设置调用command参数处理
func ProcessSinglePropertyInput(deviceId string, propertyName string, propertyValue interface{}) (map[string]interface{}, error) {
	device, err := clients.MetaDeviceClient.Device(context.Background(), deviceId)
	if err != nil {
		return nil, err
	}
	resources := device.Profile.DeviceResources
	resourceNameMap := make(map[string]bool)
	for _, resource := range resources {
		resourceNameMap[resource.Name] = true
	}
	result, err := analyzeInput(resourceNameMap, propertyName, propertyValue)
	if err != nil {
		return nil, err
	}
	if len(result) == 0 {
		return nil, fmt.Errorf("unkown property %s", propertyName)
	}
	return result, nil
}

func analyzeInput(propertyNames map[string]bool, propertyName string, propertyValue interface{}) (map[string]interface{}, error) {
	result := make(map[string]interface{})
	if _, ok := propertyNames[propertyName]; ok {
		result[propertyName] = propertyValue
	} else {
		tmp := make(map[string]interface{})
		switch propertyValue.(type) {
		case string:
			str := propertyValue.(string)
			err := json.Unmarshal([]byte(str), &tmp)
			if err != nil {
				return nil, fmt.Errorf("%s value %s is not json, error %v", propertyName, propertyValue, err)
			}
			for key, value := range tmp {
				if _, ok := propertyNames[key]; ok {
					result[key] = value
				}
			}
		}
	}
	return result, nil
}

// 物模型function调用command参数处理
func ProcessPropertiesInput(deviceId string, inputs []link.FuncInput) (map[string]interface{}, error) {
	device, err := clients.MetaDeviceClient.Device(context.Background(), deviceId)
	if err != nil {
		return nil, err
	}
	resources := device.Profile.DeviceResources
	resourceNameMap := make(map[string]bool)
	for _, resource := range resources {
		resourceNameMap[resource.Name] = true
	}
	result := make(map[string]interface{})
	for _, input := range inputs {
		tmp, err := analyzeInput(resourceNameMap, input.Name, input.Value)
		if err != nil {
			return nil, err
		}
		if len(tmp) > 0 {
			for key, value := range tmp {
				result[key] = value
			}
		}
	}
	return result, nil
}

//func analyzeInput(inputs []link.FuncInput, propertyMap map[string]PropertyValue) (map[string]interface{}, error) {
//	result := make(map[string]interface{})
//	for _, input := range inputs {
//		if propertyValue, ok := propertyMap[input.Name]; ok {
//			if propertyValue.Type == "object" || propertyValue.Type == "geoPoint" {
//				valueMap, ok := input.Value.(map[string]interface{})
//				if !ok {
//					return nil, errors.New("object value is not a map")
//				}
//				childInputs := make([]link.FuncInput, 0)
//				for key, value := range valueMap {
//					funcInput := link.FuncInput{
//						Name:  key,
//						Value: value,
//					}
//					childInputs = append(childInputs, funcInput)
//				}
//				tmp, err := analyzeInput(childInputs, propertyMap)
//				if err != nil {
//					return nil, err
//				}
//				for childKey, childValue := range tmp {
//					result[childKey] = childValue
//				}
//			} else {
//				result[input.Name] = input.Value
//			}
//		}
//	}
//	return result, nil
//}

func generatePropertyMap(properties []models.Property) map[string]PropertyValue {
	propertyMap := make(map[string]PropertyValue)
	for _, property := range properties {
		propertyValue := PropertyValue{
			Id:   property.Id,
			Name: property.Name,
			Type: property.ValueType["type"].(string),
		}
		propertyMap[property.Id] = propertyValue
		if propertyValue.Type == "object" || propertyValue.Type == "geoPoint" {
			tmp := property.ValueType["properties"].([]*models.Property)
			childProperties := make([]models.Property, 0)
			for _, childProperty := range tmp {
				childProperties = append(childProperties, *childProperty)
			}
			childPropertyMap := generatePropertyMap(childProperties)
			for key, value := range childPropertyMap {
				propertyMap[key] = value
			}
		}
	}
	return propertyMap
}

func propertyStringValueConvert(deviceResource contract.DeviceResource, reading contract.Reading) (interface{}, error) {
	valueType := deviceResource.Properties.Value.Type
	encoding := deviceResource.Properties.Value.FloatEncoding
	switch strings.ToLower(valueType) {
	case "uint8", "uint16", "uint32", "int8", "int16", "int32":
		return strconv.Atoi(reading.Value)
	case "uint8array", "uint16array", "uint32array", "int8array", "int16array", "int32array":
		array := make([]int, 0)
		err := json.Unmarshal([]byte(reading.Value), &array)
		if err != nil {
			return nil, fmt.Errorf("deserialize reading %s to array failed: %w", reading.Value, err)
		}
		return array, nil
	case "uint64", "int64", "uint64array", "int64array":
		array := make([]int64, 0)
		err := json.Unmarshal([]byte(reading.Value), &array)
		if err != nil {
			return nil, fmt.Errorf("deserialize reading %s to array failed: %w", reading.Value, err)
		}
		return array, nil
	case "float32":
		if encoding == "" || encoding == "Base64" {
			sDec, err := base64.StdEncoding.DecodeString(reading.Value)
			if err != nil {
				return nil, fmt.Errorf("data Base64 decode failed: %w", err)
			}
			var res float32
			err = binary.Read(bytes.NewReader(sDec), binary.BigEndian, &res)
			if err != nil {
				return nil, fmt.Errorf("data Base64 decode failed: %w", err)
			}
			return res, nil
		} else {
			return strconv.ParseFloat(reading.Value, 32)
		}
	case "float32array":
		array := make([]float32, 0)
		err := json.Unmarshal([]byte(reading.Value), &array)
		if err != nil {
			return nil, fmt.Errorf("deserialize reading %s to array failed: %w", reading.Value, err)
		}
		return array, nil
	case "float64":
		if encoding == "" || encoding == "Base64" {
			sDec, err := base64.StdEncoding.DecodeString(reading.Value)
			if err != nil {
				return nil, fmt.Errorf("data Base64 decode failed: %w", err)
			}
			var res float64
			err = binary.Read(bytes.NewReader(sDec), binary.BigEndian, &res)
			if err != nil {
				return nil, fmt.Errorf("data Base64 decode failed: %w", err)
			}
			return res, nil
		} else {
			return strconv.ParseFloat(reading.Value, 64)
		}
	case "float64array":
		array := make([]float64, 0)
		err := json.Unmarshal([]byte(reading.Value), &array)
		if err != nil {
			return nil, fmt.Errorf("deserialize reading %s to array failed: %w", reading.Value, err)
		}
		return array, nil
	case "bool":
		return strconv.ParseBool(reading.Value)
	case "boolarray":
		array := make([]bool, 0)
		err := json.Unmarshal([]byte(reading.Value), &array)
		if err != nil {
			return nil, fmt.Errorf("deserialize reading %s to array failed: %w", reading.Value, err)
		}
		return array, nil
	case "string":
		return reading.Value, nil
	default:
		return reading.Value, nil
	}
}

//字符首字母大写转换
func Capitalize(str string) string {
	var upperStr string
	vv := []rune(str) // 后文有介绍
	for i := 0; i < len(vv); i++ {
		if i == 0 {
			if vv[i] >= 97 && vv[i] <= 122 { // 后文有介绍
				vv[i] -= 32 // string的码表相差32位
				upperStr += string(vv[i])
			} else {
				fmt.Println("Not begins with lowercase letter,")
				return str
			}
		} else {
			upperStr += string(vv[i])
		}
	}
	return upperStr
}
