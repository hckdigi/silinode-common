package models

type TSLModel struct {
	Id         string
	Properties []Property
	Functions  []Function
	Events     []Event
}

type Property struct {
	Id          string
	Code        string
	Name        string
	Description string
	Expands     map[string]interface{}
	DataType    string
	ValueType   map[string]interface{}
}

type Function struct {
	Id          string
	Code        string
	Name        string
	Description string
	Expands     map[string]interface{}
	Inputs      []Property
	Ouputs      []Property
}

type Event struct {
	Id          string
	Code        string
	Name        string
	Description string
	Expands     map[string]interface{}
	DataType    string
	ValueType   map[string]interface{}
}
