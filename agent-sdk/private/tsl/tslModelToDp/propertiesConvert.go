package tslModelToDp

import (
	"errors"

	"github.com/bitly/go-simplejson"
)

func PropertiesConvert(js *simplejson.Json) ([]*ModelProperties, error) {
	propertiesList := make([]*ModelProperties, 0)
	if _, ok := js.CheckGet("valueType"); !ok {
		return propertiesList, errors.New("property type cannot be empty")
	}
	if _, ok := js.Get("valueType").CheckGet("type"); !ok {
		return propertiesList, errors.New("property type cannot be empty")
	}
	valueType := js.Get("valueType").Get("type").MustString()
	var property *ModelProperties
	var err error
	switch valueType {
	case "string":
		property, err = StringConvert(js)
	case "int":
		property, err = IntConvert(js)
	case "long":
		property, err = LongConvert(js)
	case "float":
		property, err = FloatConvert(js)
	case "double":
		property, err = DoubleConvert(js)
	case "boolean":
		property, err = BooleanConvert(js)
	case "enum":
		property, err = EnumConvert(js)
	case "date":
		property, err = DateConvert(js)
	case "password":
		property, err = PasswordConvert(js)
	case "array":
		property, err = ArrayConvert(js)
	case "object":
		return ObjectConvert(js)
	case "geoPoint":
		return GeoPointConvert(js)
	default:
		return propertiesList, errors.New("unsupported property type")
	}
	if err != nil {
		return propertiesList, err
	}
	propertiesList = append(propertiesList, property)
	return propertiesList, nil
}
