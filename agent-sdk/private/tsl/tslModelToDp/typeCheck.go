package tslModelToDp

import (
	"fmt"
	"github.com/bitly/go-simplejson"
	"github.com/pkg/errors"
	"strconv"
)

func checkReadWriteValue(js *simplejson.Json) (string, error) {
	if js.MustString() == "" {
		value, err := js.Bool()
		if err != nil {
			return "", errors.Wrap(err, "readOnly type must be boolean")
		}
		if value {
			return "R", nil
		} else {
			return "RW", nil
		}
	} else {
		value, err := strconv.ParseBool(js.MustString())
		if err != nil {
			return "", errors.Wrap(err, "readOnly type must be boolean")
		}
		if value {
			return "R", nil
		} else {
			return "RW", nil
		}
	}
}

func checkDefaultValue(js *simplejson.Json, valueType string) (string, error) {
	switch valueType {
	case "int", "long", "float", "double":
		if js.MustString() == "" {
			value, err := jsNumberTypeConvert(js, valueType)
			if err != nil {
				return "", errors.Wrap(err, "defaultValue type must be same as valueType")
			}
			return value, nil
		} else {
			value, err := jsStringNumberConvert(js, valueType)
			if err != nil {
				return "", errors.Wrap(err, "defaultValue type must be same as valueType")
			}
			return value, nil
		}
	default:
		return js.MustString(), nil
	}
}

func checkMask(js *simplejson.Json) (string, error) {
	if js.MustString() == "" {
		value, err := jsNumberTypeConvert(js, "long")
		if err != nil {
			return "", errors.Wrap(err, "mask type must be long")
		}
		return value, nil
	} else {
		value, err := jsStringNumberConvert(js, "long")
		if err != nil {
			return "", errors.Wrap(err, "mask type must be long")
		}
		return value, nil
	}
}

func checkShift(js *simplejson.Json) (string, error) {
	if js.MustString() == "" {
		_, err := js.Float64()
		if err != nil {
			return "", fmt.Errorf("invalid shift value, the shift should be parsed to float64 for checking the sign of the number. %v", err)
		}
		shift, err := js.Int64()
		if err != nil {
			return "", fmt.Errorf("the shift of PropertyValue cannot be parsed to int64: %v", err)
		}
		return strconv.FormatInt(shift, 10), nil
	} else {
		_, err := strconv.ParseFloat(js.MustString(), 64)
		if err != nil {
			return "", fmt.Errorf("invalid shift value, the shift should be parsed to float64 for checking the sign of the number. %v", err)
		}
		signedShift, err := strconv.ParseInt(js.MustString(), 10, 64)
		if err != nil {
			return "", fmt.Errorf("the shift %s of PropertyValue cannot be parsed to %T: %v", js.MustString(), signedShift, err)
		}
		return js.MustString(), nil
	}
}

func checkMaximum(js *simplejson.Json, valueType string) (string, error) {
	if js.MustString() == "" {
		value, err := jsNumberTypeConvert(js, valueType)
		if err != nil {
			return "", errors.Wrap(err, "maximum type must be same as valueType")
		}
		return value, nil
	} else {
		value, err := jsStringNumberConvert(js, valueType)
		if err != nil {
			return "", errors.Wrap(err, "maximum type must be same as valueType")
		}
		return value, nil
	}
}

func checkMinimum(js *simplejson.Json, valueType string) (string, error) {
	if js.MustString() == "" {
		value, err := jsNumberTypeConvert(js, valueType)
		if err != nil {
			return "", errors.Wrap(err, "minimum type must be same as valueType")
		}
		return value, nil
	} else {
		value, err := jsStringNumberConvert(js, valueType)
		if err != nil {
			return "", errors.Wrap(err, "minimum type must be same as valueType")
		}
		return value, nil
	}
}

func checkScale(js *simplejson.Json) (string, error) {
	if js.MustString() == "" {
		value, err := jsNumberTypeConvert(js, "double")
		if err != nil {
			return "", errors.Wrap(err, "scale type must be double")
		}
		return value, nil
	} else {
		value, err := jsStringNumberConvert(js, "double")
		if err != nil {
			return "", errors.Wrap(err, "scale type must be double")
		}
		return value, nil
	}
}

func checkBase(js *simplejson.Json) (string, error) {
	if js.MustString() == "" {
		value, err := jsNumberTypeConvert(js, "double")
		if err != nil {
			return "", errors.Wrap(err, "base type must be double")
		}
		return value, nil
	} else {
		value, err := jsStringNumberConvert(js, "double")
		if err != nil {
			return "", errors.Wrap(err, "base type must be double")
		}
		return value, nil
	}
}

func checkOffset(js *simplejson.Json, valueType string) (string, error) {
	if js.MustString() == "" {
		value, err := jsNumberTypeConvert(js, valueType)
		if err != nil {
			return "", errors.Wrap(err, "offset type must be same as valueType")
		}
		return value, nil
	} else {
		value, err := jsStringNumberConvert(js, valueType)
		if err != nil {
			return "", errors.Wrap(err, "offset type must be same as valueType")
		}
		return value, nil
	}
}

func checkFloatEncoding(js *simplejson.Json) (string, error) {
	encoding := js.MustString()
	if encoding == "Base64" || encoding == "eNotation" {
		return encoding, nil
	} else {
		return "", errors.New("floatEncoding value must be Base64 or eNotation")
	}
}

func jsStringNumberConvert(js *simplejson.Json, valueType string) (string, error) {
	switch valueType {
	case "int":
		if _, err := strconv.Atoi(js.MustString()); err != nil {
			return "", err
		} else {
			return js.MustString(), nil
		}
	case "long":
		if _, err := strconv.ParseInt(js.MustString(), 10, 64); err != nil {
			return "", err
		} else {
			return js.MustString(), nil
		}
	case "float":
		if _, err := strconv.ParseFloat(js.MustString(), 32); err != nil {
			return "", err
		} else {
			return js.MustString(), nil
		}
	case "double":
		if _, err := strconv.ParseFloat(js.MustString(), 64); err != nil {
			return "", err
		} else {
			return js.MustString(), nil
		}
	default:
		return "", errors.New("not a number type")
	}
}

func jsNumberTypeConvert(js *simplejson.Json, valueType string) (string, error) {
	switch valueType {
	case "int":
		value, err := js.Int()
		if err != nil {
			return "", err
		}
		return strconv.Itoa(value), nil
	case "long":
		value, err := js.Int64()
		if err != nil {
			return "", err
		}
		return strconv.FormatInt(value, 10), nil
	case "float":
		value, err := js.Float64()
		if err != nil {
			return "", err
		}
		return strconv.FormatFloat(value, 'f', 10, 32), nil
	case "double":
		value, err := js.Float64()
		if err != nil {
			return "", err
		}
		return strconv.FormatFloat(value, 'f', 10, 64), nil
	default:
		return "", errors.New("not a number type")
	}
}
