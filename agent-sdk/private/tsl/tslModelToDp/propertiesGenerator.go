package tslModelToDp

import (
	"errors"
	"fmt"
	"strings"

	"github.com/bitly/go-simplejson"
)

/**
物模型属性转deviceProfile属性
*/
func PropertiesGenerate(js *simplejson.Json) (string, map[string]*ModelProperties, error) {
	if _, ok := js.CheckGet("properties"); !ok {
		return "", nil, errors.New("properties cannot be empty")
	}
	propertiesJs := js.Get("properties")
	array, err := propertiesJs.Array()
	if err != nil {
		return "", nil, fmt.Errorf("properties require array type, error: %v", err)
	}
	propertiesStr := "deviceResources:\n"
	propertyMap := make(map[string]*ModelProperties)
	for i := 0; i < len(array); i++ {
		properties, err := PropertiesConvert(propertiesJs.GetIndex(i))
		if err != nil {
			return "", nil, err
		}
		for _, property := range properties {
			if propertyMap[property.Id] == nil {
				propertyMap[property.Id] = property
			} else {
				return "", nil, errors.New("duplicate define property id " + property.Id)
				//exist := propertyMap[property.Id]
				//if !compareProperty(property, exist) {
				//	return "", nil, errors.New("duplicate define property id " + property.Id)
				//} else {
				//	if exist.Tag == "" {
				//		exist.Tag = property.Tag
				//	} else {
				//		exist.Tag += "," + property.Tag
				//	}
				//}
			}
		}
	}
	for _, property := range propertyMap {

		propertiesStr += fmt.Sprintf("  - name: \"%s\"\n", processStrFormat(property.Id)) +
			fmt.Sprintf("    description: \"%s##%s\"\n", processStrFormat(property.Name), processStrFormat(property.Description)) +
			fmt.Sprintf("    tag: \"%s\"\n", processStrFormat(property.Tag))
		if len(property.Attributes) > 0 {
			attributeStr := "    attributes:\n      {"
			for key, value := range property.Attributes {
				attributeStr += fmt.Sprintf(" %s: \"%s\",", processStrFormat(key), processStrFormat(value))
			}
			attributeStr = attributeStr[:len(attributeStr)-1] + " }\n"
			propertiesStr += attributeStr
		}
		propertiesStr += "    properties:\n"
		valueStr := fmt.Sprintf("      value:\n        { type: \"%s\"", processStrFormat(property.Type))
		if _, ok := property.Properties["readWrite"]; !ok {
			property.Properties["readWrite"] = "RW"
		}
		for key, value := range property.Properties {
			valueStr += fmt.Sprintf(", %s: \"%s\"", processStrFormat(key), processStrFormat(value))
		}
		valueStr += " }\n"
		propertiesStr += valueStr
		unitStr := "      units:\n        { type: \"string\", readWrite: \"R\""
		if property.Unit != "" {
			unitStr += ", defaultValue: \"" + processStrFormat(property.Unit) + "\" }\n"
		} else {
			unitStr += " }\n"
		}
		propertiesStr += unitStr

	}
	return propertiesStr, propertyMap, nil
}

func processStrFormat(str string) string {
	return strings.ReplaceAll(str, "\"", "\\\"")
}

func compareProperty(p1 *ModelProperties, p2 *ModelProperties) bool {
	tag1 := p1.Tag
	tag2 := p2.Tag
	p1.Tag = ""
	p2.Tag = ""
	str1 := fmt.Sprintf("%v", p1)
	str2 := fmt.Sprintf("%v", p2)
	equal := str1 == str2
	p1.Tag = tag1
	p2.Tag = tag2
	return equal
}
