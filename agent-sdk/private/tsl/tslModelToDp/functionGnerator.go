package tslModelToDp

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"github.com/bitly/go-simplejson"
)

const (
	PROPERTY_FUNC_PREFFIX = "pof_"
	FUNCTION_FUNC_PREFFIX = "fof_"
)

/**
物模型功能转
*/
func FunctionGenerate(js *simplejson.Json, propertyMap map[string]*ModelProperties) (string, error) {
	deviceCommand, coreCommand, err := commonfuncGenerate(propertyMap)
	if err != nil {
		return "", err
	}
	deviceCommand = "deviceCommands:\n" + deviceCommand
	coreCommand = "coreCommands:\n" + coreCommand
	if _, ok := js.CheckGet("functions"); !ok {
		return deviceCommand + coreCommand, nil
	}
	functionsJs := js.Get("functions")
	array, err := functionsJs.Array()
	if err != nil {
		return "", fmt.Errorf("functions require array type, error: %v", err)
	}
	for i := 0; i < len(array); i++ {
		functionJs := functionsJs.GetIndex(i)
		if _, ok := functionJs.CheckGet("id"); !ok {
			return "", errors.New("function id cannot be empty")
		}
		id := functionJs.Get("id").MustString()
		if id == "" {
			return "", errors.New("function id cannot be empty")
		}
		if _, ok := functionJs.CheckGet("name"); !ok {
			return "", errors.New("function name cannot be empty")
		}
		name := functionJs.Get("name").MustString()
		if name == "" {
			return "", errors.New("function name cannot be empty")
		}
		inputIds := make([]string, 0)
		outputIds := make([]string, 0)
		inputsJs, ok := functionJs.CheckGet("inputs")
		if !ok {
			return "", errors.New("functions inputs cannot be null")
		}
		tmp, err := inputsJs.Array()
		if err != nil {
			return "", fmt.Errorf("functions inputs require array type, error: %v", err)
		}
		for j := 0; j < len(tmp); j++ {
			inputProperties, err := PropertiesConvert(inputsJs.GetIndex(j))
			if err != nil {
				return "", err
			}
			for _, property := range inputProperties {
				if exist, ok := propertyMap[property.Id]; ok {
					if !compareProperty(exist, property) {
						return "", errors.New("function inputs must be same as that defined in properties")
					}
					inputIds = append(inputIds, property.Id)
				} else {
					return "", errors.New("function inputs must defined in properties")
				}
			}
		}
		outputJs, ok := functionJs.CheckGet("outputs")
		if !ok {
			return "", errors.New("functions outputs cannot be null")
		}
		tmp, err = outputJs.Array()
		if err != nil {
			return "", fmt.Errorf("functions output require array type, error: %v", err)
		}
		for j := 0; j < len(tmp); j++ {
			outputProperties, err := PropertiesConvert(outputJs.GetIndex(j))
			if err != nil {
				return "", err
			}
			for _, property := range outputProperties {
				if exist, ok := propertyMap[property.Id]; ok {
					if !compareProperty(exist, property) {
						return "", errors.New("function outputs must be same as that defined in properties")
					}
					outputIds = append(outputIds, property.Id)
				} else {
					return "", errors.New("function output must defined in properties")
				}
			}
		}
		deviceCommand += fmt.Sprintf("  - name: \"%s\"\n", FUNCTION_FUNC_PREFFIX+processStrFormat(id))
		deviceCommand += deviceCommandGenerate(false, inputIds)
		coreCommand += fmt.Sprintf("  - name: %s\n", FUNCTION_FUNC_PREFFIX+id)
		coreCommand += coreCommandGenerate(FUNCTION_FUNC_PREFFIX+id, name, false, inputIds, outputIds)
	}
	return deviceCommand + coreCommand, nil
}

func commonfuncGenerate(propertyMap map[string]*ModelProperties) (string, string, error) {
	deviceCommand := ""
	coreCommand := ""
	tagMap := make(map[string][]*ModelProperties)
	for _, property := range propertyMap {
		if property.Tag == "" {
			deviceCommand += fmt.Sprintf("  - name: \"%s\"\n", PROPERTY_FUNC_PREFFIX+property.Id)
			deviceCommand += deviceCommandGenerate(true, []string{property.Id})
			coreCommand += fmt.Sprintf("  - name: %s\n", PROPERTY_FUNC_PREFFIX+property.Id)
			coreCommand += coreCommandGenerate(PROPERTY_FUNC_PREFFIX+property.Id, "", true, []string{}, []string{property.Id})
			if rw, ok := property.Properties["readWrite"]; ok {
				if strings.Contains(strings.ToLower(rw), "w") {
					deviceCommand += deviceCommandGenerate(false, []string{property.Id})
					coreCommand += coreCommandGenerate(PROPERTY_FUNC_PREFFIX+property.Id, "", false, []string{property.Id}, []string{})
				}
			}
		}
		if property.Tag != "" {
			if properties, ok := tagMap[property.Tag]; ok {
				properties = append(properties, property)
				tagMap[property.Tag] = properties
			} else {
				properties = []*ModelProperties{property}
				tagMap[property.Tag] = properties
			}
		}
	}
	for tag, properties := range tagMap {
		objectProperty := ObjectProperty{}
		err := json.Unmarshal([]byte(tag), &objectProperty)
		if err != nil {
			return "", "", fmt.Errorf("deserialize tag failed, #%v", err)
		}
		propertyIds := make([]string, 0)
		for _, property := range properties {
			propertyIds = append(propertyIds, property.Id)
		}
		deviceCommand += fmt.Sprintf("  - name: \"%s\"\n", PROPERTY_FUNC_PREFFIX+objectProperty.Id)
		deviceCommand += deviceCommandGenerate(true, propertyIds)
		deviceCommand += deviceCommandGenerate(false, propertyIds)
		coreCommand += fmt.Sprintf("  - name: %s\n", PROPERTY_FUNC_PREFFIX+objectProperty.Id)
		coreCommand += coreCommandGenerate(PROPERTY_FUNC_PREFFIX+objectProperty.Id, "", true, []string{}, propertyIds)
		coreCommand += coreCommandGenerate(PROPERTY_FUNC_PREFFIX+objectProperty.Id, "", false, propertyIds, []string{})
	}
	return deviceCommand, coreCommand, nil
}

func deviceCommandGenerate(isGet bool, resources []string) string {
	deviceCommand := ""
	if isGet {
		deviceCommand += "    get:\n"
		for _, resource := range resources {
			deviceCommand += fmt.Sprintf("      - { operation: \"get\", deviceResource: \"%s\" }\n", resource)
		}
	} else {
		deviceCommand += "    set:\n"
		for _, resource := range resources {
			deviceCommand += fmt.Sprintf("      - { operation: \"set\", deviceResource: \"%s\" }\n", resource)
		}
	}
	return deviceCommand
}

func coreCommandGenerate(name string, description string, isGet bool, input []string, output []string) string {
	coreCommand := ""
	if isGet {
		coreCommand += fmt.Sprintf("    get:\n      path: \"/api/v1/device/{deviceId}/%s\"\n", name)
		if len(input) > 0 {
			coreCommand += fmt.Sprintf("      parameterNames: [\"%s\"]\n", strings.Join(input, "\",\""))
		}
		coreCommand += "      responses:\n"
		coreCommand += "        -\n          code: \"200\"\n"
		if description != "" {
			coreCommand += fmt.Sprintf("          description: \"%s\"\n", description)
		} else {
			coreCommand += "          description: \"\"\n"
		}
		if len(output) > 0 {
			coreCommand += fmt.Sprintf("          expectedValues: [\"%s\"]\n", strings.Join(output, "\",\""))
		}
		coreCommand += "        -\n          code: \"503\"\n"
		coreCommand += "          description: \"internal server error\"\n"
		coreCommand += "          expectedValues: []\n"
	} else {
		coreCommand += fmt.Sprintf("    put:\n      path: \"/api/v1/device/{deviceId}/%s\"\n", name)
		if len(input) > 0 {
			coreCommand += fmt.Sprintf("      parameterNames: [\"%s\"]\n", strings.Join(input, "\",\""))
		}
		coreCommand += "      responses:\n"
		coreCommand += "        -\n          code: \"200\"\n"
		if description != "" {
			coreCommand += fmt.Sprintf("          description: \"%s\"\n", description)
		} else {
			coreCommand += "          description: \"\"\n"
		}
		if len(output) > 0 {
			coreCommand += fmt.Sprintf("          expectedValues: [\"%s\"]\n", strings.Join(output, "\",\""))
		} else {
			coreCommand += "          expectedValues: []\n"
		}
		coreCommand += "        -\n          code: \"503\"\n"
		coreCommand += "          description: \"internal server error\"\n"
		coreCommand += "          expectedValues: []\n"
	}
	return coreCommand
}
