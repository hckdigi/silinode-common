package tslModelToDp

import (
	"fmt"

	"github.com/bitly/go-simplejson"
)

/**
物模型转deviceProfile
输入参数为物模型的json字符串，输出deviceProfile文件字符串
*/
func ThingsModelToDp(model string) (string, error) {
	js, err := simplejson.NewJson([]byte(model))
	if err != nil {
		return "", fmt.Errorf("转换json失败，error: #%v", err)
	}
	deivceId := js.Get("id").MustString()
	deviceName := js.Get("name").MustString()
	if deivceId == "" {
		return "", fmt.Errorf("设备id不能为空")
	}
	if deviceName == "" {
		return "", fmt.Errorf("设备名称不能为空")
	}
	deviceProfile := fmt.Sprintf("name: \"%s\"\ndescription: \"%s\"\n", deivceId, deviceName)
	deviceProfile += "manufacturer: \"\"\nmodel: \"\"\nlabels:\n"
	propertiesStr, propertyMap, err := PropertiesGenerate(js)
	if err != nil {
		return "", fmt.Errorf("解析属性失败， error: #%v", err)
	}
	deviceProfile += propertiesStr
	commandStr, err := FunctionGenerate(js, propertyMap)
	if err != nil {
		return "", fmt.Errorf("解析功能失败， error: #%v", err)
	}
	deviceProfile += commandStr
	return deviceProfile, nil
}
