package tslModelToDp

import (
	json2 "encoding/json"
	"errors"
	"fmt"

	"github.com/bitly/go-simplejson"
)

type ModelProperties struct {
	Id          string
	Name        string
	Type        string
	OriginType  string
	Tag         string
	Description string
	Properties  map[string]string
	Attributes  map[string]string
	Unit        string
}

func commonConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties := ModelProperties{}
	properties.Properties = make(map[string]string)
	properties.Attributes = make(map[string]string)
	id := js.Get("id").MustString()
	if id == "" {
		return nil, errors.New("id cannot be empty")
	}
	properties.Id = id
	name := js.Get("name").MustString()
	if name == "" {
		return nil, errors.New("name cannot be empty")
	}
	properties.Name = name
	if expandsJs, ok := js.CheckGet("expands"); ok {
		expands, err := expandsJs.Map()
		if err != nil {
			return nil, errors.New("expands illegal")
		}
		properties.Attributes = expandConvert(expands)
	}
	return &properties, nil
}

func expandConvert(expands map[string]interface{}) map[string]string {
	result := make(map[string]string)
	for key, value := range expands {
		switch t := value.(type) {
		case string:
			result[key] = t
		case int, int32, int64:
			result[key] = fmt.Sprintf("%d", t)
		case float64, float32:
			result[key] = fmt.Sprintf("%f", t)
		case bool:
			result[key] = fmt.Sprintf("%t", t)
		}
	}
	return result
}

func IntConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "int" {
			return properties, errors.New("unsupported type require int now " + dataType)
		}
		if valueJs, ok := typeJs.CheckGet("maxValue"); ok {
			str, err := checkMaximum(valueJs, dataType)
			if err != nil {
				return nil, err
			}
			properties.Properties["maximum"] = str
		}
		if valueJs, ok := typeJs.CheckGet("minValue"); ok {
			str, err := checkMinimum(valueJs, dataType)
			if err != nil {
				return nil, err
			}
			properties.Properties["minimum"] = str
		}
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				str, err := checkDefaultValue(defaultValueJs, dataType)
				if err != nil {
					return nil, err
				}
				properties.Properties["defaultValue"] = str
			}
			if maskJs, ok := expandsJs.CheckGet("mask"); ok {
				str, err := checkMask(maskJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["mask"] = str
			}
			if shiftJs, ok := expandsJs.CheckGet("shift"); ok {
				str, err := checkShift(shiftJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["shift"] = str
			}
			if scaleJs, ok := expandsJs.CheckGet("scale"); ok {
				str, err := checkScale(scaleJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["scale"] = str
			}
			if offsetJs, ok := expandsJs.CheckGet("offset"); ok {
				str, err := checkOffset(offsetJs, dataType)
				if err != nil {
					return nil, err
				}
				properties.Properties["offset"] = str
			}
			if baseJs, ok := expandsJs.CheckGet("base"); ok {
				str, err := checkBase(baseJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["base"] = str
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else if typeJs, ok := js.CheckGet("type"); ok {
		dataType := typeJs.MustString()
		if dataType != "int" {
			return properties, errors.New("unsupported type require int now " + dataType)
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.Description = "$type:int$"
	properties.OriginType = "int"
	properties.Type = "int32"
	return properties, nil
}

func LongConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "long" {
			return properties, errors.New("unsupported type require long now " + dataType)
		}
		if valueJs, ok := typeJs.CheckGet("maxValue"); ok {
			str, err := checkMaximum(valueJs, dataType)
			if err != nil {
				return nil, err
			}
			properties.Properties["maximum"] = str
		}
		if valueJs, ok := typeJs.CheckGet("minValue"); ok {
			str, err := checkMinimum(valueJs, dataType)
			if err != nil {
				return nil, err
			}
			properties.Properties["minimum"] = str
		}
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				str, err := checkDefaultValue(defaultValueJs, dataType)
				if err != nil {
					return nil, err
				}
				properties.Properties["defaultValue"] = str
			}
			if maskJs, ok := expandsJs.CheckGet("mask"); ok {
				str, err := checkMask(maskJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["mask"] = str
			}
			if shiftJs, ok := expandsJs.CheckGet("shift"); ok {
				str, err := checkShift(shiftJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["shift"] = str
			}
			if scaleJs, ok := expandsJs.CheckGet("scale"); ok {
				str, err := checkScale(scaleJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["scale"] = str
			}
			if offsetJs, ok := expandsJs.CheckGet("offset"); ok {
				str, err := checkOffset(offsetJs, dataType)
				if err != nil {
					return nil, err
				}
				properties.Properties["offset"] = str
			}
			if baseJs, ok := expandsJs.CheckGet("base"); ok {
				str, err := checkBase(baseJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["base"] = str
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else if typeJs, ok := js.CheckGet("type"); ok {
		dataType := typeJs.MustString()
		if dataType != "long" {
			return properties, errors.New("unsupported type require long now " + dataType)
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.Description = "$type:long$"
	properties.OriginType = "long"
	properties.Type = "int64"
	return properties, nil
}

func FloatConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "float" {
			return properties, errors.New("unsupported type require float now " + dataType)
		}
		if valueJs, ok := typeJs.CheckGet("maxValue"); ok {
			str, err := checkMaximum(valueJs, dataType)
			if err != nil {
				return nil, err
			}
			properties.Properties["maximum"] = str
		}
		if valueJs, ok := typeJs.CheckGet("minValue"); ok {
			str, err := checkMinimum(valueJs, dataType)
			if err != nil {
				return nil, err
			}
			properties.Properties["minimum"] = str
		}
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				str, err := checkDefaultValue(defaultValueJs, dataType)
				if err != nil {
					return nil, err
				}
				properties.Properties["defaultValue"] = str
			}
			if scaleJs, ok := expandsJs.CheckGet("scale"); ok {
				str, err := checkScale(scaleJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["scale"] = str
			}
			if offsetJs, ok := expandsJs.CheckGet("offset"); ok {
				str, err := checkOffset(offsetJs, dataType)
				if err != nil {
					return nil, err
				}
				properties.Properties["offset"] = str
			}
			if baseJs, ok := expandsJs.CheckGet("base"); ok {
				str, err := checkBase(baseJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["base"] = str
			}
			if floatEncodingJs, ok := expandsJs.CheckGet("floatEncoding"); ok {
				str, err := checkFloatEncoding(floatEncodingJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["floatEncoding"] = str
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else if typeJs, ok := js.CheckGet("type"); ok {
		dataType := typeJs.MustString()
		if dataType != "float" {
			return properties, errors.New("unsupported type require float now " + dataType)
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.Description = "$type:float$"
	properties.OriginType = "float"
	properties.Type = "float32"
	return properties, nil
}

func DoubleConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "double" {
			return properties, errors.New("unsupported type require double now " + dataType)
		}
		if valueJs, ok := typeJs.CheckGet("maxValue"); ok {
			str, err := checkMaximum(valueJs, dataType)
			if err != nil {
				return nil, err
			}
			properties.Properties["maximum"] = str
		}
		if valueJs, ok := typeJs.CheckGet("minValue"); ok {
			str, err := checkMinimum(valueJs, dataType)
			if err != nil {
				return nil, err
			}
			properties.Properties["minimum"] = str
		}
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				str, err := checkDefaultValue(defaultValueJs, dataType)
				if err != nil {
					return nil, err
				}
				properties.Properties["defaultValue"] = str
			}
			if scaleJs, ok := expandsJs.CheckGet("scale"); ok {
				str, err := checkScale(scaleJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["scale"] = str
			}
			if offsetJs, ok := expandsJs.CheckGet("offset"); ok {
				str, err := checkOffset(offsetJs, dataType)
				if err != nil {
					return nil, err
				}
				properties.Properties["offset"] = str
			}
			if baseJs, ok := expandsJs.CheckGet("base"); ok {
				str, err := checkBase(baseJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["base"] = str
			}
			if floatEncodingJs, ok := expandsJs.CheckGet("floatEncoding"); ok {
				str, err := checkFloatEncoding(floatEncodingJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["floatEncoding"] = str
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else if typeJs, ok := js.CheckGet("type"); ok {
		dataType := typeJs.MustString()
		if dataType != "double" {
			return properties, errors.New("unsupported type require double now " + dataType)
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.Description = "$type:double$"
	properties.OriginType = "double"
	properties.Type = "float64"
	return properties, nil
}

func BooleanConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "boolean" {
			return properties, errors.New("unsupported type require boolean now " + dataType)
		}
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				properties.Properties["defaultValue"] = defaultValueJs.MustString()
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else if typeJs, ok := js.CheckGet("type"); ok {
		dataType := typeJs.MustString()
		if dataType != "boolean" {
			return properties, errors.New("unsupported type require boolean now " + dataType)
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.Description = "$type:boolean$"
	properties.OriginType = "boolean"
	properties.Type = "bool"
	return properties, nil
}

func StringConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "string" {
			return properties, errors.New("unsupported type require string now " + dataType)
		}
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				properties.Properties["defaultValue"] = defaultValueJs.MustString()
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else if typeJs, ok := js.CheckGet("type"); ok {
		dataType := typeJs.MustString()
		if dataType != "string" {
			return properties, errors.New("unsupported type require string now " + dataType)
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.Description = "$type:string$"
	properties.OriginType = "string"
	properties.Type = "string"
	return properties, nil
}

func EnumConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "enum" {
			return properties, errors.New("unsupported type require enum now " + dataType)
		}
		if elementsJs, ok := typeJs.CheckGet("elements"); !ok {
			return properties, errors.New("enum elements cannot be empty")
		} else {
			valueMap := make([]map[string]interface{}, 0)
			array, err := elementsJs.Array()
			if err != nil {
				return properties, fmt.Errorf("enum elements illegal, error: %v", err)
			}
			for index, element := range array {
				elementMap := element.(map[string]interface{})
				if _, ok := elementMap["value"]; !ok {
					return properties, errors.New("enum element value and text cannot be empty")
				}
				if _, ok := elementMap["text"]; !ok {
					return properties, errors.New("enum element value and text cannot be empty")
				}
				valueMap = append(valueMap, elementMap)
				if index == 0 {
					switch elementMap["value"].(type) {
					case int:
						properties.Type = "int32"
					case int64:
						properties.Type = "int64"
					case float32:
						properties.Type = "float32"
					case float64:
						properties.Type = "float64"
					case bool:
						properties.Type = "bool"
					case string:
						properties.Type = "string"
					default:
						return properties, errors.New("enum element value type illegal, require int or float or string")
					}
				}
			}
			json, err := json2.Marshal(valueMap)
			if err != nil {
				return properties, errors.New("serialize enum elements failed")
			}
			properties.OriginType = "enum"
			properties.Description = "$type:enum$" + string(json)
		}
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				properties.Properties["defaultValue"] = defaultValueJs.MustString()
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	return properties, nil
}

func DateConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "date" {
			return properties, errors.New("unsupported type require date now " + dataType)
		}
		valueMap := make(map[string]interface{}, 0)
		valueMap["format"] = typeJs.Get("format").MustString("yyyy-MM-dd")
		valueMap["tz"] = typeJs.Get("tz").MustString("Asia/Shanghai")
		json, err := json2.Marshal(valueMap)
		if err != nil {
			return properties, errors.New("serialize enum elements failed")
		}
		properties.Description = "$type:date$" + string(json)
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				properties.Properties["defaultValue"] = defaultValueJs.MustString()
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.OriginType = "date"
	properties.Type = "int64"
	return properties, nil
}

func PasswordConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "password" {
			return properties, errors.New("unsupported type require password now " + dataType)
		}
		properties.Description = "$type:password$"
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				properties.Properties["defaultValue"] = defaultValueJs.MustString()
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.OriginType = "password"
	properties.Type = "string"
	return properties, nil
}

func ArrayConvert(js *simplejson.Json) (*ModelProperties, error) {
	properties, err := commonConvert(js)
	if err != nil {
		return properties, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "array" {
			return properties, errors.New("unsupported type require array now " + dataType)
		}
		if _, ok := typeJs.CheckGet("elementType"); !ok {
			return properties, errors.New("array elementType cannot be empty")
		}
		if _, ok := typeJs.Get("elementType").CheckGet("type"); !ok {
			return properties, errors.New("array elementType cannot be empty")
		}
		elementType := typeJs.Get("elementType").Get("type").MustString()
		switch elementType {
		case "int":
			properties.Type = "int32array"
		case "long":
			properties.Type = "int64array"
		case "float":
			properties.Type = "float32array"
		case "double":
			properties.Type = "float64array"
		case "boolean":
			properties.Type = "boolarray"
		default:
			return properties, errors.New("unsupported array elementType require int or long or float or double or boolean")
		}
		valueMap := make(map[string]interface{})
		valueMap["type"] = elementType
		json, err := json2.Marshal(valueMap)
		if err != nil {
			return properties, errors.New("serialize enum elements failed")
		}
		properties.Description = "$type:array$" + string(json)
		if expandsJs, ok := typeJs.CheckGet("expands"); ok {
			if readWriteJs, ok := expandsJs.CheckGet("readOnly"); ok {
				readWrite, err := checkReadWriteValue(readWriteJs)
				if err != nil {
					return nil, err
				}
				properties.Properties["readWrite"] = readWrite
			} else {
				properties.Properties["readWrite"] = "RW"
			}
			if defaultValueJs, ok := expandsJs.CheckGet("defaultValue"); ok {
				properties.Properties["defaultValue"] = defaultValueJs.MustString()
			}
			if assertionJs, ok := expandsJs.CheckGet("assertion"); ok {
				properties.Properties["assertion"] = assertionJs.MustString()
			}
		}
		if _, ok := properties.Properties["readWrite"]; !ok {
			properties.Properties["readWrite"] = "RW"
		}
		if unitJs, ok := typeJs.CheckGet("unit"); ok {
			properties.Unit = unitJs.MustString()
		}
	} else {
		return properties, errors.New("type cannot be empty")
	}
	properties.OriginType = "array"
	return properties, nil
}

func ObjectConvert(js *simplejson.Json) ([]*ModelProperties, error) {
	propertiesList := make([]*ModelProperties, 0)
	propertyMap := make(map[string]*ModelProperties)
	tmp, err := commonConvert(js)
	if err != nil {
		return nil, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "object" {
			return nil, errors.New("unsupported type require object now " + dataType)
		}
		if _, ok := typeJs.CheckGet("properties"); !ok {
			return propertiesList, errors.New("object properties cannot be empty")
		}
		propertiesJs := typeJs.Get("properties")
		propertiesArray, err := propertiesJs.Array()
		if err != nil {
			return propertiesList, fmt.Errorf("object properties require array, error: %v", err)
		}
		for i := 0; i < len(propertiesArray); i++ {
			properties, err := PropertiesConvert(propertiesJs.GetIndex(i))
			if err != nil {
				return propertiesList, err
			}

			for _, property := range properties {
				if propertyMap[property.Id] != nil {
					//exist := propertyMap[property.Id]
					//str1 := fmt.Sprintf("%v", exist)
					//str2 := fmt.Sprintf("%v", property)
					//if str1 != str2 {
					return propertiesList, errors.New("duplicate define property id " + property.Id)
					//}
				} else {
					propertyMap[property.Id] = property
				}
				propertiesList = append(propertiesList, property)
			}
		}
		objectProperty, err := generateObjectPropertyStruct(tmp.Id, tmp.Name, "object", propertiesList)
		if err != nil {
			return propertiesList, err
		}
		objectJson, err := json2.Marshal(objectProperty)
		if err != nil {
			return propertiesList, fmt.Errorf("serialize objectProperty failed, #%v", err)
		}
		for _, property := range propertiesList {
			property.Tag = string(objectJson)
		}
	} else {
		return nil, errors.New("type cannot be empty")
	}
	return propertiesList, nil
}

func GeoPointConvert(js *simplejson.Json) ([]*ModelProperties, error) {
	propertiesList := make([]*ModelProperties, 0)
	tmp, err := commonConvert(js)
	propertyMap := make(map[string]*ModelProperties)
	if err != nil {
		return nil, err
	}
	if typeJs, ok := js.CheckGet("valueType"); ok {
		dataType := typeJs.Get("type").MustString()
		if dataType != "geoPoint" {
			return nil, errors.New("unsupported type require geoPoint now " + dataType)
		}
		if _, ok := js.CheckGet("properties"); !ok {
			return propertiesList, errors.New("object properties cannot be empty")
		}
		propertiesJs := js.Get("properties")
		propertiesArray, err := propertiesJs.Array()
		if err != nil {
			return propertiesList, fmt.Errorf("object properties require array, error: %v", err)
		}
		for i := 0; i < len(propertiesArray); i++ {
			properties, err := PropertiesConvert(propertiesJs.GetIndex(i))
			if err != nil {
				return propertiesList, err
			}
			for _, property := range properties {
				if propertyMap[property.Id] != nil {
					exist := propertyMap[property.Id]
					str1 := fmt.Sprintf("%v", exist)
					str2 := fmt.Sprintf("%v", property)
					if str1 != str2 {
						return propertiesList, errors.New("duplicate define property id " + property.Id)
					}
				} else {
					propertyMap[property.Id] = property
				}
				propertiesList = append(propertiesList, property)
			}
		}
		objectProperty, err := generateObjectPropertyStruct(tmp.Id, tmp.Name, "geoPoint", propertiesList)
		if err != nil {
			return propertiesList, err
		}
		objectJson, err := json2.Marshal(objectProperty)
		if err != nil {
			return propertiesList, fmt.Errorf("serialize objectProperty failed, #%v", err)
		}
		for _, property := range propertiesList {
			property.Tag = string(objectJson)
		}
	} else {
		return nil, errors.New("type cannot be empty")
	}
	return propertiesList, nil
}

func generateObjectPropertyStruct(id string, name string, propertyType string, properties []*ModelProperties) (ObjectProperty, error) {
	objectProperty := ObjectProperty{
		Id:       id,
		Name:     name,
		Type:     propertyType,
		Children: make([]*ObjectProperty, 0),
	}
	if len(properties) > 0 {
		for _, property := range properties {
			if property.Tag != "" {
				childProperty, err := analyzeTag(property.Tag)
				if err != nil {
					return ObjectProperty{}, err
				}
				objectProperty.Children = append(objectProperty.Children, &childProperty)
			} else {
				childProperty := ObjectProperty{Id: property.Id, Name: property.Name, Type: property.OriginType}
				objectProperty.Children = append(objectProperty.Children, &childProperty)
			}
		}
		return objectProperty, nil
	} else {
		return objectProperty, errors.New("object properties cannot be empty")
	}
}

func analyzeTag(tag string) (ObjectProperty, error) {
	objectProperty := ObjectProperty{}
	err := json2.Unmarshal([]byte(tag), &objectProperty)
	if err != nil {
		return ObjectProperty{}, fmt.Errorf("tag analysis failed, cannot deserialize objectProperty, #%v", err)
	}
	return objectProperty, nil
}
