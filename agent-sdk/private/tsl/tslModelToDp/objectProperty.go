package tslModelToDp

type ObjectProperty struct {
	Id       string            `json:"id"`
	Name     string            `json:"name"`
	Type     string            `json:"type"`
	Children []*ObjectProperty `json:"children"`
}
