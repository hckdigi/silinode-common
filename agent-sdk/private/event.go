package internal

import (
	contract "gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
)

type Event struct {
	Bytes         []byte
	CorrelationId string
	Checksum      string
	contract.Event
}
