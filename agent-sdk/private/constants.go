package internal

const (
	// services

	srvMeta   = "http://edgex-core-metadata:48081"
	srvCmd    = "http://edgex-core-command:48082"
	srvSma    = "http://edgex-sys-mgmt-agent:48090"
	srvEdgexd = "http://host.docker.internal:50002"

	// url prefixes

	UrlMetaDevice        = srvMeta + "/api/v1/device"
	UrlMetaDeviceProfile = srvMeta + "/api/v1/deviceprofile"
	UrlMetaDeviceService = srvMeta + "/api/v1/deviceservice"
	UrlMetaDeviceModel   = srvMeta + "/api/v1/devicemodel"
	UrlCommand           = srvCmd + "/api/v1/device"
	UrlMetaHost          = srvMeta + "/api/v1/host"
	UrlSma               = srvSma
	UrlEdgexd            = srvEdgexd
)
