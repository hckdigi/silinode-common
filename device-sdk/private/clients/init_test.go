// -*- Mode: Go; indent-tabs-mode: t -*-
//
// Copyright (C) 2018 IOTech Ltd
//
// SPDX-License-Identifier: Apache-2.0

package clients

import (
	"net"
	"testing"

	bootstrapConfig "gitee.com/hckdigi/silinode-common/mod/bootstrap/config"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/logger"
	"gitee.com/hckdigi/silinode-common/device-sdk/private/common"
)

func TestCheckServiceAvailableByPingWithTimeoutError(test *testing.T) {
	var clientConfig = map[string]bootstrapConfig.ClientInfo{
		common.ClientData: {
			Host:     "www.google.com",
			Port:     81,
			Protocol: "http",
		},
	}
	config := &common.ConfigurationStruct{Clients: clientConfig}
	lc := logger.NewClientStdOut("device-sdk-test", false, "DEBUG")

	err := checkServiceAvailableByPing(common.ClientData, config, lc)
	if err, ok := err.(net.Error); ok && !err.Timeout() {
		test.Fatal("Should be timeout error")
	}
}
