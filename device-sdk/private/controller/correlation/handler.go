// -*- Mode: Go; indent-tabs-mode: t -*-
//
// Copyright (C) 2019-2020 IOTech Ltd
//
// SPDX-License-Identifier: Apache-2.0

package correlation

import (
	"context"

	bootstrapContainer "gitee.com/hckdigi/silinode-common/mod/bootstrap/bootstrap/container"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients"
	"gitee.com/hckdigi/silinode-common/mod/core-contracts/clients/logger"
)

func IdFromContext(ctx context.Context) string {
	hdr, ok := ctx.Value(clients.CorrelationHeader).(string)
	if !ok {
		hdr = ""
	}
	return hdr
}

func LoggingClientFromContext(ctx context.Context) logger.LoggingClient {
	lc, ok := ctx.Value(bootstrapContainer.LoggingClientInterfaceName).(logger.LoggingClient)
	if !ok {
		lc = nil
	}
	return lc
}
