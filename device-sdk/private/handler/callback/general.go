// -*- Mode: Go; indent-tabs-mode: t -*-
//
// Copyright (C) 2017-2018 Canonical Ltd
// Copyright (C) 2018-2020 IOTech Ltd
//
// SPDX-License-Identifier: Apache-2.0

package callback

import (
	"fmt"

	bootstrapContainer "gitee.com/hckdigi/silinode-common/mod/bootstrap/bootstrap/container"
	"gitee.com/hckdigi/silinode-common/mod/bootstrap/di"
	contract "gitee.com/hckdigi/silinode-common/mod/core-contracts/models"
	"gitee.com/hckdigi/silinode-common/device-sdk/private/common"
)

func CallbackHandler(cbAlert contract.CallbackAlert, method string, dic *di.Container) common.AppError {
	lc := bootstrapContainer.LoggingClientFrom(dic.Get)

	if (cbAlert.Id == "") || (cbAlert.ActionType == "") {
		appErr := common.NewBadRequestError("Missing parameters", nil)
		lc.Error(fmt.Sprintf("Missing callback parameters"))
		return appErr
	}

	if cbAlert.ActionType == contract.DEVICE {
		return handleDevice(method, cbAlert.Id, dic)
	} else if cbAlert.ActionType == contract.SERVICE {
		return handleService(method, cbAlert.Id, dic)
	} else if cbAlert.ActionType == contract.PROFILE {
		return handleProfile(method, cbAlert.Id, dic)
	} else if cbAlert.ActionType == contract.PROVISIONWATCHER {
		return handleProvisionWatcher(method, cbAlert.Id, dic)
	}

	lc.Error(fmt.Sprintf("Invalid callback action type: %s", cbAlert.ActionType))
	appErr := common.NewBadRequestError("Invalid callback action type", nil)
	return appErr
}
