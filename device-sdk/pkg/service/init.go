// -*- Mode: Go; indent-tabs-mode: t -*-
//
// Copyright (C) 2020 IOTech Ltd
//
// SPDX-License-Identifier: Apache-2.0

package service

import (
	"context"
	"fmt"
	bootstrapContainer "gitee.com/hckdigi/silinode-common/mod/bootstrap/bootstrap/container"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"
	"gitee.com/hckdigi/silinode-common/mod/bootstrap/bootstrap/startup"
	"gitee.com/hckdigi/silinode-common/mod/bootstrap/di"
	"gitee.com/hckdigi/silinode-common/device-sdk/private/autoevent"
	"gitee.com/hckdigi/silinode-common/device-sdk/private/cache"
	"gitee.com/hckdigi/silinode-common/device-sdk/private/container"
	"gitee.com/hckdigi/silinode-common/device-sdk/private/provision"
	v2cache "gitee.com/hckdigi/silinode-common/device-sdk/private/v2/cache"
	dsModels "gitee.com/hckdigi/silinode-common/device-sdk/pkg/models"
)

// Bootstrap contains references to dependencies required by the BootstrapHandler.
type Bootstrap struct {
	router *mux.Router
}

// NewBootstrap is a factory method that returns an initialized Bootstrap receiver struct.
func NewBootstrap(router *mux.Router) *Bootstrap {
	return &Bootstrap{
		router: router,
	}
}

func (b *Bootstrap) BootstrapHandler(ctx context.Context, wg *sync.WaitGroup, startupTimer startup.Timer, dic *di.Container) (success bool) {
	ds.UpdateFromContainer(b.router, dic)
	autoevent.NewManager(ctx, wg, ds.config.Service.AsyncBufferSize, dic)

	err := ds.selfRegister()
	if err != nil {
		ds.LoggingClient.Error(fmt.Sprintf("Couldn't register to metadata service: %v\n", err))
		return false
	}

	// initialize devices, deviceResources, provisionWatchers & profiles cache
	cache.InitCache(
		ds.ServiceName,
		ds.LoggingClient,
		container.CoredataValueDescriptorClientFrom(dic.Get),
		container.MetadataDeviceClientFrom(dic.Get),
		container.MetadataProvisionWatcherClientFrom(dic.Get))
	v2cache.InitV2Cache()

	if ds.AsyncReadings() {
		ds.asyncCh = make(chan *dsModels.AsyncValues, ds.config.Service.AsyncBufferSize)
		go ds.processAsyncResults(ctx, wg)
	}
	if ds.DeviceDiscovery() {
		ds.deviceCh = make(chan []dsModels.DiscoveredDevice, 1)
		go ds.processAsyncFilterAndAdd(ctx, wg)
	}

	err = ds.driver.Initialize(ds.LoggingClient, ds.asyncCh, ds.deviceCh)
	if err != nil {
		ds.LoggingClient.Error(fmt.Sprintf("Driver.Initialize failed: %v\n", err))
		return false
	}
	ds.initialized = true

	dic.Update(di.ServiceConstructorMap{
		container.DeviceServiceName: func(get di.Get) interface{} {
			return ds.deviceService
		},
		container.ProtocolDiscoveryName: func(get di.Get) interface{} {
			return ds.discovery
		},
		container.ProtocolDriverName: func(get di.Get) interface{} {
			return ds.driver
		},
	})

	ds.controller.InitRestRoutes()

	err = provision.LoadProfiles(ds.config.Device.ProfilesDir, dic)
	if err != nil {
		ds.LoggingClient.Error(fmt.Sprintf("Failed to create the pre-defined device profiles: %v\n", err))
		return false
	}

	err = provision.LoadDevices(ds.config.DeviceList, dic)
	if err != nil {
		ds.LoggingClient.Error(fmt.Sprintf("Failed to create the pre-defined devices: %v\n", err))
		return false
	}

	go checkDeviceOnlineState(ctx, dic)

	autoevent.GetManager().StartAutoEvents(dic)
	http.TimeoutHandler(nil, time.Millisecond*time.Duration(ds.config.Service.Timeout), "Request timed out")

	return true
}

func checkDeviceOnlineState(ctx context.Context, dic *di.Container) {
	lc := bootstrapContainer.LoggingClientFrom(dic.Get)
	driver := container.ProtocolDriverFrom(dic.Get)
	mc := container.MetadataDeviceClientFrom(dic.Get)

	timer := time.NewTicker(time.Minute)
	defer timer.Stop()
	for {
		select {
		case <-timer.C:
			lc.Debug("check device online state begin")
			devices := cache.Devices().All()
			if len(devices) > 0 {
				for _, device := range devices {
					online, err := driver.CheckOnline(device.Name, device.Protocols)
					if err != nil {
						lc.Error(fmt.Sprintf("device %s check online state error: %v", device.Name, err))
						err = mc.UpdateOnlineStateByName(ctx, device.Name, false)
						if err != nil {
							lc.Error(fmt.Sprintf("update device %s online state %v error: %v", device.Name, false, err))
						}
					} else {
						if online != 0 {
							state := false
							if online == 1 {
								state = true
							}
							err = mc.UpdateOnlineStateByName(ctx, device.Name, state)
							if err != nil {
								lc.Error(fmt.Sprintf("update device %s online state %v error: %v", device.Name, online, err))
							}
						}
					}
				}
			}
			lc.Debug("check device online state end")
		case <-ctx.Done():
			return
		}
	}
}
